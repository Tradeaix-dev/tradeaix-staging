({
    getBuyerlist :function(component, event, helper){
        debugger;
        var sitename = localStorage.getItem("TSiteName").charAt(0).toUpperCase() + localStorage.getItem("TSiteName").slice(1)
            document.title =sitename +' : Quote';
        component.set("v.Tenantid",localStorage.getItem('LoggeduserTenantID'));
        if(localStorage.getItem('LoggeduserProfile') == $A.get("{!$Label.c.ACP_ProfileID}"))
        {
            component.set("v.Tenantcheck",true);
            var Action1 = component.get("c.loadorgs");   
            Action1.setParams({
                "username" : localStorage.getItem('UserSession')                
            });        
            Action1.setCallback(this, function(actionResult1) {
                debugger;
                var state = actionResult1.getState(); 
                var results = actionResult1.getReturnValue();
                if(state === "SUCCESS"){
                    component.set("v.OrgFilter",results);
                }
                
            }); 
            $A.enqueueAction(Action1);
        }
        else{
            component.set("v.Tenantcheck",false);                        
        }
        
        var action = component.get('c.buyerDetails');  
        var self = this;
        action.setParams({
           "counter" : (component.get("v.counter")).toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
			"LoggedUserId" : localStorage.getItem("IDTenant"),
            "Filtervalue" : component.get("v.selectedFilterItem")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){ 
                helper.setGridOption(component, actionResult.getReturnValue());
                //component.set("v.offer", result.results);
            }
            
        });
        $A.enqueueAction(action);
    },
    onSingleSelectChange: function(component, event, helper) {
        debugger;
        var selectCmp = component.find("InputSelectSingle").get("v.value");
        component.set("v.selectedFilterItem",selectCmp);
        var action = component.get('c.Filter'); 
        var self = this; 
        action.setParams({
            "counter" : (component.get("v.counter")).toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "LoggeduserTenantID" : localStorage.getItem("Tenantid"),
			"LoggedUserId" : localStorage.getItem("IDTenant"),
            "Filtervalue" : selectCmp
        });
        action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
                ///component.set("v.offer", result.results);
            }
        }); 
        $A.enqueueAction(action); 
    }, 
    
     Next: function(component, event, helper) {
        debugger;        
        var action = component.get('c.getNext'); 
        var list_size = component.get('v.list_size');
        var counter = component.get('v.counter');
        var res = list_size + counter;
        action.setParams({ 
            "counter" : res.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
			"LoggedUserId" : localStorage.getItem("IDTenant"),
            "Filtervalue" : component.get("v.selectedFilterItem")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
         $A.enqueueAction(action);
     },
    End: function(component, event, helper) {
        debugger;
        var action = component.get('c.getEnd');  
        var list_size = component.get('v.list_size');
        var counter = component.get('v.counter');
        var total_size = component.get('v.total_size');
        var res = total_size - (total_size % list_size); 
        if(res == total_size) { res -= list_size; }
        action.setParams({ 
           "counter" : res.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
			"LoggedUserId" : localStorage.getItem("IDTenant"),
            "Filtervalue" : component.get("v.selectedFilterItem")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    Beginning: function(component, event, helper) {
        debugger;
        var action = component.get('c.getBeginning'); 
        action.setParams({
             "counter" : '0',
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
			"LoggedUserId" : localStorage.getItem("IDTenant"),
            "Filtervalue" : component.get("v.selectedFilterItem")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    Previous: function(component, event, helper) {
        debugger;        
        var action = component.get('c.getPrevious'); 
        var list_size = component.get('v.list_size');        
        var counter = component.get('v.counter');
        var res = counter - list_size;  
        if(res < 0) { res = 0; }
        action.setParams({            
           "counter" : res.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
			"LoggedUserId" : localStorage.getItem("IDTenant"),
            "Filtervalue" : component.get("v.selectedFilterItem")
        });
        action.setCallback(this, function(actionResult) {           
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){ 
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },  btnFilter: function(component) { 
        debugger;        
          
            component.set("v.Filter", true);
        var action = component.get('c.getFilter');
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            console.log("state" + state);
            if(state === "SUCCESS"){
                var result = actionResult.getReturnValue(); 
                component.set("v.filterObject", result);
                component.set("v.filterObject.CreateddateTo__c", result.CreateddateTo__c);
                component.set("v.filterObject.CreateddateFrom__c", result.CreateddateFrom__c);
                component.set("v.Filter", true);
                component.set("v.MinFilter", false);
                window.setTimeout($A.getCallback(function(){ 
                    //Make the DIV element draggagle:
                    dragElement(document.getElementById("mydiv")); 
                    
                    function dragElement(elmnt) {  
                        var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
                        if (document.getElementById(elmnt.id + "header")) {
                            /* if present, the header is where you move the DIV from:*/
                            document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
                        } else {
                            /* otherwise, move the DIV from anywhere inside the DIV:*/
                            elmnt.onmousedown = dragMouseDown;
                        }
                        
                        function dragMouseDown(e) { 
                            e = e || window.event;
                            e.preventDefault();
                            // get the mouse cursor position at startup:
                            pos3 = e.clientX;
                            pos4 = e.clientY;
                            document.onmouseup = closeDragElement;
                            // call a function whenever the cursor moves:
                            document.onmousemove = elementDrag;
                        }
                        
                        function elementDrag(e) { 
                            e = e || window.event;
                            e.preventDefault();
                            // calculate the new cursor position:
                            pos1 = pos3 - e.clientX;
                            pos2 = pos4 - e.clientY;
                            pos3 = e.clientX;
                            pos4 = e.clientY;
                            // set the element's new position:
                            elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
                            elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
                        }
                        
                        function closeDragElement() { 
                            /* stop moving when mouse button is released:*/
                            document.onmouseup = null;
                            document.onmousemove = null;
                        }
                    } 
                }), 500);
            } 
        });
        $A.enqueueAction(action);        
    }, 
    cancelFilter : function(component, event) {
        debugger;
        component.set("v.Filter", false);         
    },     
    minFilter: function(component, event) {
        component.set("v.Filter", false);
        component.set("v.MinFilter", true);        
        window.setTimeout($A.getCallback(function(){ 
            //Make the DIV element draggagle:
            dragElement(document.getElementById("mydiv1")); 
            
            function dragElement(elmnt) {  
                var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
                if (document.getElementById(elmnt.id + "header")) {
                    /* if present, the header is where you move the DIV from:*/
                    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
                } else {
                    /* otherwise, move the DIV from anywhere inside the DIV:*/
                    elmnt.onmousedown = dragMouseDown;
                }
                
                function dragMouseDown(e) { 
                    e = e || window.event;
                    e.preventDefault();
                    // get the mouse cursor position at startup:
                    pos3 = e.clientX;
                    pos4 = e.clientY;
                    document.onmouseup = closeDragElement;
                    // call a function whenever the cursor moves:
                    document.onmousemove = elementDrag;
                }
                
                function elementDrag(e) { 
                    e = e || window.event;
                    e.preventDefault();
                    // calculate the new cursor position:
                    pos1 = pos3 - e.clientX;
                    pos2 = pos4 - e.clientY;
                    pos3 = e.clientX;
                    pos4 = e.clientY;
                    // set the element's new position:
                    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
                    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
                }
                
                function closeDragElement() { 
                    /* stop moving when mouse button is released:*/
                    document.onmouseup = null;
                    document.onmousemove = null;
                }
            } 
        }), 500);
    }, 
    maxFilter: function(component, event) { 
        component.set("v.MinFilter", false); 
        component.set("v.Filter", true);
        window.setTimeout($A.getCallback(function(){ 
            //Make the DIV element draggagle:
            dragElement(document.getElementById("mydiv")); 
            
            function dragElement(elmnt) {  
                var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
                if (document.getElementById(elmnt.id + "header")) {
                    /* if present, the header is where you move the DIV from:*/
                    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
                } else {
                    /* otherwise, move the DIV from anywhere inside the DIV:*/
                    elmnt.onmousedown = dragMouseDown;
                }
                
                function dragMouseDown(e) { 
                    e = e || window.event;
                    e.preventDefault();
                    // get the mouse cursor position at startup:
                    pos3 = e.clientX;
                    pos4 = e.clientY;
                    document.onmouseup = closeDragElement;
                    // call a function whenever the cursor moves:
                    document.onmousemove = elementDrag;
                }
                
                function elementDrag(e) { 
                    e = e || window.event;
                    e.preventDefault();
                    // calculate the new cursor position:
                    pos1 = pos3 - e.clientX;
                    pos2 = pos4 - e.clientY;
                    pos3 = e.clientX;
                    pos4 = e.clientY;
                    // set the element's new position:
                    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
                    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
                }
                
                function closeDragElement() { 
                    /* stop moving when mouse button is released:*/
                    document.onmouseup = null;
                    document.onmousemove = null;
                }
            } 
        }), 500);
    }, 
    GoSearch : function(component, event,helper) { 
        debugger;
        var filter = component.get("v.searchKeyword"); 
        console.log('@filter@'+filter);
        var action = component.get('c.saveFilter'); 
        action.setParams({ 
            "counter" : (component.get("v.counter")).toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "filter" : filter,
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
            "Filtervalue" : component.get("v.selectedFilterItem")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){
                helper.setGridOption(component, actionResult.getReturnValue());
                debugger;
                component.set("v.filterObject", filter);
                component.set("v.Filter", false);   
            } 
        });
        $A.enqueueAction(action);
    }, 
    applyFilter : function(component, event,helper) { 
        debugger;
        var stardateTime=component.find("Createddatefrom").get("v.value");
        var enddateTime=component.find("Createddateto").get("v.value");
        debugger;
        if((stardateTime==null && enddateTime!=null) || (stardateTime!=null && enddateTime==null)) {
            debugger;
            component.set("v.EnddateErrormsg", "Created Date cannot be a blank.");
            return null;
        }
        var filter = component.get("v.filterObject");
        console.log('@filter@'+filter);
        var action = component.get('c.alyFilter'); 
        var self = this;
        action.setParams({ 
            "counter" : (component.get("v.counter")).toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),            
            "filter" : filter,
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "Filtervalue" : component.get("v.selectedFilterItem")
        });
        action.setCallback(this, function(actionResult) { 
            
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){
                helper.setGridOption(component, actionResult.getReturnValue());
                debugger;
                component.set("v.filterObject", filter);
                component.set("v.Filter", false); 
            }               
        });
        $A.enqueueAction(action);
        
    }, resetFilter : function(component, event) { 
        debugger;
        component.set("v.filterObject.CreateddateTo__c", null);
        component.set("v.filterObject.CreateddateFrom__c", null);
        component.set("v.searchKeyword", null);
        component.set("v.isFilterA", false);
        
        
    },
})