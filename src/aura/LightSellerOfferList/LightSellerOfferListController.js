({ 
    doneRendering: function(component) {
        /* jaya */
        window.resize = function(){
            var profileMenuRegion = document.getElementsByClassName("profileMenuRegion")[0];
            var navigation = document.getElementsByClassName("navigation")[0];
            var newHeader = document.getElementsByClassName("newHeader")[0];
            var pageHeader = document.getElementsByClassName("slds-page-header0")[0];
            var pageHeader1 = document.getElementsByClassName("slds-page-header1")[0];
            var pageHeader2 = document.getElementsByClassName("slds-page-header2")[0];
            
            var tableresponsive = document.getElementsByClassName("table-responsive")[0];
            var pager = document.getElementsByClassName("pager-div")[0];
            
            var tblCustomizedstocktable =  document.getElementsByClassName("Customizedstocktable")[0];
            var tBody = tblCustomizedstocktable.tBodies[0];
            var tHead = tblCustomizedstocktable.tHead;
            
            var totalHieght = profileMenuRegion.clientHeight;
            totalHieght = totalHieght + navigation.clientHeight;
            totalHieght = totalHieght + newHeader.clientHeight;
            totalHieght = totalHieght + pageHeader.clientHeight;
			
            if (typeof(pageHeader1) != 'undefined'){
                totalHieght = totalHieght + pageHeader1.clientHeight;
            }
            if (typeof(pageHeader2) != 'undefined'){
                totalHieght = totalHieght + pageHeader2.clientHeight;
            }
            
            totalHieght = totalHieght + (tHead.clientHeight+18);
            totalHieght = totalHieght + pager.clientHeight;
            var bodyHeight = window.innerHeight - totalHieght;

            if(bodyHeight <= 78){
                tBody.style.height = "74px";
            } else if(bodyHeight <= 103){
                tBody.style.height = "101px";
            } else {
	            tBody.style.height = bodyHeight+"px";
            }
            
            tableresponsive.style.height = (tblCustomizedstocktable.clientHeight+14) + "px";    
        }
        window.resize();
        window.addEventListener('resize', resize, true);
        /* jaya */
    },
    getOfferList: function(component) { 
        var action = component.get('c.getSellerOfferList');
        debugger;
        action.setParams({
            "counter" : (component.get("v.counter")).toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem")
        }); 
        action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                component.set("v.Offer", result.results);
                component.set("v.loginUserCU", result.loginUserCU.Id); 
                component.set("v.counter", result.counter);                
                component.set("v.total_size", result.total_size); 
                component.set("v.total_page", result.total_page);  
                component.set("v.sortbyField", result.sortbyField);
                component.set("v.sortDirection", result.sortDirection);

                component.set("v.offerIn", false);
                component.set("v.validation", false);
                component.set("v.loanError", '');
                component.set("v.templateError", '');
                component.set("v.nameError", '');
                component.set("v.participationAmountError", '');
                component.set("v.participationPercentageError", '');
                component.set("v.SellerOfferObject.Name", null);
                component.set("v.SellerOfferObject.OfferType__c", "-- None --");
                component.set("v.SellerOfferObject.IsPrimary__c", false);
                component.set("v.SellerOfferObject.Seller_Notes__c", null); 
                component.set("v.SellerOfferObject.ParticipationAmount__c", null);
                component.set("v.SellerOfferObject.ParticipationPercentage__c", null);
                component.set("v.SellerOfferObject.Record_Id__c", null);
                
                var productTemplates = component.get("v.productTemplates");
                if(productTemplates){
                    if(productTemplates.length>0){
                		component.set("v.SellerOfferObject.TemplateId__c", productTemplates[0].value); 
                    }
                }
                var pageOptions=[]; 
                for(var i=0;i<=result.total_page-1;i++) { 
                    pageOptions.push({"class": "optionClass", label: i+1, value: i+1});
                } 
                var inputsel = component.find("pageOptions");
                inputsel.set("v.options", pageOptions); 
                component.set("v.offerList", result.offerList); 
                component.set("v.list_size", result.list_size);
                component.set("v.showpage", '1');
                inputsel.set("v.value", '1');
                component.find("recordSize").set("v.value", (result.list_size).toString()); 
				debugger;
                var opts = [];
                if(result.isSeller || result.isSuperUser)
                
                {
                    opts = [
                        { label: "--Select--", value: "--Select--", selected: "true" },
                        { label: "Pending Approval", value: "Pending Approval"},
                        { label: "Pending Validation", value: "Pending Validation"},
                        { label: "Validation Rejected", value: "Validation Rejected"},
                        { label: "Unpublished", value: "Unpublished"},
                        { label: "Transaction Rejected", value: "Transaction Rejected"},
                        { label: "Transaction Approved", value: "Transaction Approved"},
                        { label: "Bid Submitted", value: "Bid Submitted"},
                        { label: "Completed Transaction", value: "Completed Transaction"},
                        { label: "Inactive", value: "Inactive"}
                    ];
                }
                else if(result.isPartial)
                {
                    opts = [
                        { label: "All Active", value: "AllOffers", selected: "true" } /*Transactions*/
                    ];
                } 
                else if(result.isSellerBuyer)
                {
                    opts = [
                        { label: "All Active", value: "AllOffers", selected: "true" },/*Transactions*/
                        { label: "My Party Activity", value: "MyCUOffers" },
                        { label: "My Party Created", value: "UnPublishedOffers" },/*Transactions*/
                        { label: "My Party Inactive", value: "InactiveOffers" }/*Transactions*/
                    ];
                } 
                else
                {
                    opts = [
                        { label: "For Approval", value: "UnPublishedOffers", selected: "true"},/*Transactions*/
                    ];
                    opts = [];
                }  
                /*jaya*/
                component.set("v.filterOptions", opts); 
                debugger;
                component.set("v.selectedItem", result.selectedItem); 
                component.set("v.isSeller", result.isSeller);
                /*jaya*/
                
                component.set("v.isSuperUser", result.isSuperUser); 
                component.set("v.isSellerBuyer", result.isSellerBuyer);
                component.set("v.isSellerAssociate", result.isSellerAssociate);
                component.set("v.isPartial", result.isPartial);
                component.set("v.isTandC", result.isTandC);
                
                if (result.counter > 0) {
                    component.find("disableBeginning").set("v.disabled", false);
                    component.find("disablePrevious").set("v.disabled", false);
                } 
                else
                {
                    component.find("disableBeginning").set("v.disabled", true);
                    component.find("disablePrevious").set("v.disabled", true);
                } 
                if (result.counter + result.list_size < result.total_size) 
                {
                    component.find("disableNext").set("v.disabled", false);
                    component.find("disableEnd").set("v.disabled", false); 
                }   
                else 
                {
                    component.find("disableNext").set("v.disabled", true);
                    component.find("disableEnd").set("v.disabled", true);
                } 
                
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        }); 
        $A.enqueueAction(action);      
        var ActionTitle = component.get("c.getloggeduserBrowserTitle");
ActionTitle.setCallback(this, function(actionResultTitle) {
var statetitle = actionResultTitle.getState(); 
var resulttitle = actionResultTitle.getReturnValue();
if(statetitle === "SUCCESS"){
debugger;
document.title =resulttitle +' :: Transactions';
}
});
$A.enqueueAction(ActionTitle);

    },
    Beginning: function(component) {
        var action = component.get('c.getBeginning'); 
		var self = this;
        action.setParams({
            "counter" : '0',
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem")/*jaya*/
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.Offer", result.results);
            component.set("v.loginUserCU", result.loginUserCU.Id);
            component.set("v.counter", result.counter); 
            component.set("v.total_size", result.total_size); 
            component.set("v.total_page", result.total_page);
            component.set("v.sortbyField", result.sortbyField);
            component.set("v.sortDirection", result.sortDirection);
            
            var pageOptions=[]; 
            for(var i=0;i<=result.total_page-1;i++) { 
                pageOptions.push({"class": "optionClass", label: i+1, value: i+1});
            } 
            var inputsel = component.find("pageOptions");
            inputsel.set("v.options", pageOptions); 
            component.set("v.offerList", result.offerList);            
            component.set("v.list_size", result.list_size); 

            var showpage = '';
            if(result.counter == 0) 
                showpage ='1'; 
            else
                showpage = ((result.counter/result.list_size)+1).toString();
            
            component.set("v.showpage", showpage);
            inputsel.set("v.value", showpage);  
            component.find("recordSize").set("v.value", (result.list_size).toString());
            
            if (result.counter > 0) {
                component.find("disableBeginning").set("v.disabled", false);
                component.find("disablePrevious").set("v.disabled", false);
            } 
            else
            {
                component.find("disableBeginning").set("v.disabled", true);
                component.find("disablePrevious").set("v.disabled", true);
            } 
            if (result.counter + result.list_size < result.total_size) 
            {
                component.find("disableNext").set("v.disabled", false);
                component.find("disableEnd").set("v.disabled", false); 
            }   
            else 
            {
                component.find("disableNext").set("v.disabled", true);
                component.find("disableEnd").set("v.disabled", true);
            }
        });
        $A.enqueueAction(action);
    },
    Previous: function(component) {
        var action = component.get('c.getPrevious');  
        var list_size = component.get('v.list_size');
        var counter = component.get('v.counter');
        var res = counter - list_size;  
        if(res < 0) 
            res = 0; 
        var self = this; 
        action.setParams({ 
            "counter" : res.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem")/*jaya*/
        });
        action.setCallback(this, function(actionResult) { 
            var result = actionResult.getReturnValue();
            component.set("v.Offer", result.results);
            component.set("v.loginUserCU", result.loginUserCU.Id);
            component.set("v.counter", result.counter); 
            component.set("v.total_size", result.total_size); 
            component.set("v.total_page", result.total_page);
            component.set("v.sortbyField", result.sortbyField);
            component.set("v.sortDirection", result.sortDirection);
            
            var pageOptions=[]; 
            for(var i=0;i<=result.total_page-1;i++) { 
                pageOptions.push({"class": "optionClass", label: i+1, value: i+1});
            } 
            var inputsel = component.find("pageOptions");
            inputsel.set("v.options", pageOptions); 
            component.set("v.offerList", result.offerList);
            component.set("v.list_size", result.list_size); 
            var showpage = '';
            if(result.counter == 0) 
                showpage ='1'; 
            else
                showpage = ((result.counter/result.list_size)+1).toString();
            
            component.set("v.showpage", showpage);
            inputsel.set("v.value", showpage);
            component.find("recordSize").set("v.value", (result.list_size).toString());
            
            if (result.counter > 0) {
                component.find("disableBeginning").set("v.disabled", false);
                component.find("disablePrevious").set("v.disabled", false);
            } 
            else
            {
                component.find("disableBeginning").set("v.disabled", true);
                component.find("disablePrevious").set("v.disabled", true);
            } 
            
            if (result.counter + result.list_size < result.total_size) 
            {
                component.find("disableNext").set("v.disabled", false);
                component.find("disableEnd").set("v.disabled", false); 
            }   
            else 
            {
                component.find("disableNext").set("v.disabled", true);
                component.find("disableEnd").set("v.disabled", true);
            }
        });
        $A.enqueueAction(action);
    },
    Next: function(component) {
        var action = component.get('c.getNext'); 
        var list_size = component.get('v.list_size');
        var counter = component.get('v.counter');
        var res = list_size + counter;
        
        action.setParams({ 
            "counter" : res.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem")/*jaya*/ 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.Offer", result.results);
            component.set("v.loginUserCU", result.loginUserCU.Id);
            component.set("v.counter", result.counter); 
            component.set("v.total_size", result.total_size); 
            component.set("v.total_page", result.total_page);
            component.set("v.sortbyField", result.sortbyField);
            component.set("v.sortDirection", result.sortDirection);
                       
            var pageOptions=[]; 
            for(var i=0;i<=result.total_page-1;i++) { 
                pageOptions.push({"class": "optionClass", label: i+1, value: i+1});
            }  
            var inputsel = component.find("pageOptions");
            inputsel.set("v.options", pageOptions);  
            component.set("v.offerList", result.offerList);
            component.set("v.list_size", result.list_size); 
            var showpage = '';
            if(result.counter == 0) 
                showpage ='1'; 
            else
                showpage = ((result.counter/result.list_size)+1).toString();
            
            component.set("v.showpage", showpage);
            inputsel.set("v.value", showpage);
            component.find("recordSize").set("v.value", (result.list_size).toString());
            
            if (result.counter > 0) {
                component.find("disableBeginning").set("v.disabled", false);
                component.find("disablePrevious").set("v.disabled", false);
            } 
            else
            {
                component.find("disableBeginning").set("v.disabled", true);
                component.find("disablePrevious").set("v.disabled", true);
            } 
            
            if (result.counter + result.list_size < result.total_size) 
            {
                component.find("disableNext").set("v.disabled", false);
                component.find("disableEnd").set("v.disabled", false); 
            }   
            else 
            {
                component.find("disableNext").set("v.disabled", true);
                component.find("disableEnd").set("v.disabled", true);
            }
        });
        $A.enqueueAction(action);
    },
    End: function(component) {
        var action = component.get('c.getEnd');  
        var list_size = component.get('v.list_size');
        var counter = component.get('v.counter');
        var total_size = component.get('v.total_size');
        var res = total_size - (total_size % list_size); 
        
        if(res == total_size)
            res -= list_size; 
        
        action.setParams({ 
            "counter" : res.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem")/*jaya*/
        });
        action.setCallback(this, function(actionResult) {
            var result = actionResult.getReturnValue();
            component.set("v.Offer", result.results);
            component.set("v.loginUserCU", result.loginUserCU.Id);
            component.set("v.counter", result.counter); 
            component.set("v.total_size", result.total_size); 
            component.set("v.total_page", result.total_page);
            component.set("v.sortbyField", result.sortbyField);
            component.set("v.sortDirection", result.sortDirection);
            
            var pageOptions=[]; 
            for(var i=0;i<=result.total_page-1;i++) { 
                pageOptions.push({"class": "optionClass", label: i+1, value: i+1});
            } 
            var inputsel = component.find("pageOptions");
            inputsel.set("v.options", pageOptions);
            component.set("v.offerList", result.offerList);
            component.set("v.list_size", result.list_size); 
            var showpage = '';
            if(result.counter == 0) 
                showpage ='1'; 
            else
                showpage = ((result.counter/result.list_size)+1).toString();
            
            component.set("v.showpage", showpage);
            inputsel.set("v.value", showpage);
            component.find("recordSize").set("v.value", (result.list_size).toString());
            
            if (result.counter > 0) {
                component.find("disableBeginning").set("v.disabled", false);
                component.find("disablePrevious").set("v.disabled", false);
            } 
            else
            {
                component.find("disableBeginning").set("v.disabled", true);
                component.find("disablePrevious").set("v.disabled", true);
            } 
            
            if (result.counter + result.list_size < result.total_size) 
            {
                component.find("disableNext").set("v.disabled", false);
                component.find("disableEnd").set("v.disabled", false); 
            }   
            else 
            {
                component.find("disableNext").set("v.disabled", true);
                component.find("disableEnd").set("v.disabled", true);
            }
        });
        $A.enqueueAction(action);
    }, 
    sort: function(component, event, helper) {
        debugger;
        var action = component.get('c.SortTable'); 
        var sortfield = event.currentTarget.getAttribute("data-recId");  
        action.setParams({
            "counter" : (component.get("v.counter")).toString(),
            "sortbyField" : sortfield,
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem")/*jaya*/ 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.Offer", result.results);
            component.set("v.loginUserCU", result.loginUserCU.Id);
            component.set("v.counter", result.counter); 
            component.set("v.total_size", result.total_size); 
            component.set("v.total_page", result.total_page);
            component.set("v.sortbyField", result.sortbyField);
            component.set("v.sortDirection", result.sortDirection);
                        
            var pageOptions=[]; 
            for(var i=0;i<=result.total_page-1;i++) { 
                pageOptions.push({"class": "optionClass", label: i+1, value: i+1});
            } 
            var inputsel = component.find("pageOptions");
            inputsel.set("v.options", pageOptions);
            component.set("v.offerList", result.offerList);
            component.set("v.list_size", result.list_size); 
            var showpage = '';
            if(result.counter == 0) 
                showpage ='1'; 
            else
                showpage = ((result.counter/result.list_size)+1).toString();
            
            component.set("v.showpage", showpage);
            inputsel.set("v.value", showpage);
            component.find("recordSize").set("v.value", (result.list_size).toString());
            
            if (result.counter > 0) {
                component.find("disableBeginning").set("v.disabled", false);
                component.find("disablePrevious").set("v.disabled", false);
            } 
            else
            {
                component.find("disableBeginning").set("v.disabled", true);
                component.find("disablePrevious").set("v.disabled", true);
            } 
            
            if (result.counter + result.list_size < result.total_size) 
            {
                component.find("disableNext").set("v.disabled", false);
                component.find("disableEnd").set("v.disabled", false); 
            }   
            else 
            {
                component.find("disableNext").set("v.disabled", true);
                component.find("disableEnd").set("v.disabled", true);
            }
        });
        $A.enqueueAction(action);
    }, 
    changePageSizes: function(component, event, helper) {
        debugger;
        var action = component.get('c.changelist_size');  
        var list_size = component.find("recordSize").get("v.value"); 
        action.setParams({
            "counter" : '0',
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "list_size" : list_size,
            "selected" : component.get("v.selectedItem")/*jaya*/
        });
        action.setCallback(this, function(actionResult) {
            var result = actionResult.getReturnValue();
            component.set("v.Offer", result.results);
            component.set("v.loginUserCU", result.loginUserCU.Id);
            component.set("v.counter", result.counter); 
            component.set("v.total_size", result.total_size); 
            component.set("v.total_page", result.total_page);
            component.set("v.sortbyField", result.sortbyField);
            component.set("v.sortDirection", result.sortDirection);
                       
            var pageOptions=[]; 
            for(var i=0;i<=result.total_page-1;i++) { 
                pageOptions.push({"class": "optionClass", label: i+1, value: i+1});
            }
            var inputsel = component.find("pageOptions");
            inputsel.set("v.options", pageOptions);
            component.set("v.offerList", result.offerList);
            component.set("v.list_size", result.list_size); 
            var showpage = '';
            if(result.counter == 0) 
                showpage ='1'; 
            else
                showpage = ((result.counter/result.list_size)+1).toString();
            
            component.set("v.showpage", showpage);
            inputsel.set("v.value", showpage);
            component.find("recordSize").set("v.value", (result.list_size).toString());
            
            if (result.counter > 0) {
                component.find("disableBeginning").set("v.disabled", false);
                component.find("disablePrevious").set("v.disabled", false);
            } 
            else
            {
                component.find("disableBeginning").set("v.disabled", true);
                component.find("disablePrevious").set("v.disabled", true);
            } 
            
            if (result.counter + result.list_size < result.total_size) 
            {
                component.find("disableNext").set("v.disabled", false);
                component.find("disableEnd").set("v.disabled", false); 
            }   
            else 
            {
                component.find("disableNext").set("v.disabled", true);
                component.find("disableEnd").set("v.disabled", true);
            }
        });
        $A.enqueueAction(action);
    }, 
    View: function(component, event, helper) {
        debugger;
        var action = component.get('c.Views'); 
        var selected = event.getSource().get("v.text");
        component.set("v.selectedItem", selected)/*jaya*/
        action.setParams({
            "counter" : '0',
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem")/*jaya*/
        });
        action.setCallback(this, function(actionResult) {
            var result = actionResult.getReturnValue();
            component.set("v.Offer", result.results);
            component.set("v.loginUserCU", result.loginUserCU.Id);
            component.set("v.counter", result.counter); 
            component.set("v.total_size", result.total_size); 
            component.set("v.total_page", result.total_page);
            component.set("v.sortbyField", result.sortbyField);
            component.set("v.sortDirection", result.sortDirection);
            component.set("v.selectedItem", result.selectedItem);  
            
            var pageOptions=[]; 
            for(var i=0;i<=result.total_page-1;i++) { 
                pageOptions.push({"class": "optionClass", label: i+1, value: i+1});
            }
            var inputsel = component.find("pageOptions");
            inputsel.set("v.options", pageOptions);
            component.set("v.offerList", result.offerList);            
            component.set("v.list_size", result.list_size); 
            var showpage = '';
            if(result.counter == 0) 
                showpage ='1'; 
            else
                showpage = ((result.counter/result.list_size)+1).toString();
            
            component.set("v.showpage", showpage);
            inputsel.set("v.value", showpage);
            component.find("recordSize").set("v.value", (result.list_size).toString());
            
            if (result.counter > 0) {
                component.find("disableBeginning").set("v.disabled", false);
                component.find("disablePrevious").set("v.disabled", false);
            } 
            else
            {
                component.find("disableBeginning").set("v.disabled", true);
                component.find("disablePrevious").set("v.disabled", true);
            } 
            
            if (result.counter + result.list_size < result.total_size) 
            {
                component.find("disableNext").set("v.disabled", false);
                component.find("disableEnd").set("v.disabled", false); 
            }   
            else 
            {
                component.find("disableNext").set("v.disabled", true);
                component.find("disableEnd").set("v.disabled", true);
            }
        });
        $A.enqueueAction(action);
    }, 
    changePageOptions: function(component, event, helper) {
        debugger;
        var action = component.get('c.changePage'); 
        var showpage = component.find("pageOptions").get("v.value"); 
        var list_size = component.find("recordSize").get("v.value"); 
        var counter = (parseInt(showpage)-1)*list_size;
        action.setParams({
            "counter" : counter.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem")/*jaya*/
        });
        action.setCallback(this, function(actionResult) {
            var result = actionResult.getReturnValue();
            component.set("v.Offer", result.results);
            component.set("v.loginUserCU", result.loginUserCU.Id);
            component.set("v.counter", result.counter); 
            component.set("v.total_size", result.total_size); 
            component.set("v.total_page", result.total_page);
            component.set("v.sortbyField", result.sortbyField);
            component.set("v.sortDirection", result.sortDirection);
           
            var pageOptions=[]; 
            for(var i=0;i<=result.total_page-1;i++) { 
                pageOptions.push({"class": "optionClass", label: i+1, value: i+1});
            } 
            var inputsel = component.find("pageOptions");
            inputsel.set("v.options", pageOptions); 
            component.set("v.offerList", result.offerList);
            component.set("v.list_size", result.list_size); 
            component.set("v.showpage", showpage); 
            inputsel.set("v.value", showpage.toString());
            component.find("recordSize").set("v.value", (result.list_size).toString());
            
            if (result.counter > 0) {
                component.find("disableBeginning").set("v.disabled", false);
                component.find("disablePrevious").set("v.disabled", false);
            } 
            else
            {
                component.find("disableBeginning").set("v.disabled", true);
                component.find("disablePrevious").set("v.disabled", true);
            } 
            
            if (result.counter + result.list_size < result.total_size) 
            {
                component.find("disableNext").set("v.disabled", false);
                component.find("disableEnd").set("v.disabled", false); 
            }   
            else 
            {
                component.find("disableNext").set("v.disabled", true);
                component.find("disableEnd").set("v.disabled", true);
            }
        });
        $A.enqueueAction(action);
    }, 
    downloadCsv : function(component,event,helper){
        var stockData = component.get("v.offerList"); 
        
        /* Jaya */
        var isSeller = component.get("v.isSeller");
        var isSuperUser = component.get("v.isSuperUser"); 
        var isSellerBuyer = component.get("v.isSellerBuyer");
		
        // userType {
        // 		0 - Super User
        // 		1 - Combined User
        // 		2 - Seller
        // 		3 - Buyer
    	//}
    	
        var userType = (isSeller) ? 2 : 3;
        
        if (isSuperUser) {
            userType = 0;
        }
        if (isSellerBuyer) {
            userType = 1;
        }

        var filename = "Market Place Export Results.csv";
        
        if (userType == 2){
            filename = 'Seller Offers Export Results.csv';
        }
        debugger;
        //var csv = helper.convertArrayOfObjectsToXcel(component,stockData);  
        var csv = helper.convertArrayOfObjectsToXcel(component, stockData, userType);  
        if (csv == null){return;} 
       	var blob = new Blob([csv]);
        debugger;
        if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(blob, filename);
        } else {
            var hiddenElement = document.createElement("a");
            if (hiddenElement.download !== undefined) { 
                var url = URL.createObjectURL(blob,{ type: 'text/csv;charset=utf-8;' });
                hiddenElement.setAttribute("href", url);
                hiddenElement.setAttribute("download", filename);
                hiddenElement.style = "visibility:hidden";
                document.body.appendChild(hiddenElement);
                hiddenElement.click();
                document.body.removeChild(hiddenElement);
            }
       } 
    },
    offerIn : function(component, event, helper) { 
        component.set("v.offerIn", true);       
    },
    cancel : function(component, event) {
        component.set("v.offerIn", false);
        component.set("v.validation", false);
        component.set("v.loanError", '');
        component.set("v.templateError", '');
        component.set("v.nameError", '');
        component.set("v.participationAmountError", '');
        component.set("v.participationPercentageError", '');
        component.set("v.SellerOfferObject.Name", null);
        component.set("v.SellerOfferObject.OfferType__c", "-- None --");
        component.set("v.SellerOfferObject.IsPrimary__c", true);
        component.set("v.SellerOfferObject.Seller_Notes__c", null); 
        component.set("v.SellerOfferObject.ParticipationAmount__c", null);
        component.set("v.SellerOfferObject.ParticipationPercentage__c", null);
        component.set("v.SellerOfferObject.Record_Id__c", null);
        var productTemplates = component.get("v.productTemplates");
        if(productTemplates){
            if(productTemplates.length > 0){
                component.set("v.SellerOfferObject.TemplateId__c", productTemplates[0].value); 
            }
        }
    },
    changeParticipationAmount: function(component, event, helper) { 
        var ParticipationAmount = component.find("ParticipationAmount").get("v.value");        
        if(ParticipationAmount != undefined)
        { 
            var val = helper.numberWithoutCommas(ParticipationAmount); 
            var val1 = helper.numberWithCommas(val);
            component.set("v.SellerOfferObject.ParticipationAmount__c", val1);
        }  
    },
    changeParticipationPercentage: function(component, event, helper) {  
        var ParticipationPercentage = component.find("ParticipationPercentage").get("v.value"); 
        if(ParticipationPercentage != undefined)
        { 
            var val = helper.numberWithoutCommas(ParticipationPercentage); 
            var val1 = helper.round(val, 3); 
			component.set("v.SellerOfferObject.ParticipationPercentage__c", helper.numberWithCommas(val1)); 
        }         
    }, 
    ParticipationAmount: function(component, event, helper) {
        var inp = component.find("ParticipationAmount").get("v.value"); 
        if(inp == 0)
	        component.set("v.SellerOfferObject.ParticipationAmount__c", null);
    },
    ParticipationPercentage: function(component, event, helper) {
        var inp = component.find("ParticipationPercentage").get("v.value"); 
        if(inp == 0.000)
            component.set("v.SellerOfferObject.ParticipationPercentage__c", null);
	},
    saveOffer : function(component, event, helper) { 
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false);  
        var loanType = component.find("loan").get("v.value"); 
        var productTemplate = component.find("productTemplate").get("v.value"); 
        var offerName = component.find("offername").get("v.value");
        
        var participationAmountField = component.find("ParticipationAmount");
        var participationAmount;
        if(participationAmountField){
        	participationAmount = participationAmountField.get("v.value");
        }
        
        var participationPercentageField = component.find("ParticipationPercentage");
        var participationPercentage;
        if(participationPercentageField){
        	participationPercentage = participationPercentageField.get("v.value");
        }
        
        if(loanType == '' || loanType == null || loanType == undefined || loanType.trim().length*1 == 0) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.loanError", 'Input field required'); 
        } else { 
            component.set("v.loanError", ''); 
        }
        
        if(productTemplate == '' || productTemplate == null || productTemplate == undefined || productTemplate.trim().length*1 == 0) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.templateError", 'Input field required'); 
        } else { 
            component.set("v.templateError", ''); 
        }
        
        /*
         * commented by jaya since the name field is going to be auto generated number.
         * 
        if(offerName == '' || offerName == null || offerName == undefined || offerName.trim().length*1 == 0) 
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.nameError", 'Input field required'); 
        }
        else
        { 
            if(helper.checkSpecialCharecter1(offerName))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.nameError", 'Please enter without special characters.'); 
            } 
            else{  
                component.set("v.nameError", ''); 
            } 
        }
       	*/
        
        /*
         * commented by jaya since participation amount is not needed
         * 
        if(participationAmount == undefined || participationAmount == ''){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.participationAmountError", 'Input field required'); 
        } else {   
            var numWithoutComma = helper.numberWithoutCommas(participationAmount);
            if(helper.checkSpecialCharecter(numWithoutComma) || helper.checkCharecter(numWithoutComma)) { 
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.participationAmountError", 'Please enter a numeric value.'); 
            } else {
                if(numWithoutComma <= 0)
                {
                    errorFlag = 'true';
                    component.set("v.validation", true);
                    component.set("v.participationAmountError", 'Enter a value greater than 0'); 
                } else {
                    component.set("v.participationAmountError", '');
                }
            }
        }
		*/
        
        /*
         * commented by jaya since participation percentage is not needed
         * 
        if(participationPercentage == undefined || participationPercentage == '') {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.participationPercentageError", 'Input field required'); 
        } else {
            var numWithoutComma = helper.numberWithoutCommas(participationPercentage);
            if(helper.checkSpecialCharecter(numWithoutComma) || helper.checkCharecter(numWithoutComma)) { 
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.participationPercentageError", 'Please enter a numeric value.'); 
            } else {
                if(isNaN(numWithoutComma) || numWithoutComma <= 0 || numWithoutComma > 99)
                {
                    errorFlag = 'true';
                    component.set("v.validation", true);
                    component.set("v.participationPercentageError", 'Enter a value between 1% - 99%'); 
                } else {
                    component.set("v.participationPercentageError", '');  
                }
            }
        }
        */
        
        if(errorFlag == 'false') {
            var img = component.find("pdfImgloading");
            
            $A.util.removeClass(img,'slds-hide');
            $A.util.addClass(img,'slds-show');  

            component.find("btnSaveTrans").set("v.disabled", true); 
            var customComponent = component.find("MyCustomComponent");
            customComponent.find("recordCreator").saveRecord(function(saveResult) {
                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                    customComponent.set("v.recordId", saveResult.recordId);
                    component.set("v.SellerOfferObject.Record_Id__c", customComponent.get("v.recordId")); 
                    var OfferObject = component.get("v.SellerOfferObject"); 
                    var action = component.get("c.saveSellerOffersItems");
                    action.setParams({ "saveSellerOffers": OfferObject });
                    action.setCallback(this, function(response) {
                        var offerId = response.getReturnValue(); 
                        if(offerId != '') {
                            if(offerId == 'Already Used') {
                                component.find("btnSaveTrans").set("v.disabled", false); 
                                errorFlag = 'true';
                                component.set("v.validation", true);
                                component.set("v.nameError", 'This "Transaction Name" has already been used. Please enter a new Seller Offer Name.');
                                $A.util.addClass(img,'slds-hide'); 
                                $A.util.removeClass(img,'slds-show');
                                
                            } else {
                                window.location = '/demo/s/transaction-detail?transactionId='+ offerId;
                                
                                $A.util.addClass(img,'slds-hide'); 
                                $A.util.removeClass(img,'slds-show');
                                
                            }
                        } else {
                            component.find("btnSaveTrans").set("v.disabled", false); 
                            component.set("v.validation", false);
                            component.set("v.offerIn", false); 
                            
                            $A.util.addClass(img,'slds-hide'); 
                            $A.util.removeClass(img,'slds-show');
                            
                            var showToast = $A.get('e.force:showToast');
                            showToast.setParams(
                                {
                                    'title': 'Failure : ',
                                    'message': 'Unable to Create Transaction',
                                    'type': 'Success'
                                }
                            );
                            showToast.fire();
                        }
                    });
                    $A.enqueueAction(action);                    
                } else if (saveResult.state === "INCOMPLETE") {
                    component.find("btnSaveTrans").set("v.disabled", false); 
                    
                    $A.util.addClass(img,'slds-hide'); 
                    $A.util.removeClass(img,'slds-show');
                    
                    console.log("User is offline, device doesn't support drafts.");
                } else if (saveResult.state === "ERROR") {
                    component.find("btnSaveTrans").set("v.disabled", false); 
                    
                    $A.util.addClass(img,'slds-hide'); 
                    $A.util.removeClass(img,'slds-show');
                    
                    console.log('Problem saving transaction, error: ' + JSON.stringify(saveResult.error));
                } else {
                    component.find("btnSaveTrans").set("v.disabled", false); 
                    
                    $A.util.addClass(img,'slds-hide'); 
                    $A.util.removeClass(img,'slds-show');
                    
                    console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
                }
            });
        }
    },
    hoverCreditUnion: function(component, event) { 
        var action = component.get('c.CUDetails'); 
        action.setParams({
            "CUId" : event.target.id
        });       
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                component.set("v.CUUser", result); 
                component.set("v.popoverCreditUnion", true);
            } 
        });
        $A.enqueueAction(action);
    },
    hoverCreditUnionhide: function(component, event) { 
        component.set("v.popoverCreditUnion", false); 
    },
    PartialPopUp : function(component, event) {
        component.set("v.PartialPopUp", true);
    },
    PartialCancel : function(component, event) {
        component.set("v.PartialPopUp", false);
    },
    handleTemplateChange: function(component, event, helper) {
        debugger;
		component.set('v.templateLoaded','false');
        var selectedTemplateId = component.find("productTemplate").get("v.value"); 
        if(selectedTemplateId != '')
        {
        	component.set("v.templateError", '');
        }

        var loanError = component.get("v.loanError");   
		var templateError = component.get("v.templateError");
        
        if(loanError.length == 0 && templateError.length == 0){
            component.set("v.validation", false);
        }
        
        var action = component.get('c.getTemplateDetail');
        action.setParams({
            "templateId" : selectedTemplateId
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                var result = actionResult.getReturnValue();
                component.set('v.templateLoaded','true');
                $A.createComponent("c:"+result.Component_Name__c,
                {
                    "aura:id": 'MyCustomComponent',
                    "edit": 'true',
                    "mode": 'EDIT',
                    "objectApiName": result.SObject_Name__c,
                    "recordId": ''
                },
                function(newComponent, status, errorMessage){ 
                    var targetCmp = component.find('placeHolder');
                    targetCmp.set("v.body", []); 
                    var body = targetCmp.get("v.body");
                    body.push(newComponent);
                    targetCmp.set("v.body", body); 
                });
            }
        });
        $A.enqueueAction(action);
    },
    handleMarkerTypeChange: function(component, event, helper) {
        var market = event.getSource().get("v.label");
        if(market == 'Primary'){
        	component.set("v.SellerOfferObject.IsPrimary__c", true);
        } else {
            component.set("v.SellerOfferObject.IsPrimary__c", false);
        }
        debugger;
        var productType = component.find("loan").get("v.value"); 
        var isPrimary = component.get("v.SellerOfferObject.IsPrimary__c"); 
        var marketType = isPrimary ? 'Primary' : 'Secondary';
        var action = component.get('c.getProductTemplates');
        action.setParams({
            "productType": productType,
            "marketType": marketType
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                debugger;
                var result = actionResult.getReturnValue();
                component.set("v.productTemplates", result);
                var productTemplates = component.get("v.productTemplates");
                if(productTemplates){
                    if(productTemplates.length>0){
                        component.set("v.SellerOfferObject.TemplateId__c", productTemplates[0].value); 
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },
    handleLoanTypeChange: function(component, event, helper) {
		debugger;
        var productType = component.find("loan").get("v.value"); 
        if(productType != '')
        {
        	component.set("v.loanError", '');    
        }
        var loanError = component.get("v.loanError");   
		var templateError = component.get("v.templateError");
        
        if(loanError.length == 0 && templateError.length == 0){
            component.set("v.validation", false);
        }
        
        var isPrimary = component.get("v.SellerOfferObject.IsPrimary__c"); 
        var marketType = isPrimary ? 'Primary' : 'Secondary';
        var action = component.get('c.getProductTemplates');
        action.setParams({
            "productType": productType,
            "marketType": marketType
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                debugger;
                var result = actionResult.getReturnValue();
                component.set("v.productTemplates", result);
                var productTemplates = component.get("v.productTemplates");
                if(productTemplates){
                    if(productTemplates.length>0){
                        component.set("v.SellerOfferObject.TemplateId__c", productTemplates[0].value); 
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },
})