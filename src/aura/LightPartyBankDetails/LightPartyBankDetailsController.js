({    
    getBank: function(component) {   
        debugger;
        var action = component.get('c.getBankDetails');         
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            }catch(ex){}
        };
         component.set("v.FID",getUrlParameter('flag'));
        var bankId = getUrlParameter('bankId');
        action.setParams({
            "bankId" : bankId
        });
        action.setCallback(this, function(actionResult) { 
            
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){                
                component.set("v.Result", result); 
                component.set("v.bank", result.result);
                component.set("v.ruleList", result.ruleList);
                 component.set("v.CUUsers", result.CUUsers);
                component.set("v.bankId", result.result.Id);
                component.set("v.oldBankName", result.result.CU_Name__c);                
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Result"); 
                component.set("v.Result", emptyTask);
            }
            
            debugger;
            var menu = document.getElementById('Party Banks');
            var classArr = menu.className.split(" ");
            if (classArr.indexOf("current") == -1) {
                menu.className += " " + "current";
            }
            
        }); 
        $A.enqueueAction(action);
        var ActionTitle = component.get("c.getloggeduserBrowserTitle");
ActionTitle.setCallback(this, function(actionResultTitle) {
var statetitle = actionResultTitle.getState(); 
var resulttitle = actionResultTitle.getReturnValue();
if(statetitle === "SUCCESS"){
debugger;
document.title =resulttitle +' :: Party Bank Detail';
}
});
$A.enqueueAction(ActionTitle);

    },
    editBank: function(component, event, helper) {   
        component.set("v.showEdit", true);     
    },
    cancel: function(component, event, helper) {
        component.set("v.showEdit", false);
        component.set("v.validation", false);
    }, 
    editBankDetail: function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
		var bankd = component.get("v.bank");
        var action = component.get("c.CheckBankName");
        action.setParams({ "bankName": bankd.CU_Name__c, "oldBankName": component.get("v.oldBankName") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var bank = response.getReturnValue(); 
            if(bank != '' && bank != undefined) {
                if(bank == 'Already Used') { 
                    errorFlag = 'true';
                    component.set("v.validation", true); 
                    component.set("v.NameError", 'This Bank Name has already been used. Please enter a new Bank Name.');
                } else {
                    component.set("v.NameError", ''); 
                }
            } else { 
                component.set("v.NameError", ''); 
            }
            if(errorFlag == 'false') {
                component.find("btnEditBank").set("v.disabled", true);                    
                var action1 = component.get("c.EditBank");
                action1.setParams({
                    "creditUnion" : bankd
                }); 
                action1.setCallback(this, function(response1) {
                    var state1 = response1.getState();
                    var bankId = response1.getReturnValue(); 
                    debugger;
                    component.find("btnEditBank").set("v.disabled", false);
                    if(bankId != '' && bankId != undefined) {
                        if(bankId == 'Already Used'){ 
                            component.set("v.validation", true);                     
                            component.set("v.NameError", 'This Bank Name has already been used. Please enter a new Bank Name.');
                        } else {
                            window.location = '/demo/s/bank-detail?bankId='+ bankId;
                        }
                    }
                    else{ 
                        component.set("v.validation", false); 
                        component.set("v.showCreate", false); 
                    }
                    
                });
                $A.enqueueAction(action1);
            }
        });
        $A.enqueueAction(action); 
    },
     editBankshow: function(component, event, helper) {   
        component.set("v.showEdit1", true);     
    },
    cancel: function(component, event, helper) {
        component.set("v.showEdit1", false);
        component.set("v.validation", false);
    }, 
     inactiveCheck: function(component, event, helper) {
        if(component.find("chkInactive").get('v.value')==true)
        {
            debugger;
            component.find("btnEditBank").set("v.disabled", "false");
        }
        else{
            debugger;
            component.find("btnEditBank").set("v.disabled", "true");
        }
    },
    editBankDetail: function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
		var bankd = component.get("v.bank");
       
            if(errorFlag == 'false') {
                component.find("btnEditBank").set("v.disabled", true);                    
                var action1 = component.get("c.EditBank1");
                action1.setParams({
                    "creditUnion" : bankd
                }); 
                action1.setCallback(this, function(response1) {
                    var state1 = response1.getState();
                    var bankId = response1.getReturnValue(); 
                    debugger;
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Party Bank has been activated.',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                    component.set("v.showEdit1", false);
                    $A.get('e.force:refreshView').fire();
                });
                $A.enqueueAction(action1);
            }
        
    },
    RejectCPRequestpop: function(component, event, helper) {   
        component.set("v.showRCPRequest", true);     
    },
    cancelCPRequest: function(component, event, helper) {
        component.set("v.showRCPRequest", false);
        component.set("v.validation", false);
    }, 
    RejectCHK: function(component, event, helper) {
        if(component.find("chkreject").get('v.value')==true)
        {
            debugger;
            component.find("btnEditBank").set("v.disabled", "false");
        }
        else{
            debugger;
            component.find("btnEditBank").set("v.disabled", "true");
        }
    },
    RejectCP: function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
		var bankd = component.get("v.bank");
       
            if(errorFlag == 'false') {
                component.find("btnEditBank").set("v.disabled", true);                    
                var action1 = component.get("c.RejectCPForAdmin");
                action1.setParams({
                    "creditUnion" : bankd,
                    "Reason" :  component.find("body5").get("v.value")
                }); 
                action1.setCallback(this, function(response1) {
                    var state1 = response1.getState();
                    var bankId = response1.getReturnValue(); 
                    debugger;
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'CounterParty Bank has been Rejected.',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                    component.set("v.showRCPRequest", false);
                    $A.get('e.force:refreshView').fire();
                });
                $A.enqueueAction(action1);
            }
        
    }
})