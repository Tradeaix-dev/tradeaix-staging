({ 
    getloginuser : function(component, event, helper){
        debugger;  
        //alert(new Date().getTime());
        var Action = component.get("c.getUsername");         
        var sessionid = localStorage.getItem("UserSession");
        //alert(sessionid);
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            } catch(ex){}
        };
        
        var Tid = getUrlParameter('Tids');
        if(getUrlParameter('transactionId') !=undefined){
         var Action1 = component.get("c.getTransactions");   
        Action1.setParams({
            "TID" : getUrlParameter('transactionId')                
        });
        Action1.setCallback(this, function(actionResult1) {
            debugger;
            var state = actionResult1.getState(); 
            var results = actionResult1.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.transrefnumber",results);
                }
        });
        $A.enqueueAction(Action1);
        }  
        //alert(Tid);
        localStorage.setItem("Tid");
        component.set("v.Flag",localStorage.getItem("Flag"));
        var processed = getUrlParameter('processed');
        localStorage.setItem("processed");
        Action.setParams({
            "username" : sessionid                
        });
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                localStorage.setItem("LoggeduserProfile", results[0].Profiles__c);
                localStorage.setItem("IDTenant", results[0].Id);
                component.set("v.UM",results[0]);
                localStorage.setItem("LoggeduserTenantID", results[0].Tenant__c);
                localStorage.setItem("LoggeduserOrgID", results[0].Tenant__r.Organization__c);
                localStorage.setItem("LoggeduserName", results[0].First_Name__c+' '+results[0].Last_Name__c);
                localStorage.setItem("TaskAssigneeloggeduser", results[0].Task_Assignee__c);
                localStorage.setItem("TaskviewAssigneeloggeduser", results[0].View_Transaction__c);
                component.set("v.loginusername",results[0].First_Name__c+' '+results[0].Last_Name__c);
                localStorage.setItem("TSiteName", results[0].Tenant__r.Tenant_Short_Name__c);
                component.set("v.TenantID",results[0].Tenant__r.Tenant_Short_Name__c);
                if(results[0].Tenant__r.Tenant_Logo_Url__c==undefined)
                {
                    component.set("v.LogoURL",$A.get("{!$Label.c.TradeAix_Logo}"));
                }else{
                    component.set("v.LogoURL",results[0].Tenant__r.Tenant_Logo_Url__c);
                }
                if(results[0].Task_Assignee__c == true){
                    component.set("v.LogoURL",$A.get("{!$Label.c.TradeAix_Logo}"));
                }
                component.set("v.emailid",results[0].User_Email__c);
                component.set("v.pwd",results[0].Password__c);  
                localStorage.setItem("FromRaidUserPWD", results[0].Password__c);
                localStorage.setItem("FromRaidCHK", results[0].CreatedFromrapid__c);
                localStorage.setItem("LoggeduserTenantType", results[0].Tenant__r.Tenant_Type__c);
                localStorage.setItem("BPuser",results[0].Become_a_Premium_user__c);
               
            }
        });
        $A.enqueueAction(Action);
        
        var action = component.get('c.getOTPinterval');
        var self = this; 
        action.setParams({
            "ID" : localStorage.getItem("LBID")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            debugger;
            if(state === "SUCCESS"){
                if(localStorage.getItem("LoggeduserProfile") == $A.get("{!$Label.c.OBCP_ProfileID}"))
                {
                    if(result!=null){                   
                    if(result.ExpDate!=null){
                        component.set("v.endTime",result.ExpDate);
                        if(result.ValDate ==null){
                            component.set("v.OTPPopup",true);
                        }else{
                            component.set("v.OTPPopup",false);}
                        var timer = setInterval(function() {                             
                            var now = new Date().getTime();
                            var fiveminus = new Date(component.get("v.endTime")).getTime();
                            var distance = fiveminus - now;
                            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                            var result =  minutes + "m " + seconds + "s ";
                            component.set("v.expOTPtxt",false);
                            component.set("v.OTPtxt",true);
                            
                            if(result=='0m 0s ' || now > fiveminus){ 
                                component.set("v.chkerrorotpexpire",'0');
                                clearInterval(timer);
                                
                                component.set("v.OTPPopup",true);
                                component.set("v.expOTPtxt",true);
                                component.set("v.OTPtxt",false);
                                
                                var action = component.get('c.UpdateLB');
                                var self = this; 
                                action.setParams({
                                    "ID" : localStorage.getItem("LBID"),
                                    "resendLB" : 'No'
                                });
                                action.setCallback(this, function(actionResult) { 
                                    var state = actionResult.getState(); 
                                    var result = actionResult.getReturnValue();
                                    debugger;
                                    if(state === "SUCCESS"){
                                        
                                    }
                                }); 
                                $A.enqueueAction(action);
                                
                            }else{
                                
                                component.set("v.chkerrorotpexpire",result);
                            }
                            
                        }, 500);
                    }else{
                        component.set("v.chkerrorotpexpire",'0');
                        clearInterval(timer);
                        component.set("v.OTPPopup",true);
                        component.set("v.expOTPtxt",true);
                        component.set("v.OTPtxt",false);
                    }
                    }else{
                        component.set("v.chkerrorotpexpire",'0');
                        clearInterval(timer);
                        component.set("v.OTPPopup",true);
                        component.set("v.expOTPtxt",true);
                        component.set("v.OTPtxt",false);
                    }
                }
            }
        }); 
        $A.enqueueAction(action);
        
        
        
        
        var action1 = component.get('c.getNotificationCount');
        action1.setParams({
            "LoggedUserId" : localStorage.getItem("IDTenant")                
        });
        action1.setCallback(this, function(actionResult1) {  
            var state1 = actionResult1.getState(); 
            var result1 = actionResult1.getReturnValue();
            if(state1 === "SUCCESS"){                 
                component.set("v.total_size",result1);
            }
        }); 
        $A.enqueueAction(action1); 
    },
    
    sidebarCollapse : function(component, event, helper) {
        var toggleText = component.find("sidebar");
        $A.util.toggleClass(toggleText, 'active');
    },
    myAction : function(component, event, helper) {
        
    },
    onSingleSelectChange: function(component, event, helper) {
        debugger;
        var selectCmp = component.find("InputSelectSingle").get("v.value");    
         var site = $A.get("{!$Label.c.Org_URL}");
        if(selectCmp == 'Logout'){
            var Action = component.get("c.userLogout");
            var sessionname = localStorage.getItem("UserSession");
            Action.setParams({
                "username" : sessionname
            });
            Action.setCallback(this, function(actionResult) {
                debugger;
                var state = actionResult.getState(); 
                var results = actionResult.getReturnValue();
                if(state === "SUCCESS"){
                    localStorage.removeItem('UserSession');
                    localStorage.removeItem('LoggeduserProfile');
                    localStorage.removeItem('IDTenant');
                    localStorage.removeItem('LoggeduserTenantID');
                    localStorage.removeItem('LoggeduserOrgID');
                    localStorage.removeItem('LoggeduserName');
                    localStorage.removeItem('TSiteName');
                    localStorage.removeItem('FromRaidUserPWD');
                    localStorage.removeItem('FromRaidCHK');
                    localStorage.removeItem('LoggeduserTenantType');
                    localStorage.removeItem('BPuser');
                   
                    if(document.URL.split('/')[3] ==site)
                    {
                        window.location.replace("https://tradeaixqa-stratizantqa.cs93.force.com/demo/secur/logout.jsp?retUrl=https%3A%2F%2Ftradeaixqa-stratizantqa.cs93.force.com%2Fdemo%2Fs%2F"+component.get("v.TenantID"));
                    }else{
                        window.location.replace("https://tradeaixqa-stratizantqa.cs93.force.com/demo/secur/logout.jsp?retUrl=https%3A%2F%2Ftradeaixqa-stratizantqa.cs93.force.com%2Fdemo%2Fs%2F"+component.get("v.TenantID"));
                    }
                }
            });
            $A.enqueueAction(Action);
        }
        if(selectCmp == 'My Profile'){
            window.location = '/'+site+'/s/profile';
        }
    },
    valdate : function(component, event, helper) {       
        if(component.find("txt1").get('v.value') !='' && component.find("txt2").get('v.value') !='' && component.find("txt3").get('v.value') !='' && component.find("txt4").get('v.value') !='' && component.find("txt5").get('v.value') !='' && component.find("txt6").get('v.value') !='')
        {
            var regPhoneNo = /^[0-9]*$/;
            if(component.find("txt1").get('v.value').match(regPhoneNo) && component.find("txt2").get('v.value').match(regPhoneNo) && component.find("txt3").get('v.value').match(regPhoneNo) && component.find("txt4").get('v.value').match(regPhoneNo) && component.find("txt5").get('v.value').match(regPhoneNo) && component.find("txt6").get('v.value').match(regPhoneNo))
            {
                var BidderOTP = component.find("txt1").get('v.value') + component.find("txt2").get('v.value') +component.find("txt3").get('v.value')+component.find("txt4").get('v.value')+component.find("txt5").get('v.value') + component.find("txt6").get('v.value');
                var action = component.get('c.OTPMatch');
                var self = this; 
                action.setParams({
                    "ID" : localStorage.getItem("LBID"),
                    "OTP" : BidderOTP
                });
                action.setCallback(this, function(actionResult) { 
                    var state = actionResult.getState(); 
                    var result = actionResult.getReturnValue();
                    debugger;
                    if(state === "SUCCESS"){
                        
                        if(result== 'MATCH'){
                            
                            component.set("v.OTPPopup",false);
                            
                        }else{
                            
                             component.set("v.chkotperror","Error: Please Entire Correct OTP.");
                            //component.set("v.chkotperror","Error: 6 digit OTP should be in only digits.");
                        }
                    }
                }); 
                $A.enqueueAction(action);
            }
            else{
                component.set("v.chkotperror","Error: Please Enter the 6 digit OTP.");
            }
        }else{
            
            
            component.set("v.chkotperror","Error: 6 digit OTP should be in only digits.");
        }
        
    },
    resend: function(component, event, helper) {        
        debugger;
        var action = component.get('c.UpdateLB');
        var self = this; 
        action.setParams({
            "ID" : localStorage.getItem("LBID"),
            "resendLB" : 'Yes'
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();           
            if(state === "SUCCESS"){
                	component.set("v.endTime",new Date().getTime()+5*60000);
                	var timer = setInterval(function() { 
                    var now = new Date().getTime();
                    var fiveminus = new Date(component.get("v.endTime"));
                    var distance = fiveminus - now;
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                    var result =  minutes + "m " + seconds + "s ";
                    component.set("v.expOTPtxt",false);
                    component.set("v.OTPtxt",true);
                    if(result=='0m 0s '){ 
                        component.set("v.chkerrorotpexpire",'0');
                        clearInterval(timer); 
                        
                        component.set("v.expOTPtxt",true);
                        component.set("v.OTPtxt",false);
                        var action = component.get('c.UpdateLB');
                        var self = this; 
                        action.setParams({
                            "ID" : localStorage.getItem("LBID"),
                            "resendLB" : 'No'
                        });
                        action.setCallback(this, function(actionResult) { 
                            var state = actionResult.getState(); 
                            var result = actionResult.getReturnValue();
                            debugger;
                            if(state === "SUCCESS"){
                                // alert('');
                            }
                        }); 
                        $A.enqueueAction(action);
                    }else{
                        component.set("v.chkerrorotpexpire",result);
                    }
                    
                }, 500);
            }
        }); 
        $A.enqueueAction(action);
        
    },
      updateinfo : function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        var newPassword = component.find("newPassword").get("v.value");
        var confirmPassword = component.find("confirmPassword").get("v.value"); 
        var re = new RegExp("^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$");
       
        if(newPassword == undefined || newPassword == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.newpassworderror",'Input field required');
        }else{
            component.set("v.newpassworderror",'');
        }
        if(confirmPassword == undefined || confirmPassword == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.confirmpassworderror",'Input field required');
        }
          else if(newPassword != confirmPassword){  
              errorFlag = 'true';
              component.set("v.confirmpassworderror",'');
              component.set("v.changepasswordvalidation",true);
              component.set("v.validation", false);
              component.set("v.combinationvalidation",false);
          }
              else if (!re.test(confirmPassword)) {
                  errorFlag = 'true';
                  component.set("v.combinationvalidation",true);
                  component.set("v.changepasswordvalidation",false);
                  component.set("v.validation",false);
              }
          
          
        if(errorFlag == 'false') {
            var action = component.get('c.UpdateLBFROMMain');
            var self = this; 
            action.setParams({
                "ID" : localStorage.getItem("UID"),
                "UMgmt" : component.get("v.UM"),
                "newPassword" : newPassword
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                debugger;
                if(state === "SUCCESS"){
                    component.set("v.OTPPopup",false);
                    component.set("v.Customer",false);
                    component.set("v.CustomerConfirmationpop",true);
                    var showToast = $A.get('e.force:showToast');
                showToast.setParams(
                    {
                        'title': 'Success: ',
                        'message': 'Successfully Registered Premium Customer. You will get the user activation email shortly.',
                        'type': 'Success'
                    }
                );
                showToast.fire();
                    $A.get('e.force:refreshView').fire();
                }
            }); 
            $A.enqueueAction(action);    
        }
          
        
    },
    afterregister: function(component, event, helper) {
        component.set("v.CustomerConfirmationpop",false);
        component.set("v.OTPPopup",false);
        component.set("v.Customer",false);
    },
    cancelupdateinfo: function(component, event, helper) {
        component.set("v.Customer",false);
    },
    ontxt1 : function(component, event, helper) {
        component.find('txt2').focus();
    },
    ontxt2 : function(component, event, helper) {
        component.find('txt3').focus();
    },
    ontxt3 : function(component, event, helper) {
        component.find('txt4').focus();
    },
    ontxt4 : function(component, event, helper) {
        component.find('txt5').focus();
    },
    ontxt5 : function(component, event, helper) {
        component.find('txt6').focus();
    },
    customer: function(component, event, helper) {
        debugger;
        component.set("v.Customer",true);
        component.set("v.OTPPopup",false);
    },
})