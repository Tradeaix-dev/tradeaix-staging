({
	getReportTitle : function() {
		var length = document.URL.split('/').length;
        var title = document.URL.split('/')[length - 1]; 
        title = title.split('?')[0]; 
        var str = title.replace(/\-/g, function(letter) {
            return ' ';
        }); 
        str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        });            
        var reportTitle = '';
        for(var i = 0; i < str.split(' ').length; i++){
            
            var repTitle = str.split(' ')[i];
            
            if(repTitle == 'Nda' || repTitle == 'nda')
                repTitle = 'NDA';
            
            if(repTitle == 'Tc' || repTitle == 'tc' || repTitle == 't&c' || repTitle == 'T&c')
                repTitle = 'T&C';  
            
            if(i == 0)
                reportTitle = repTitle;
            else
                reportTitle = reportTitle + ' ' + repTitle;
        }
        return reportTitle;
	}
})