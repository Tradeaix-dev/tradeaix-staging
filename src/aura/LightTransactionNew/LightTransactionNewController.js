({
    getOfferList: function(component) { 
        debugger;
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1;
        var yyyy = today.getFullYear();
        if(dd > 9){var date = dd;}else{var date = '0'+dd;}
         if(mm > 9){var month = mm;}else{var month = '0'+mm;}
        component.set("v.currentDate",yyyy+'-'+month+'-'+date);
        
       // component.set("v.pastDate",yyyy+'-'+today.getMonth()+'-'+date);
        component.set("v.LoggeduserTenantType", localStorage.getItem('LoggeduserTenantType'));
        
        var action1 = component.get('c.getProductTemplates');
        action1.setParams({
            "Tid" : localStorage.getItem('LoggeduserTenantID')
        });
        action1.setCallback(this, function(actionResult1) {
            var state1 = actionResult1.getState(); 
            if(state1 === "SUCCESS"){  
                debugger;
                component.find("rbtnFunded").set("v.disabled", true);
                component.find("rbtnUnFunded").set("v.disabled", true);
                /* component.find("rbtnPrimary").set("v.disabled", true);
                component.find("rbtnSecondary").set("v.disabled", true);*/
                var result1 = actionResult1.getReturnValue();
                component.set("v.productTemplates", result1);
                var productTemplates = component.get("v.productTemplates");
                if(productTemplates){
                    if(productTemplates.length>0){
                        component.set("v.SellerOfferObject.Product_Template__c", productTemplates[0].value);
                    }
                }
            }
        });
        $A.enqueueAction(action1);
        
        var CP = component.get("c.getCP");
        CP.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state == 'SUCCESS') {   
                var CP = response.getReturnValue();
                var options = [];
                CP.forEach(function(CPdata)  { 
                    //alert(CPdata.Id);
                    options.push({ value: CPdata.Id, label: CPdata.Tenant_Site_Name__c});
                    
                });
                component.set("v.listOptions", options);
            } else {
                console.log('Failed with state: ' + state);
            }
        });
        $A.enqueueAction(CP); 
        
        //var menu = document.getElementById('Transactions');
        //var classArr = menu.className.split(" ");
        //if (classArr.indexOf("current") == -1) {
        //    menu.className += " " + "current";
        // }
        var ActionTitle = component.get("c.getloggeduserBrowserTitle");
        ActionTitle.setCallback(this, function(actionResultTitle) {
            var statetitle = actionResultTitle.getState(); 
            var resulttitle = actionResultTitle.getReturnValue();
            if(statetitle === "SUCCESS"){
                debugger;
                document.title = 'Create Transaction';
            }
        });
        $A.enqueueAction(ActionTitle);
        
    }, 
    handleMarkerTypeChange: function(component, event, helper) {
        debugger;
        component.set('v.templateLoaded','false');
        var market = event.getSource().get("v.label");
        var pTyperbtnFunded = component.find("rbtnFunded").get("v.value"); 
        var pTyperbtnUnFunded = component.find("rbtnUnFunded").get("v.value"); 
        if(market != '')
        {
            component.set("v.MytypeError", '');    
        }
        if(pTyperbtnFunded){
            var prodType='Funded';
        }
        else if(pTyperbtnUnFunded){
            var prodType='UnFunded';
        }
        if(pTyperbtnFunded || pTyperbtnUnFunded) {
            debugger;
            var action = component.get('c.getProductTemplates');
            action.setCallback(this, function(actionResult) {
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){  
                    debugger;
                    var result = actionResult.getReturnValue();
                    component.set("v.productTemplates", result);
                    var productTemplates = component.get("v.productTemplates");
                    if(productTemplates){
                        if(productTemplates.length>0){
                            component.set("v.SellerOfferObject.Product_Template__c", productTemplates[0].value);
                        }
                    }
                }
            });
            $A.enqueueAction(action);
        }
    },
    handleTemplateChange: function(component, event, helper) {
        debugger;
        component.set('v.templateLoaded','false');
        var selectedTemplateId = component.find("productTemplate").get("v.value"); 
        if(selectedTemplateId != '')
        {
            component.set("v.templateError", '');
        }
        var action = component.get('c.getTemplateDetail');
        action.setParams({
            "templateId" : selectedTemplateId
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                var result = actionResult.getReturnValue();
                component.set("v.jsonmsg",result.PT.Transactiondetailfields__c);
                component.set("v.Templateobj",result.strList);
                component.set('v.templateLoaded','true');
                if(result.PT.IsPrimary__c)
                {component.set("v.mtypep",true);
                 component.set("v.mtypes",false);
                }else{
                    component.set("v.mtypes",true); 
                    component.set("v.mtypep",false);
                }
                if(result.PT.ProductType__c=='Funded')
                {
                    component.set("v.Ptypef",true);
                    component.set("v.Ptypeunf",false);    
                }else{
                    component.set("v.Ptypef",false);
                    component.set("v.Ptypeunf",true);                    
                }
                /* $A.createComponent("c:"+result.Component_Name__c,
                                   {
                                       "aura:id": 'MyCustomComponent',
                                       "edit": 'true',
                                       "mode": 'EDIT',
                                       "objectApiName": result.SObject_Name__c,
                                       "recordId": ''
                                   },
                                   function(newComponent, status, errorMessage){ 
                                       var targetCmp = component.find('placeHolder');
                                       targetCmp.set("v.body", []); 
                                       var body = targetCmp.get("v.body");
                                       body.push(newComponent);
                                       targetCmp.set("v.body", body); 
                                   });*/
            }
        });
        $A.enqueueAction(action);
    },
    handleProductTypeChange: function(component, event, helper) {
        debugger;
        component.set('v.templateLoaded','false');
        var productType = event.getSource().get("v.label"); 
        if(productType != '')
        {
            component.set("v.loanError", '');    
        }
        var loanError = component.get("v.loanError");   
        var templateError = component.get("v.templateError");
        
        /*if(loanError.length == 0 && templateError.length == 0){
            component.set("v.validation", false);
        }*/
        var mTyperbtnPrimary = component.find("rbtnPrimary").get("v.value"); 
        var mTyperbtnSecondary = component.find("rbtnSecondary").get("v.value"); 
        
        if(mTyperbtnPrimary){
            var market='Primary';
        }
        else if(mTyperbtnSecondary){
            var market='Secondary';
        }
        if(mTyperbtnPrimary || mTyperbtnSecondary) {
            var action = component.get('c.getProductTemplates');
            action.setParams({
                "productType": productType,
                "marketType": market
            });
            action.setCallback(this, function(actionResult) {
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){  
                    debugger;
                    var result = actionResult.getReturnValue();
                    component.set("v.productTemplates", result);
                    var productTemplates = component.get("v.productTemplates");
                    if(productTemplates){
                        if(productTemplates.length>0){
                            component.set("v.SellerOfferObject.Product_Template__c", productTemplates[0].value); 
                            
                        }
                    }
                }
            });
            $A.enqueueAction(action);
        }
    },
    handleChange: function (component, event, helper) {
        debugger;
        var selectedFields = event.getParam("value");
        var targetName = event.getSource().get("v.name");
        if(targetName == 'srcFields'){ 
            component.set("v.selectedOptions", selectedFields);
        }
    },
    cancel : function(component, event) {
        component.set("v.loanError", '');
        component.set("v.MytypeError", '');
        
        component.set("v.templateError", '');
        component.set("v.nameError", '');
        component.set("v.participationAmountError", '');
        component.set("v.participationPercentageError", '');
        component.set("v.SellerOfferObject.Name", null);
        component.set("v.SellerOfferObject.OfferType__c", "-- None --");
        component.set("v.SellerOfferObject.IsPrimary__c", true);
        component.set("v.SellerOfferObject.Seller_Notes__c", null); 
        component.set("v.SellerOfferObject.ParticipationAmount__c", null);
        component.set("v.SellerOfferObject.ParticipationPercentage__c", null);
        component.set("v.SellerOfferObject.Record_Id__c", null);
        var productTemplates = component.get("v.productTemplates");
        if(productTemplates){
            if(productTemplates.length > 0){
                component.set("v.SellerOfferObject.Product_Template__c", productTemplates[0].value); 
            }
        }
    },
    onSingleSelectChange: function(component, event, helper) {
        debugger;
        component.set("v.Currency",component.find("InputSelectSingle").get("v.value"));
    },
    saveOffer : function(component, event, helper) { 
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        
        var pTyperbtnFunded = component.find("rbtnFunded").get("v.value"); 
        var pTyperbtnUnFunded = component.find("rbtnUnFunded").get("v.value"); 
        var productTemplate = component.find("productTemplate").get("v.value");
        
        if(pTyperbtnFunded){
            var loanType='Funded';
        }
        else if(pTyperbtnUnFunded){
            var loanType='UnFunded';
        }
        if(loanType == '' || loanType == null || loanType == undefined || loanType.trim().length*1 == 0) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.loanError", 'Input field required'); 
        } else { 
            component.set("v.loanError", ''); 
        }
        if(component.get("v.LoggeduserTenantType")=='Corporate'){
            
            var SellNotesib = component.find("seller_notesib").get("v.value");
            if(SellNotesib == '' || SellNotesib == null || SellNotesib == undefined || SellNotesib.trim().length*1 == 0) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.SellNotesib", 'Input field required'); 
            } else { 
                component.set("v.SellNotesib", ''); 
            }
            
            var IB = component.get("v.selectedOptions");
            if(IB.length ==0){
                if(IB == '' || IB == null || IB == undefined) {
                    errorFlag = 'true';
                    component.set("v.validation", true);
                    component.set("v.IBError", 'Input field required'); 
                }
            } else { 
                component.set("v.IBError", ''); 
            }
        }else{
            var SellNotes = component.find("seller_notes").get("v.value");
            if(SellNotes == '' || SellNotes == null || SellNotes == undefined || SellNotes.trim().length*1 == 0) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.SellNotes", 'Input field required'); 
            } else { 
                component.set("v.SellNotes", ''); 
            }
        }
        
        
        if(productTemplate == '' || productTemplate == null || productTemplate == undefined || productTemplate.trim().length*1 == 0) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.templateError", 'Input field required'); 
        } else { 
            component.set("v.templateError", ''); 
        }
        var Errormsg ='';
        var Attoptions = [];
        var TemObj = component.get("v.Templateobj");
        var ExpDate=''; var DoSDate='';
        for(var k = 0; k < TemObj.length; k++) {
            var T = TemObj[k].Attributes__r.Name;
            var D = document.getElementById(T).value;
            if(TemObj[k].Attributes__r.Mandatory__c){
                if(document.getElementById(T).value=='')
                {Errormsg =Errormsg+T+' , ';}
            }
            if(TemObj[k].Attributes__r.Attribute_Type__c=='Number')
            {
                var regPhoneNo = /^[0-9]*$/;
                if(!$A.util.isEmpty(D))
                {   
                    
                    if(!D.match(regPhoneNo))
                    {  
                        Errormsg =Errormsg+T+' should accept only number , ';
                    } 
                    
                }
                
            }  
             if(TemObj[k].Attributes__r.Attribute_Type__c=='DateTime')
            {
                if(D!='')
                {
                    if(Date.parse(D) < Date.parse(component.get("v.currentDate")))
                    {
                        var today = new Date();
                        var dd = today.getDate();
                        var mm = today.getMonth()+1;
                        var yyyy = today.getFullYear();
                        if(dd > 9){var date = dd;}else{var date = '0'+dd;}
                        if(mm > 9){var month = mm;}else{var month = '0'+mm;}
                        Errormsg =Errormsg+T+' must be '+date+'-'+month+'-'+yyyy+' or later , ';
                    }
                   
                }
                if(T!='' && T=='Date of Expiry')
                {
                   ExpDate = D; 
                }
                if(T!='' && T =='Date of Shipment')
                {
                   DoSDate = D; 
                }
            }
            Attoptions.push(T+':'+D+':'+TemObj[k].Attributes__r.Attribute_Type__c);
        }
        
        var idListJSON=JSON.stringify(Attoptions);
       
        if(Date.parse(ExpDate) < Date.parse(DoSDate))
        {
            Errormsg =Errormsg+'Date of Shipment should not be greater than Date of Expiry';
        }
        if(Errormsg != '')
        {errorFlag = 'true';
         component.set("v.FullErrormsg", Errormsg);
         component.set("v.validation", true);
        }
        else{
            component.set("v.FullErrormsg", '');
            component.set("v.validation", false);
        }
        
        if(errorFlag == 'false') {        
            component.set("v.showProgress",true);
            var img = component.find("pdfImgloading");
            $A.util.removeClass(img,'slds-hide');
            $A.util.addClass(img,'slds-show');  
            
            component.find("btnSaveTrans").set("v.disabled", true); 
            var OfferObject = component.get("v.SellerOfferObject");
            OfferObject.IsPrimary__c=true;
            OfferObject.Tenant__c = localStorage.getItem("LoggeduserTenantID");
            
            var pTyperbtnFunded = component.find("rbtnFunded").get("v.value"); 
            var pTyperbtnUnFunded = component.find("rbtnUnFunded").get("v.value"); 
            
            if(pTyperbtnUnFunded){
                OfferObject.Loan_Type__c ='UnFunded';
            }
            else if(pTyperbtnFunded){
                OfferObject.Loan_Type__c ='Funded';
            } 
            //alert(OfferObject.Loan_Type__c);
            var Cvar = component.get("v.Currency");
            if(Cvar == undefined)
            {
                OfferObject.Currency__c='USD';
            }else{
                OfferObject.Currency__c=Cvar;
            }
            
            OfferObject.CreatedBy__c = localStorage.getItem('UserSession');
            OfferObject.LastModifiedBy__c = localStorage.getItem('UserSession');
            debugger;
            
            var action = component.get("c.saveSellerOffersItems");
            action.setParams({ "saveSellerOffers": OfferObject,
                              "IB" : component.get("v.selectedOptions"),
                              "RID" : idListJSON});
            action.setCallback(this, function(response) {
                debugger;
                var offerId = response.getReturnValue();
                if(offerId.includes("GT-") &&  offerId!= '') {
                    var site = $A.get("{!$Label.c.Org_URL}");
                    window.location = '/'+site+'/s/transactionsummary?ids='+ offerId;
                }
                else{
                    var site = $A.get("{!$Label.c.Org_URL}");
                    window.location = '/'+site+'/s/transactionviewdetails?transactionId='+ offerId;
                }
            });
            $A.enqueueAction(action); 
        }
    },
})