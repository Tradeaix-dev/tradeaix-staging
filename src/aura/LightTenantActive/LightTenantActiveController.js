({
	getTenantlist : function(component, event, helper) {
		debugger;
        
          component.set("v.userinfo",true);
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            }catch(ex){}
        }; 
        var tId = getUrlParameter('tenantid'); 
        component.set("v.tenantId",tId);
        var Action = component.get("c.getTenantdetails");
         component.set("v.UserID",getUrlParameter('id'));
        var sessionname = localStorage.getItem("UserSession");   
        Action.setParams({
            "tenantId" : tId,
            "username" : getUrlParameter('id')
        });
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state == "SUCCESS"){
                component.set("v.Tenant",results.tenant);
                component.set("v.UM",results.userLists[0]);
                
            }
        });
        $A.enqueueAction(Action);
	},
    nexttosetpwd: function(component, event) { 
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        
        //var FirstName = component.find("FirstName").get("v.value");
        //var sitename = component.find("sitename").get("v.value");
       // var email = component.find("email").get("v.value");
        //var lastname = component.find("lastname").get("v.value");
        //var swiftcode = component.find("swiftcode").get("v.value");
        //var location = component.find("location").get("v.value");
        var phone = component.find("phone").get("v.value");   
        var Mobile = component.find("Mobile").get("v.value");  
        
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var regPhoneNo = /^[0-9]*$/;
        
        
        /*if(FirstName == '' || FirstName == 'undefined' || FirstName == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.FirstNameError", 'Input field required'); 
        } else { 
            component.set("v.FirstNameError", ''); 
        } 
        
        if(sitename == '' || sitename == 'undefined' || sitename == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LegalNameError", 'Input field required'); 
        } else { 
            component.set("v.LegalNameError", ''); 
        } 
        
        if(lastname == '' || lastname == 'undefined' || lastname == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LastNameError", 'Input field required'); 
        } else { 
            component.set("v.LastNameError", ''); 
        } 
        if(swiftcode == '' || swiftcode == 'undefined' || swiftcode == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.SwiftCodeError", 'Input field required'); 
        } else { 
            component.set("v.SwiftCodeError", ''); 
        } */
        if(!$A.util.isEmpty(phone))
        {   
            
            if(!phone.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Please enter valid phone number'); 
            } 
            else if(phone.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.PhoneError", ''); 
                }
        }
        if(!$A.util.isEmpty(Mobile))
        {   
            
            if(!Mobile.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.MobileError", 'Please enter valid Mobile number'); 
            } 
            else if(Mobile.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.MobileError", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.MobileError", ''); 
                }
        }
        if(errorFlag == 'false') {
            var action = component.get('c.UpdateRegForm1');  
            action.setParams({
                "LoggedUserID" : component.get("v.UserID"),            
                "UM" : component.get("v.UM")
            });
            debugger;
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                if(state === "SUCCESS"){
                   component.set("v.userinfo",false);
                   component.set("v.Tenantinfo",true);
                    
                }
            });
            debugger;
            $A.enqueueAction(action);
        }
    },
    saveTenant : function(component, event, helper) {
		debugger;
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false);
        
        var Tenantnameval = component.find("Tenantnameval").get("v.value");
        var Tenantsitenameval = component.find("Tenantsitenameval").get("v.value");
        var Tenanttyppeval = component.find("Tenanttyppeval").get("v.value");
        if(component.get("v.EnableBanktype")){
            var Tenantbanktyppeval = component.find("Tenantbanktyppeval").get("v.value");
        }
        var Tenantdatavisibilityval = component.find("Tenantdatavisibilityval").get("v.value");
        var TenantNewsmsgURLVal = component.find("TenantNewsmsgURLVal").get("v.value");
        var Tenantthkval = component.find("Tenantthkval").get("v.value");
        var Tenantshiftcodeval = component.find("Tenantshiftcodeval").get("v.value");
        var TenantWebsiteval = component.find("TenantWebsiteval").get("v.value");
        var Tanantlandingval = component.find("Tanantlandingval").get("v.value");
        var Tenantlogoval = component.find("Tenantlogoval").get("v.value");
        var Tenantbannerurlval = component.find("Tenantbannerurlval").get("v.value");
        var Tenantnewsmsgval = component.find("Tenantnewsmsgval").get("v.value");
        var TenantPConnameval = component.find("TenantPConnameval").get("v.value");
        var TenantpconEmailval = component.find("TenantpconEmailval").get("v.value");
        var TenantPConphoneval = component.find("TenantPConphoneval").get("v.value");
        
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var regPhoneNo = /^[0-9]*$/;
        
       if(!$A.util.isEmpty(TenantPConphoneval))
        {   
            
            if(!TenantPConphoneval.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.TenantPConphonemsg", 'Please enter valid phone number'); 
            } 
            else if(TenantPConphoneval.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.TenantPConphonemsg", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.TenantPConphonemsg", ''); 
                }
        }
        if(TenantpconEmailval == undefined || TenantpconEmailval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenantpconEmailmsg",'Input field required');
        }
        else if(!$A.util.isEmpty(TenantpconEmailval))
        {   
            if(!TenantpconEmailval.match(regExpEmailformat))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.TenantpconEmailmsg", 'Please Enter a Valid Email Address'); 
            }
            else
            {
                component.set("v.TenantpconEmailmsg", ''); 
            }
        }else{
            component.set("v.TenantpconEmailmsg",'');
        }
        if(Tenantnameval == undefined || Tenantnameval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.tenantnamemsg",'Input field required');
        }else{
            component.set("v.tenantnamemsg",'');
        }
        if(Tenantsitenameval == undefined || Tenantsitenameval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.tenantsitenamemsg",'Input field required');
        }else{
            component.set("v.tenantsitenamemsg",'');
        }
        if(Tenanttyppeval == "--Select--" || Tenanttyppeval == undefined || Tenanttyppeval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenanttyppeMsg",'Input field required');
        }else{
            component.set("v.TenanttyppeMsg",'');
        }
        if(component.get("v.EnableBanktype")){
            
            if(Tenantbanktyppeval == "--Select--" || Tenantbanktyppeval== undefined || Tenantbanktyppeval == ""){
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.Tenantbanktyppemsg",'Input field required');
            }else{
                component.set("v.Tenantbanktyppemsg",'');
            }
        }
        if(Tenantdatavisibilityval == "--Select--" || Tenantdatavisibilityval == undefined || Tenantdatavisibilityval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Tenantdatavisibilitymsg",'Input field required');
        }else{
            component.set("v.Tenantdatavisibilitymsg",'');
        }
        if(Tenantnewsmsgval == undefined || Tenantnewsmsgval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenantnewsmsgMsg",'Input field required');
        }else{
            component.set("v.TenantnewsmsgMsg",'');
        }
        if(Tenantthkval == undefined || Tenantthkval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Tenantthkmsg",'Input field required');
        }else{
            component.set("v.Tenantthkmsg",'');
        }
        if(Tenantshiftcodeval == undefined || Tenantshiftcodeval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenantshiftcodeMsg",'Input field required');
        }else{
            component.set("v.TenantshiftcodeMsg",'');
        }
        if(TenantWebsiteval == undefined || TenantWebsiteval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenantWebsitevmsg",'Input field required');
        }else{
            component.set("v.TenantWebsitevmsg",'');
        }
        if(Tanantlandingval == undefined || Tanantlandingval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Tanantlandingmsg",'Input field required');
        }else{
            component.set("v.Tanantlandingmsg",'');
        }
        if(Tenantlogoval == undefined || Tenantlogoval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Tenantlogomsg",'Input field required');
        }else{
            component.set("v.Tenantlogomsg",'');
        }
        if(Tenantbannerurlval == undefined || Tenantbannerurlval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Tenantbannerurlmsg",'Input field required');
        }else{
            component.set("v.Tenantbannerurlmsg",'');
        }
        if(Tenantnewsmsgval == undefined || Tenantnewsmsgval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenantnewsmsgMsg",'Input field required');
        }else{
            component.set("v.TenantnewsmsgMsg",'');
        }if(TenantNewsmsgURLVal == undefined || TenantNewsmsgURLVal == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenantNewsmsgURLMsg",'Input field required');
        }else{
            component.set("v.TenantNewsmsgURLMsg",'');
        }
        
        
        if(TenantPConnameval == undefined || TenantPConnameval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenantPConnamemsg",'Input field required');
        }else{
            component.set("v.TenantPConnamemsg",'');
        } 
        
        if(errorFlag == 'false') {  
        var Action = component.get("c.updateTenant");
            var sessionname = localStorage.getItem("UserSession");   
            Action.setParams({
                "Tenant" : component.get("v.Tenant"),
                "username" : component.get("v.UserID")
            });
            Action.setCallback(this, function(actionResult) {
                debugger;
                var state = actionResult.getState(); 
                var results = actionResult.getReturnValue();
                if(state == "SUCCESS"){
                    var site = $A.get("{!$Label.c.Org_URL}");
                    window.location = '/'+site+'/s/administration';
                }
            });
            
        }
        $A.enqueueAction(Action);
    },
    canceltoadmin: function(component, event, helper) {
		debugger; 
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/administration';
    },
    cancelTenant : function(component, event, helper) {
		debugger; 
        component.set("v.userinfo",true);
                   component.set("v.Tenantinfo",false);
        //var site = $A.get("{!$Label.c.Org_URL}");
        //window.location = '/'+site+'/s/tenantview?tenantid='+component.get("v.tenantId");
    }
})