<aura:application extends="force:slds">
  <!-- Create attribute to store lookup value as a sObject--> 
  <aura:attribute name="selectedLookUpRecords" type="sObject[]" default="[]"/>
 
  <c:reUsableMultiSelectLookup objectAPIName="User_Management__c"                              
                               lstSelectedRecords="{!v.selectedLookUpRecords}"
                               label="User Email"/>
   <!-- here c: is org. namespace prefix-->
</aura:application>