({
    callsetup : function(component, event, helper) {
        component.set("v.callsetup",false);
        component.set("v.CreateOrg",true);
        
    },
    initorg: function(component, event, helper) {
        component.set("v.callsetup",true);
        //component.set("v.CreateTenantUser",true);
        component.set("v.CreateOrg",false);
        
    },
    backtocallsetup: function(component, event, helper) {
        component.set("v.callsetup",true);
        component.set("v.CreateOrg",false);
        
    },
    callcreateTenant: function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false);
        var orgnameval = component.find("orgnameval").get("v.value");
        var orgindval = component.find("orgindval").get("v.value");
        var OrgRevenueval = component.find("OrgRevenueval").get("v.value");
        var orgpWebsiteval = component.find("orgpWebsiteval").get("v.value");
        var orgEmployeesval = component.find("orgEmployeesval").get("v.value");
        var orgLogoval = component.find("orgLogoval").get("v.value");
        var orgconnameval = component.find("orgconnameval").get("v.value");
        var orgconemailval = component.find("orgconemailval").get("v.value");
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var regPhoneNo = /^[0-9]*$/;
        var onlyletter =/^[A-Za-z ]+$/;
        var website =/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;

        var orgphoneval = component.find("orgphoneval").get("v.value");
        var OrgPconphoneval = component.find("OrgPconphoneval").get("v.value");
        if(!$A.util.isEmpty(orgphoneval))
        {   
            
            if(!orgphoneval.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.orgphonemsg", 'Please enter valid phone number'); 
            } 
            else if(orgphoneval.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.orgphonemsg", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.orgphonemsg", ''); 
                }
        }
        if(!$A.util.isEmpty(OrgPconphoneval))
        {   
            
            if(!OrgPconphoneval.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.OrgPconphonemsg", 'Please enter valid phone number'); 
            } 
            else if(OrgPconphoneval.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.OrgPconphonemsg", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.OrgPconphonemsg", ''); 
                }
        }
        if(orgnameval == undefined || orgnameval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.orgnamemsg",'Input field required');
        }else if(!$A.util.isEmpty(orgnameval))
        {   
            if(!orgnameval.match(onlyletter))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.orgnamemsg", 'Name not allowed special characters'); 
            }
            else
            {
                component.set("v.orgnamemsg", ''); 
            }
        }     
        else {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.orgnamemsg",'Input field required');
        } 
        if(orgindval == undefined || orgindval == "--Select--" || orgindval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Industrymsg",'Input field required');
        }
        else{
            component.set("v.Industrymsg",'');            
        } if(OrgRevenueval == undefined || OrgRevenueval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Revenuemsg",'Input field required');
        }
        else if(!OrgRevenueval.match(regPhoneNo))
        {
            
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Revenuemsg", 'Please enter only number'); 
        } 
            else {
                component.set("v.Revenuemsg",'');
            }
        if(orgpWebsiteval == undefined || orgpWebsiteval == ""){
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.WebsiteMsg",'Input field required');
        }
        else if(!$A.util.isEmpty(orgpWebsiteval))
        {   
            if(!orgpWebsiteval.match(website))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.WebsiteMsg", 'Please Enter a Valid Website'); 
            }
            else
            {
                component.set("v.WebsiteMsg", ''); 
            }
        }        
            else {
                component.set("v.WebsiteMsg",'');
                
        }if(orgEmployeesval == undefined || orgEmployeesval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Employeesmsg",'Input field required');
        }else if(!orgEmployeesval.match(regPhoneNo))
        {
            
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Employeesmsg", 'Please enter only number'); 
        } 
            else{
                component.set("v.Employeesmsg",'');
            } 
        if(orgLogoval == undefined || orgLogoval == ""){
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.orglogomsg",'Input field required');
            }
        else if(!$A.util.isEmpty(orgLogoval))
        {   
            if(!orgLogoval.match(website))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.orglogomsg", 'Please Enter a Valid URL'); 
            }
            else
            {
                component.set("v.orglogomsg", ''); 
            }
        }   
        else {
            component.set("v.orglogomsg",'');
        }if(orgconnameval == undefined || orgconnameval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.ContnameMsg",'Input field required');
        }
        else{
            component.set("v.ContnameMsg",'');
        } if(orgconemailval == undefined || orgconemailval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.orgemailmsg",'Input field required');
        }
        else if(!$A.util.isEmpty(orgconemailval))
        {   
            if(!orgconemailval.match(regExpEmailformat))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.orgemailmsg", 'Please Enter a Valid Email Address'); 
            }
            else
            {
                component.set("v.orgemailmsg", ''); 
            }
        }else{
            component.set("v.orgemailmsg",'');
        }
        var action = component.get("c.CheckOrgName");
        action.setParams({ "OrgName": orgnameval });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var bank = response.getReturnValue(); 
            if(bank != '' && bank != undefined) {
                if(bank == 'Already Used') { 
                    errorFlag = 'true';
                    component.set("v.validation", true); 
                    component.set("v.orgnamemsg", 'This Organization Name already Exist. Please enter a new Organization Name.');
                } else {
                    component.set("v.orgnamemsg", ''); 
                }
            } 
            if(errorFlag == 'false') {  
                component.set("v.CreateTenant",true);
                component.set("v.CreateOrg",false);
                component.set("v.callsetup",false);
            }
        });
        $A.enqueueAction(action);
    },
    backtocallsetuptenant: function(component, event, helper) {
        component.set("v.CreateTenant",false);
        component.set("v.CreateOrg",true);
        component.set("v.callsetup",false);
        
    },
    callcreateTenantuser: function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false);
        
        var Tenantnameval = component.find("Tenantnameval").get("v.value");
        
        var Tenantsitenameval = component.find("Tenantsitenameval").get("v.value");
        var Tenanttyppeval = component.find("Tenanttyppeval").get("v.value");
        if(component.get("v.EnableBanktype")){
            var Tenantbanktyppeval = component.find("Tenantbanktyppeval").get("v.value");
        }
        var Tenantdatavisibilityval = component.find("Tenantdatavisibilityval").get("v.value");
        var TenantNewsmsgURLVal = component.find("TenantNewsmsgURLVal").get("v.value");
        var Tenantthkval = component.find("Tenantthkval").get("v.value");
        var Tenantshiftcodeval = component.find("Tenantshiftcodeval").get("v.value");
        var TenantWebsiteval = component.find("TenantWebsiteval").get("v.value");
        var Tanantlandingval = component.find("Tanantlandingval").get("v.value");
        var Tenantlogoval = component.find("Tenantlogoval").get("v.value");
        var Tenantbannerurlval = component.find("Tenantbannerurlval").get("v.value");
        var Tenantnewsmsgval = component.find("Tenantnewsmsgval").get("v.value");
        var TenantPConnameval = component.find("TenantPConnameval").get("v.value");
        var TenantpconEmailval = component.find("TenantpconEmailval").get("v.value");
        var TenantPConphoneval = component.find("TenantPConphoneval").get("v.value");
        
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var regPhoneNo = /^[0-9]*$/;
        //var onlyletter ='^[0-9]*$'; 
        var onlyletter =/^[a-zA-Z]*$/;
        var onlyletterandspace =/^[A-Za-z ]+$/;
        var nameletter =/^[A-Za-z0-9 ]+$/;
        var website =/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;

        if(!$A.util.isEmpty(TenantPConphoneval))
        {   
            
            if(!TenantPConphoneval.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.TenantPConphonemsg", 'Please enter valid phone number'); 
            } 
            else if(TenantPConphoneval.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.TenantPConphonemsg", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.TenantPConphonemsg", ''); 
                }
        }
        if(TenantpconEmailval == undefined || TenantpconEmailval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenantpconEmailmsg",'Input field required');
        }
        else if(!$A.util.isEmpty(TenantpconEmailval))
        {   
            if(!TenantpconEmailval.match(regExpEmailformat))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.TenantpconEmailmsg", 'Please Enter a Valid Email Address'); 
            }
            else
            {
                component.set("v.TenantpconEmailmsg", ''); 
            }
        }else{
            component.set("v.TenantpconEmailmsg",'');
        }
        if(Tenantnameval == undefined || Tenantnameval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.tenantnamemsg",'Input field required');
        } else if(!$A.util.isEmpty(Tenantnameval))
        {   
            if(!Tenantnameval.match(onlyletterandspace))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.tenantnamemsg", 'Tenant Name should only be in alphabets.'); 
            }
            else
            {
                component.set("v.tenantnamemsg", ''); 
            }
        }else{
            component.set("v.tenantnamemsg",'');
        }
        if(Tenantsitenameval == undefined || Tenantsitenameval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.tenantsitenamemsg",'Input field required');
        }else{
            if(!$A.util.isEmpty(Tenantsitenameval))
            {   
                debugger;
                
                if(!Tenantsitenameval.match(onlyletter))
                {
                    errorFlag = 'true';
                    component.set("v.validation", true);
                    component.set("v.tenantsitenamemsg",'Tenant Site Name should only be in alphabets.');
                }else{
                    component.set("v.tenantsitenamemsg",'');
                }
            }
        }
        if(Tenanttyppeval == "--Select--" || Tenanttyppeval == undefined || Tenanttyppeval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenanttyppeMsg",'Input field required');
        }else{
            component.set("v.TenanttyppeMsg",'');
        }
        if(component.get("v.EnableBanktype")){
            
            if(Tenantbanktyppeval == "--Select--" || Tenantbanktyppeval== undefined || Tenantbanktyppeval == ""){
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.Tenantbanktyppemsg",'Input field required');
            }else{
                component.set("v.Tenantbanktyppemsg",'');
            }
        }
        if(Tenantdatavisibilityval == "--Select--" || Tenantdatavisibilityval == undefined || Tenantdatavisibilityval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Tenantdatavisibilitymsg",'Input field required');
        }else{
            component.set("v.Tenantdatavisibilitymsg",'');
        }
        if(Tenantnewsmsgval == undefined || Tenantnewsmsgval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenantnewsmsgMsg",'Input field required');
        }else{
            component.set("v.TenantnewsmsgMsg",'');
        }
        if(Tenantthkval == undefined || Tenantthkval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Tenantthkmsg",'Input field required');
        }else{
            component.set("v.Tenantthkmsg",'');
        }
        if(Tenantshiftcodeval == undefined || Tenantshiftcodeval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenantshiftcodeMsg",'Input field required');
        }else{
            component.set("v.TenantshiftcodeMsg",'');
        }
        if(TenantWebsiteval == undefined || TenantWebsiteval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenantWebsitevmsg",'Input field required');
        }
        else if(!$A.util.isEmpty(TenantWebsiteval))
        {   
            if(!TenantWebsiteval.match(website))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.TenantWebsitevmsg", 'Please Enter a Valid Website'); 
            }
            else
            {
                component.set("v.TenantWebsitevmsg", ''); 
            }
        }
        else{
            component.set("v.TenantWebsitevmsg",'');
        }
        if(Tanantlandingval == undefined || Tanantlandingval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Tanantlandingmsg",'Input field required');
        }else{
            component.set("v.Tanantlandingmsg",'');
        }
        if(Tenantlogoval == undefined || Tenantlogoval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Tenantlogomsg",'Input field required');
        }else if(!$A.util.isEmpty(Tenantlogoval))
        {   
            if(!Tenantlogoval.match(website))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.Tenantlogomsg", 'Please Enter a Valid URL'); 
            }
            else
            {
                component.set("v.Tenantlogomsg", ''); 
            }
        }else{
            component.set("v.Tenantlogomsg",'');
        }
        if(Tenantbannerurlval == undefined || Tenantbannerurlval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Tenantbannerurlmsg",'Input field required');
        }else if(!$A.util.isEmpty(Tenantbannerurlval))
        {   
            if(!Tenantbannerurlval.match(website))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.Tenantbannerurlmsg", 'Please Enter a Valid URL'); 
            }
            else
            {
                component.set("v.Tenantbannerurlmsg", ''); 
            }
        }else{
            component.set("v.Tenantbannerurlmsg",'');
        }
        if(Tenantnewsmsgval == undefined || Tenantnewsmsgval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenantnewsmsgMsg",'Input field required');
        }else{
            component.set("v.TenantnewsmsgMsg",'');
        }if(TenantNewsmsgURLVal == undefined || TenantNewsmsgURLVal == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenantNewsmsgURLMsg",'Input field required');
        }else if(!$A.util.isEmpty(TenantNewsmsgURLVal))
        {   
            if(!TenantNewsmsgURLVal.match(website))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.TenantNewsmsgURLMsg", 'Please Enter a Valid URL'); 
            }
            else
            {
                component.set("v.TenantNewsmsgURLMsg", ''); 
            }
        }else{
            component.set("v.TenantNewsmsgURLMsg",'');
        }
        
        
        if(TenantPConnameval == undefined || TenantPConnameval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenantPConnamemsg",'Input field required');
        }else{
            component.set("v.TenantPConnamemsg",'');
        }
        if(Tenantnameval!=undefined && Tenantsitenameval!=undefined){
        var action = component.get("c.CheckTenantName");
        action.setParams({ "TenantName": Tenantnameval,
                          "Tenantsiteval": Tenantsitenameval});
        action.setCallback(this, function(response) {
            var state = response.getState();
            var bank = response.getReturnValue(); 
            if(bank != '' && bank != undefined) {
                if(bank.CheckTenantUserName == 'Already Used') { 
                    errorFlag = 'true';
                    component.set("v.validation", true); 
                    component.set("v.tenantnamemsg", 'This Tenant Name already Exist. Please enter a new Tenant Name.');
                } 
                if(bank.CheckTenantsiteName == 'Already Used') { 
                    errorFlag = 'true';
                    component.set("v.validation", true); 
                    component.set("v.tenantsitenamemsg", 'This Tenant Site Name already Exist. Please enter a new Tenant Site Name.');
                }
            } else { 
               // component.set("v.tenantnamemsg", '');
                //component.set("v.tenantsitenamemsg", '');
            }
            if(errorFlag == 'false') {  
                component.set("v.CreateTenantUser",true);
                component.set("v.CreateTenant",false);
                component.set("v.CreateOrg",false);
                component.set("v.callsetup",false);
            } });
        $A.enqueueAction(action);
        }
        
    },
    backtocallsetuptenantuser: function(component, event, helper) {
        component.set("v.CreateTenantUser",false);
        component.set("v.CreateTenant",true);
        component.set("v.CreateOrg",false);
        component.set("v.callsetup",false);
    },
    onTenantTypeSelectChange: function(component, event, helper) {
        var selectCmp = component.find("Tenanttyppeval").get("v.value");
        if(selectCmp =='Bank')
        {
            component.set("v.EnableBanktype",true); 
        }else{
            component.set("v.EnableBanktype",false);
        }
    },
    Finalsave: function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false);
        var Tenantuserfirstval = component.find("Tenantuserfirstval").get("v.value");
        var Tenantuseruserval = component.find("Tenantuseruserval").get("v.value");
        var Tenantroleval = component.find("Tenantroleval").get("v.value");
        var Tenantuserlstnameval = component.find("Tenantuserlstnameval").get("v.value");
        var Tenantuserphoneval = component.find("Tenantuserphoneval").get("v.value");
        var Tenantuseremailval = component.find("Tenantuseremailval").get("v.value");
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var regPhoneNo = /^[0-9]*$/;
        var onlyletter =/^[A-Za-z ]+$/;         
        if(!$A.util.isEmpty(Tenantuserphoneval))
        {   
            
            if(!Tenantuserphoneval.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.Tenantuserphonemsg", 'Please enter valid phone number'); 
            } 
            else if(Tenantuserphoneval.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.Tenantuserphonemsg", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.Tenantuserphonemsg", ''); 
                }
        }
        if(Tenantuseremailval == undefined || Tenantuseremailval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Tenantuseremailmsg",'Input field required');
        }
        else if(!$A.util.isEmpty(Tenantuseremailval))
        {   
            if(!Tenantuseremailval.match(regExpEmailformat))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.Tenantuseremailmsg", 'Please Enter a Valid Email Address'); 
            }
            else
            {
                component.set("v.Tenantuseremailmsg", ''); 
            }
        }else{
            component.set("v.Tenantuseremailmsg",'');
        }
        if(Tenantuserfirstval == undefined || Tenantuserfirstval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Tenantuserfirstmsg",'Input field required');
        }else if(!$A.util.isEmpty(Tenantuserfirstval)){
            if(!Tenantuserfirstval.match(onlyletter)){
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.Tenantuserfirstmsg",'First Name not allowed numbers and special characters');
            }else{
                component.set("v.Tenantuserfirstmsg",'');
            }
        }
        else {
            component.set("v.Tenantuserfirstmsg",'');
        }
        if(Tenantuserlstnameval == undefined || Tenantuserlstnameval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Tenantuserlstnamemsg",'Input field required');
        }else if(!$A.util.isEmpty(Tenantuserlstnameval)){
            if(!Tenantuserlstnameval.match(onlyletter)){
                errorFlag = 'true';
            	component.set("v.validation", true);
            	component.set("v.Tenantuserlstnamemsg",'First Name not allowed numbers and special characters');
            }else{
                component.set("v.Tenantuserlstnamemsg",'');
            }
        }
        else {
            component.set("v.Tenantuserlstnamemsg",'');
        }
        if(Tenantuseruserval == undefined || Tenantuseruserval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Tenantuserusermsg",'Input field required');
        }else if(!$A.util.isEmpty(Tenantuseruserval)){
            if(!Tenantuseruserval.match(regExpEmailformat)){
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.Tenantuserusermsg",'User Name should be email format');
            }else{
                component.set("v.Tenantuserusermsg",'');
            }
        }
        else {
            component.set("v.Tenantuserusermsg",'');
        }    
        if(Tenantroleval == undefined || Tenantroleval =='--Select--' || Tenantroleval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Tenantrolevalmsg",'Input field required');
        }
        else {
            component.set("v.Tenantrolevalmsg",'');
        }
        if(Tenantuseruserval != undefined || Tenantuseruserval != ""){
        var action = component.get("c.CheckTenantUserName");
        action.setParams({ "UserName": Tenantuseruserval });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var bank = response.getReturnValue(); 
            if(bank != '' && bank != undefined) {
                if(bank == 'Already Used') { 
                    errorFlag = 'true';
                    component.set("v.validation", true); 
                    component.set("v.Tenantuserusermsg", 'This User Name already Exist. Please enter a new User Name.');
                } else {
                    component.set("v.Tenantuserusermsg", ''); 
                }
            }
            if(errorFlag == 'false') { 
                
                var action = component.get('c.SavesetupOrg');  
                action.setParams({
                    "LoggedUserID" : localStorage.getItem("UserSession"),
                    "OrgDetails" : component.get("v.Org"),
                    "TenantDetails" : component.get("v.Tenant"),
                    "TenantUserDetails" : component.get("v.TenantUser"),
                    "iterationcall" : component.get("v.iterationcall"),
                    "InsertedTIds" : component.get("v.InsertedTIds")
                });
                debugger;
                action.setCallback(this, function(actionResult) { 
                    var state = actionResult.getState(); 
                    var result = actionResult.getReturnValue();
                    if(state === "SUCCESS"){
                        component.set("v.iterationcall",true);
                        if(component.get("v.InsertedTIds") == ''){
                            component.set("v.InsertedTIds",result);
                        }
                        component.set("v.CreateTenantUser",true);
                        component.set("v.CreateTenant",false);
                        component.set("v.CreateOrg",false);
                        component.set("v.callsetup",false);
                        
                        var TenantUser = {
                            'sobjectType': 'User_Management__c',
                            'First_Name__c': '',
                            'User_Name__c':  '',
                            'User_Profile__c':  '',
                            'Last_Name__c':  '',
                            'User_Email__c':  '',
                            'Phone__c':  '',
                            'Street__c':  '',
                            'City__c':  '',
                            'Country__c':  '',
                            'State_Province__c':  '',
                            'Zip_Postal_Code__c':  ''
                        };
                        component.set("v.TenantUser",TenantUser);
                        var showToast = $A.get('e.force:showToast');
                        showToast.setParams(
                            {
                                'title': 'Success: ',
                                'message': component.get("v.TenantUser.User_Name__c")+' Tenant User Successfully Added.',
                                'type': 'Success'
                            }
                        );
                        showToast.fire();                        
                    }
                });
                debugger;
                $A.enqueueAction(action);
            }
        });
        $A.enqueueAction(action);
        }
    },
    Finalsavecall: function(component, event, helper) {
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/administration';
    },
    reqCheck: function(component, event, helper) {
        if(component.find("ChkreqAccess").get('v.value')==true)
        {
            component.find("Accessbtn1").set("v.disabled", false);
        }
        else{
            component.find("Accessbtn1").set("v.disabled", true);
        }
    },
    reqAccesscancel: function(component, event) {
        component.set("v.Finishpopup",false); 
    },
    btnreqAccess: function(component, event, helper) {
        var action = component.get('c.SavesetupOrg');  
        action.setParams({
            "LoggedUserID" : localStorage.getItem("UserSession"),
            "OrgDetails" : component.get("v.Org"),
            "TenantDetails" : component.get("v.Tenant"),
            "TenantUserDetails" : component.get("v.TenantUser")
        });
        debugger;
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                var site = $A.get("{!$Label.c.Org_URL}");
                window.location = '/'+site+'/s/administration';
            }
        });
        debugger;
        $A.enqueueAction(action);
    },
})