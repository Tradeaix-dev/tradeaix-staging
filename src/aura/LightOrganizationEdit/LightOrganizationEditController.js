({
    getOrglist : function(component, event, helper) {
        debugger;
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            }catch(ex){}
        };
        
        var orgId = getUrlParameter('orgid');        
        var Action = component.get("c.getOrganization");
        var sessionname = localStorage.getItem("UserSession");       
        Action.setParams({
            "username" : sessionname,
            "orgname" : orgId
        });
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.Orglist",results.organization);  
                component.set("v.organization", results);
            }
        });
        $A.enqueueAction(Action);
    },
    
    editOrganization : function(component, event, helper) {
        debugger;
         var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            }catch(ex){}
        };        
        var orgId = getUrlParameter('orgid');  
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/organizationupdate?orgid='+orgId; 
    },
    
    backtoAdmin : function(component, event, helper) {
        debugger;
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/administration'; 
    }
    
})