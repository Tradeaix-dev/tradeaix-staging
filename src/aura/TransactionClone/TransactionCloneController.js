({ 
       getOfferDetails: function(component, event, helper) { 
        debugger;
        // component.set("v.bindtabnamesforoff",'Offers');
        var action = component.get('c.OfferDetails');
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            } catch(ex){}
        };
        
        var offerId = getUrlParameter('transactionId');
           
        var status = getUrlParameter('Status');
           
        var self = this; 
        action.setParams({
            "offerId" : offerId
        });
        
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            debugger;
            if(state === "SUCCESS"){
                
                var nextAction = component.get("c.secondarymarketingflag"); 
                debugger;
                nextAction.setCallback(this, function(actionResult1) {
                    var state1 = actionResult1.getState(); 
                    var results = actionResult1.getReturnValue();
                    if(state1 === "SUCCESS"){
                        debugger;
                        if(results)
                        {
                            component.set("v.bindtabnamesforoff",'Quotes');
                            component.set("v.bindtabnamesfornotes",'Internal Memos');
                            component.set("v.bindtabnamesforPart",'Participant');
                            
                        }else{
                            component.set("v.bindtabnamesforoff",'Quotes');
                            component.set("v.bindtabnamesfornotes",'Internal Memos');
                            component.set("v.bindtabnamesforPart",'Counter Parties');
                        }
                        component.set("v.issecondaryMarket",results);
                        component.set("v.issecondaryMarketflag",true);
                    }
                });
                $A.enqueueAction(nextAction);
                //alert(result.OfferExtended.Offer.Offer_Status__c);
                var Transstatus=result.OfferExtended.Offer.Offer_Status__c;
                component.set("v.Created",result.OfferExtended.Offer.CreatedDate);
                
                var appdate = result.OfferExtended.Offer.Approvedate__c;
                //alert(appdate);
                if(appdate!='undefined' || appdate!='' )
                {
                     component.set("v.approveddate",appdate);
                }
                 
               
                var valdate = result.OfferExtended.Offer.validateddate__c;
               if(valdate=='undefined' || valdate!='' )
                {  
                     component.set("v.Validateddate",valdate);
                }
                component.set("v.Tstatus",Transstatus);
                if(Transstatus==='Pending Validation' || Transstatus==='Level 1 - Pending Approval' 
                   || Transstatus==='Pending Approval' || Transstatus==='Validation Rejected' || Transstatus==='Transaction Rejected')
                {
                    component.set("v.Tprogress", "process");
                    component.set("v.Tbox", "p-card progress-border");
					component.set("v.hTstatus","spinneryes");
                } else if(Transstatus == 'Inactive'){
                    component.set("v.Tprogress", "red");
                    component.set("v.Tbox", "p-card red-border");
					//component.set("v.hTstatus","spinneryes"); 
                        
                    }
                    
                else if(Transstatus==='Transaction Approved' || Transstatus==='Requested For Info')
                {   
                    if(Transstatus==='Transaction Approved'){
                        component.set("v.RFI","Requested For Info / Quote");
                    }else{
                    component.set("v.Requesteddate",result.OfferExtended.Offer.PublishedDate__c);
                    component.set("v.RequestedBy",result.OfferExtended.Offer.Published_By__c);
                   
                   component.set("v.RFI","Requested For Info");
                    }
                    component.set("v.Tprogress", "active");
                    component.set("v.Tbox", "p-card active-border");
                    component.set("v.RFIprogress", "process");
                    component.set("v.RFIbox", "p-card progress-border");
                    component.set("v.hTstatus","spinnerno");
                    component.set("v.hRFQstatus","spinneryes");
                    component.set("v.FullRFIstatus",true);
                }      
                else if(Transstatus==='Requested For Quote' || Transstatus==='Bid Submitted')
                {
 
                    component.set("v.Requesteddate",result.OfferExtended.Offer.PublishedDate__c);
                    component.set("v.RequestedBy",result.OfferExtended.Offer.Published_By__c);
                    component.set("v.Tprogress", "active");
                    component.set("v.RFIprogress", "active");
                    component.set("v.RFIbox", "p-card active-border");
                    component.set("v.hTstatus","spinnerno");
                    component.set("v.hRFQstatus","spinnerno");
                    component.set("v.Tbox", "p-card active-border");
                    component.set("v.Bidprogress", "process");
                    component.set("v.Bidbox", "p-card progress-border");
                    component.set("v.hTstatus","spinnerno");
                    component.set("v.RFI","Requested For Quote");
                    component.set("v.hRFQstatus","spinnerno");
                    component.set("v.hbidstatus","spinneryes");
                    component.set("v.FullRFIstatus",true);
					component.set("v.FullBidstatus",true);                    
                    if(Transstatus==='Bid Submitted')
                    {
                        if(result.OfferExtended.Syndications.length > 0){
                    component.set("v.BidStatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                   	component.set("v.LastBidDate", result.OfferExtended.Syndications[0].Syndication.submitted_Date__c);
                    component.set("v.HighestBidder",result.OfferExtended.Syndications[0].Syndication.Buyer_Name__c);
                    component.set("v.progressBidamount",result.OfferExtended.Syndications[0].Syndication.Buyer_Counter_Price__c);
                        }
                    }
                    
                    
                }
                else if(Transstatus==='Accept Bid')
                {
                    component.set("v.BidStatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                   	component.set("v.LastBidDate", result.OfferExtended.Syndications[0].Syndication.submitted_Date__c);
                    component.set("v.HighestBidder",result.OfferExtended.Syndications[0].Syndication.Buyer_Name__c);
                    component.set("v.progressBidamount",result.OfferExtended.Syndications[0].Syndication.Buyer_Counter_Price__c);
                    component.set("v.Requesteddate",result.OfferExtended.Offer.PublishedDate__c);
                    component.set("v.RequestedBy",result.OfferExtended.Offer.Published_By__c);
                    component.set("v.Tprogress", "active");
                    component.set("v.RFIprogress", "active");
                    component.set("v.RFIbox", "p-card active-border");
                    component.set("v.hTstatus","spinnerno");
                    component.set("v.hRFQstatus","spinnerno");
                    component.set("v.Tbox", "p-card active-border");
                    component.set("v.Bidprogress", "active");
                    component.set("v.Bidbox", "p-card active-border");
                    component.set("v.hTstatus","spinnerno");
                    component.set("v.RFI","Requested For Quote");
                    component.set("v.comprogress", "process");
                    component.set("v.combox", "p-card progress-border");
                    component.set("v.hcomstatus","spinneryes");
                    component.set("v.hbidstatus","spinnerno");
                    component.set("v.FullRFIstatus",true);
					component.set("v.FullBidstatus",true); 
                    component.set("v.Fullcomstatus",true);
                    
                }else if(Transstatus==='Completed Transaction')
                {
                    component.set("v.FullRFIstatus",true);
					component.set("v.FullBidstatus",true); 
                    component.set("v.Fullcomstatus",true);
                    component.set("v.BidStatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                   	component.set("v.LastBidDate", result.OfferExtended.Syndications[0].Syndication.submitted_Date__c);
                    component.set("v.HighestBidder",result.OfferExtended.Syndications[0].Syndication.Buyer_Name__c);
                    component.set("v.progressBidamount",result.OfferExtended.Syndications[0].Syndication.Buyer_Counter_Price__c);
                    component.set("v.Completed",result.finalizedOfferdate);
                    component.set("v.Requesteddate",result.OfferExtended.Offer.PublishedDate__c);
                    component.set("v.RequestedBy",result.OfferExtended.Offer.Published_By__c);
                    component.set("v.Tprogress", "active");
                    component.set("v.RFIprogress", "active");
                    component.set("v.RFIbox", "p-card active-border");
                    component.set("v.hTstatus","spinnerno");
                    component.set("v.hRFQstatus","spinnerno");
                    component.set("v.Tbox", "p-card active-border");
                    component.set("v.Bidprogress", "active");
                    component.set("v.Bidbox", "p-card active-border");
                    component.set("v.hTstatus","spinnerno");
                    component.set("v.RFI","Requested For Quote");
                    component.set("v.hcomstatus","spinnerno");
                    component.set("v.comprogress", "active");
                    component.set("v.combox", "p-card active-border");
                    component.set("v.hbidstatus","spinnerno");
                    
                }
                    
                debugger;
                component.set("v.Offer", result);
                document.title = result.OfferExtended.Offer.TransactionRefNumber__c;
                component.set("v.offerId", result.offerId); 
                component.set("v.parentId", result.offerId);
                component.set("v.SellerUserId", result.OfferExtended.Offer.CreatedById);
                debugger;
                component.set("v.validation", false);
                component.set("v.loanError", '');
                component.set("v.templateError", '');
                component.set("v.nameError", '');
                component.set("v.participationAmountError", '');
                component.set("v.participationPercentageError", '');
                component.set("v.LogeduserCUnionId", result.LogeduserCBId);
                component.set("v.credtiunionchk", result.credtiunionchk);
                component.set("v.PartyID", result.OfferExtended.CreditUnion.Id);
                component.set("v.Partname", result.OfferExtended.CreditUnion.CU_Name__c);
                if(result.isSeller) {
                    try{ component.set("v.selectedTabId", "0"); } catch(ex){}
                } else {
                    component.set("v.selectedTabId", "1");
                }
                
                debugger;
                var container = component.find("container");
                $A.util.removeClass(container, "slds-hide");
                $A.util.addClass(container, "slds-show");
                
                $A.createComponent("c:"+result.OfferExtended.Offer.Component_Name__c,
                                   {
                                       "aura:id": 'MyCustomComponent',
                                       "edit": 'false',
                                       "mode": 'VIEW',
                                       "objectApiName": result.OfferExtended.Offer.SObject_Name__c,
                                       "recordId": result.OfferExtended.Offer.Record_Id__c
                                   },
                                   function(newComponent, status, errorMessage){ 
                                       debugger;
                                       var targetCmp = component.find('placeHolder');
                                       
                                       targetCmp.set("v.body", []); 
                                       var body = targetCmp.get("v.body");
                                       console.log('@body'+body);
                                       if((result.isBuyer || result.isBuyerValidator) && !(result.OfferExtended.Offer.Granted__c))
                                       {
                                           //if(result.OfferExtended.Offer.Request_For_Information__c!=NULL){
                                           debugger;
                                           var json = JSON.parse(result.OfferExtended.Offer.Request_For_Information__c);
                                           for(var i = 0; i < json.length; i++) {
                                               var obj = json[i];
                                               if(obj.Flag=='TRUE'){
                                                   var hidediv = newComponent.find('div'+obj.ID);
                                                   if(hidediv){
                                                       $A.util.addClass(hidediv, 'hide');
                                                   }
                                               }
                                               
                                           }
                                           //}
                                       }
                                       body.push(newComponent);
                                       alert('@body'+newComponent);
                                       targetCmp.set("v.body", body); 
                                       
                                       
                                   });
                debugger;
                //changeParticipationAmount(component, event, helper);
                //changeParticipationPercentage(component, event, helper);
            }
            var menu = document.getElementById('Transactions');
            var classArr = menu.className.split(" ");
            if (classArr.indexOf("current") == -1) {
                menu.className += " " + "current";
            }
        
        }); 
        $A.enqueueAction(action);
    },  
    handleResponse : function(component, event, helper) {
        var response = event.getParam("response");
    },
    uploadfilepopup: function(component, event) {     
        debugger;
        component.set("v.opendocvault", true); 
    },
     btncancelfor: function(component, event) {     
        debugger;
        component.set("v.opendocvault", false); 
    },
    requestForInfo : function(component, event) {
        
        debugger;
        console.log('@inside info@');
        component.set("v.requestForInfo", true); 
        var contentVersion = component.get("v.Offer.content");
        var attachment = component.get("v.Offer.attachment");
        var errorFlag = 'false';
        var counterParties = component.get("v.Offer.OfferExtended.CounterParties");
        if(errorFlag == 'true') 
        {
            component.set("v.PubEditError", 'One or more fields below do not have appropriate data');
        }
        else{ 
            /*
            if((contentVersion.length > 0 || attachment.length > 0) && counterParties.length > 0 && errorFlag == 'false') 
            {
                component.set("v.publishOffer", true); 
            }         
            else
            {
                if(counterParties.length <= 0){
                    component.set("v.counterError", true);
                } else {
                    component.set("v.publishError", true);
                }
            }
            */
            
            if(counterParties.length > 0 && errorFlag == 'false') 
            {
                component.set("v.POPrequestForInfo", true); 
                //component.set("v.publishOffer", true); 
            }         
            else
            {
                if(counterParties.length <= 0){
                    component.set("v.counterError", true);
                    component.find("tabs").set("v.Activetab",3);
                } 
            }
        }
        var action = component.get('c.getTransactionfields');
        //var Tid='a07m0000008DkAbAAK';
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            } catch(ex){}
        };
        var Tid=getUrlParameter('transactionId'); 
        console.log('@@'+Tid);
        debugger;
        var self = this;
        action.setParams({
            "Transid" : Tid          
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                debugger;
                console.log('@@'+result);
                //component.set("v.templateforRFI", result);
                var custs = [];
                for(var key in result){
                    debugger;
                    custs.push({value:result[key], key:key}); //Here we are creating the list to show on UI.
                }
                component.set("v.lensize",custs.length%2); 
                component.set("v.templateforRFI",custs); 
            }
            else if(state === "ERROR") {
                debugger;
            }
        });
        $A.enqueueAction(action);
    },
    onCheck: function(component, event) { 
        debugger;
        var checkboxes = document.getElementsByClassName("chk");
        
        var chkTF = [];
        for(var i = 0; i < checkboxes.length; i++)
        {  
            if (checkboxes[i].checked) {
                
                console.log('##'+event.target.value);
                
                chkTF.push(event.target.value);
            }
            
            
        } 
        
        component.set("v.CheckedTfields", chkTF);
        var d=component.get("v.CheckedTfields");
        onsole.log('#&#'+d);
    },	
    saveTR: function(component, event) {
        debugger;
        var jsonObj='[';
        var getAllId = component.find("boxPack");
        var len=getAllId.length-1;
        var j = 0;
        for (var i = 0; i < getAllId.length; i++) {        
            jsonObj=jsonObj+'{';
            if (getAllId[i].get("v.value") == true) {
                jsonObj=jsonObj+'"ID":"'+getAllId[i].get("v.text")+'","Flag":"TRUE"';
                j++;
            }else
            {  jsonObj=jsonObj+'"ID":"'+getAllId[i].get("v.text")+'","Flag":"FALSE"';
              
            }
            if(len==i){
                jsonObj=jsonObj+'}';
            }else{
                jsonObj=jsonObj+'},';
            }
        }  
        jsonObj=jsonObj+']';
        console.log('#json1#'+jsonObj); 
        component.set('v.maskcount',j); 
        
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            } catch(ex){}
        };
        debugger;
        var action = component.get('c.SaveRFI'); 
        var Tnsid=getUrlParameter('transactionId');
        action.setParams({
            "RFI" : jsonObj,
            "Transid" : Tnsid
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.POPrequestForInfo", false); 
                component.set("v.publishOffer", true); 
            }
        });
        $A.enqueueAction(action);
    },
    publishingOffer : function(component, event) {
        debugger;
        component.set("v.requestForInfo", false); 
        var contentVersion = component.get("v.Offer.content");
        var attachment = component.get("v.Offer.attachment");
        var errorFlag = 'false';
        
        var counterParties = component.get("v.Offer.OfferExtended.CounterParties");
        
        if(errorFlag == 'true') 
        {
            component.set("v.PubEditError", 'One or more fields below do not have appropriate data');
        }
        else{ 
            /*
            if((contentVersion.length > 0 || attachment.length > 0) && counterParties.length > 0 && errorFlag == 'false') 
            {
                component.set("v.publishOffer", true); 
            }         
            else
            {
                if(counterParties.length <= 0){
                    component.set("v.counterError", true);
                } else {
                    component.set("v.publishError", true);
                }
            }
            */
            
            if(counterParties.length > 0 && errorFlag == 'false') 
            {
                //component.set("v.requestForInfo", true); 
                component.set("v.publishOffer", true); 
            }         
            else
            {
                if(counterParties.length <= 0){
                    debugger;
                    component.set("v.counterError", true);
                    component.find("tabs").set("v.Activetab","3");
                    
                } 
            }
        }
    },
    counterErrorcancel : function(component, event) {
        debugger;
        component.set("v.counterError", false);
        component.find("tabs").set("v.Activetab","3");
    },
    SecondpublishPopupcancel : function(component, event) {
        component.set("v.SecondpublishPopup", false);
        $A.get('e.force:refreshView').fire();
    },
     completePopupcancel : function(component, event) {
        component.set("v.CompletePopup", false);
        $A.get('e.force:refreshView').fire();
    },
    publishErrorcancel : function(component, event) {
        component.set("v.publishError", false);
    }, 
    publishcancel : function(component, event) {
        component.set("v.publishOffer", false);
    }, 
    selectdocuvault : function(component, event) {
        component.set("v.popdocuvault", true);
    },
    canceldocuvault : function(component, event) {
        component.set("v.popdocuvault", false);
    },
    selectChange : function(component, event, helper) {
        if(component.find("chk1").get('v.value')==true)
        {
            component.find("btn1").set("v.disabled", false);
        }
        else{
            component.find("btn1").set("v.disabled", true);
        }
    }, 
    btnpublish: function(component, event, helper) {
        var action = component.get('c.publish'); 
        var self = this; 
        action.setParams({
            "offerId" : component.get("v.offerId"),
            "requestForInfo": component.get("v.requestForInfo")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.publishOffer", false);
            component.set("v.SecondpublishPopup", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);
                component.set("v.offerId", result.offerId);  
                component.set("v.parentId", result.offerId);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    }, 
    
    publishcancel : function(component, event) {
        component.set("v.publishOffer", false);
    }, 
    UnpublishOffer : function(component, event) { 
        component.set("v.Unpublish", true);
    }, 
    unpublishCheck: function(component, event, helper) {
        if(component.find("chkUnpublish").get('v.value')==true)
        {
            component.find("Unpublishbtn1").set("v.disabled", false);
        }
        else{
            component.find("Unpublishbtn1").set("v.disabled", true);
        }
    }, 
    SecondUnpublishPopupcancel : function(component, event) {
        component.set("v.SecondUnpublishPopup", false);
    },
    btnUnpublish: function(component, event, helper) {
        var action = component.get('c.Unpublish'); 
        var self = this; 
        action.setParams({
            "offerId" : component.get("v.offerId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.Unpublish", false);
            component.set("v.SecondUnpublishPopup", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);
                component.set("v.offerId", result.offerId);  
                component.set("v.parentId", result.offerId);               
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    }, 
    Unpublishcancel: function(component, event) {
        component.set("v.Unpublish", false);
    },
    InactiveOffer1 : function(component, event) { 
        component.set("v.Inactive1", true);       
    },    
    inactiveCheck: function(component, event, helper) {
        if(component.find("chkInactive").get('v.value')==true)
        {
            debugger;
            component.find("Inactivebtn1").set("v.disabled", "false");
        }
        else{
            debugger;
            component.find("Inactivebtn1").set("v.disabled", "true");
        }
    },
    btnInactive: function(component, event, helper) {
        var action = component.get('c.Inactive'); 
        var self = this; 
        action.setParams({
            "offerId" : component.get("v.offerId"),
            "body5" : component.find("body5").get("v.value")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.Inactive1", false);
            component.set("v.SecondInactive", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);
                component.set("v.offerId", result.offerId); 
                component.set("v.parentId", result.offerId);                
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    }, 
    Inactivecancel: function(component, event) {
        component.set("v.Inactive1", false);
    },
    SecondInactivecancel: function(component, event) {
        component.set("v.SecondInactive", false);
    },
    reqAccess : function(component, event) { 
        component.set("v.reqAccess", true);       
    },    
    reqCheck: function(component, event, helper) {
        if(component.find("ChkreqAccess").get('v.value')==true)
        {
            component.find("Accessbtn1").set("v.disabled", false);
        }
        else{
            component.find("Accessbtn1").set("v.disabled", true);
        }
    },
    btnreqAccess: function(component, event, helper) {
        var action = component.get('c.RequestAccess'); 
        var self = this; 
        action.setParams({
            "offerId" : component.get("v.offerId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.reqAccess", false);
            component.set("v.SecondRequestPopup", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);
                component.set("v.offerId", result.offerId);  
                component.set("v.parentId", result.offerId);               
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },  
    reqAccesscancel: function(component, event) {
        component.set("v.reqAccess", false);
    },
    SecondRequestPopupcancel: function(component, event) {
        component.set("v.SecondRequestPopup", false);
    },
    grantAccess : function(component, event) {
        component.set("v.docId", event.target.id); 
        component.set("v.grantAccess", true);       
    },    
    grantCheck: function(component, event, helper) {
        if(component.find("chkGrant").get('v.value')==true)
        {
            component.find("Grantbtn1").set("v.disabled", false);
        }
        else{
            component.find("Grantbtn1").set("v.disabled", true);
        }
    }, 
    btnRequestForQuote: function(component, event, helper) {
        component.set("v.docId", event.target.id); 
        var action = component.get('c.GrantAccess'); 
        action.setParams({
            "docId" : component.get("v.docId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.SecondGrantPopup", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);  
                component.set("v.parentId", result.offerId);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }             
        });
        
        $A.enqueueAction(action);
    }, 
    btngrantAccess: function(component, event, helper) {
        var action = component.get('c.GrantAccess'); 
        var self = this; 
        action.setParams({
            "docId" : component.get("v.docId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.grantAccess", false);
            component.set("v.SecondGrantPopup", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);  
                component.set("v.parentId", result.offerId);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }             
        });
        $A.enqueueAction(action);
    }, 
    grantAccesscancel: function(component, event) {
        component.set("v.grantAccess", false);
         $A.get('e.force:refreshView').fire();
    },
    SecondGrantPopupcancel: function(component, event) {
        component.set("v.SecondGrantPopup", false);
         $A.get('e.force:refreshView').fire();
    },
    RejectAccess : function(component, event) { 
        component.set("v.docId", event.target.id);
        component.set("v.RejectAccess", true);       
    },    
    rejectCheck: function(component, event, helper) {
        if(component.find("chkReject").get('v.value')==true)
        {
            component.find("Rejectbtn1").set("v.disabled", false);
        }
        else{
            component.find("Rejectbtn1").set("v.disabled", true);
        }
    },
    btnrejectAccess: function(component, event, helper) {
        var action = component.get('c.rejectAccess'); 
        var self = this; 
        action.setParams({
            "docId" : component.get("v.docId"),
            "body3" : component.find("body3").get("v.value")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue(); 
            component.set("v.RejectAccess", false);
            component.set("v.SecondRejectPopup", true);
            $A.log(actionResult);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);  
                component.set("v.parentId", result.offerId);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    }, 
    rejectAccesscancel: function(component, event) {
        component.set("v.RejectAccess", false);
    },
    POPrequestForInfocel: function(component, event) {
        component.set("v.POPrequestForInfo", false);
    },
    
    SecondRejectPopupcancel: function(component, event) {
        component.set("v.SecondRejectPopup", false);
    },
    RevokeAccess : function(component, event) { 
        component.set("v.docId", event.target.id);
        component.set("v.RevokeAccess", true);       
    },   
    revokeCheck: function(component, event, helper) {
        if(component.find("chkRevoke").get('v.value')==true)
        {
            component.find("Revokebtn1").set("v.disabled", false);
        }
        else{
            component.find("Revokebtn1").set("v.disabled", true);
        }
    }, 
    btnrevokeAccess: function(component, event, helper) {
        var action = component.get('c.revokeAccess'); 
        var self = this; 
        action.setParams({
            "docId" : component.get("v.docId"),
            "body4" : component.find("body4").get("v.value")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.RevokeAccess", false);
            component.set("v.SecondRevokePopup", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);  
                component.set("v.parentId", result.offerId);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    }, 
    revokeAccesscancel: function(component, event) {
        component.set("v.RevokeAccess", false);
    }, 
    SecondRevokePopupcancel: function(component, event) {
        component.set("v.SecondRevokePopup", false);
    },
    BidWithdrawn : function(component, event) { 
        component.set("v.syndId", event.target.id); 
        component.set("v.BidWithdrawn", true);       
    },     
    BidWidrawnCheck: function(component, event, helper) {
        if(component.find("chkBidWidrawn").get('v.value')==true)
        {
            component.find("BidWidrawnbtn1").set("v.disabled", false);
        }
        else{
            component.find("BidWidrawnbtn1").set("v.disabled", true);
        }
        
    }, 
    btnBidWidrawn: function(component, event, helper) {
        var action = component.get('c.postWithdrawn'); 
        var self = this; 
        action.setParams({
            "syndId" : component.get("v.syndId"),
            "body1" : component.find("body1").get("v.value")
        });
        action.setCallback(self, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.BidWithdrawn", false);
            component.set("v.SecondBidWithdrawn", true);
            $A.log(actionResult);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);  
                component.set("v.parentId", result.offerId);
                $A.get('e.force:refreshView').fire();
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    }, 
    BidWidrawncancel: function(component, event) {
        component.set("v.BidWithdrawn", false);
        $A.get('e.force:refreshView').fire();
    },
    SecondBidWithdrawncancel: function(component, event) {
        component.set("v.SecondBidWithdrawn", false);
         $A.get('e.force:refreshView').fire();
    },
    PreWithdraw : function(component, event) { 
        component.set("v.syndId", event.target.id); 
        component.set("v.PreWithdrawn", true);       
    }, 
    PreWithdrawnCheck: function(component, event, helper) {
        if(component.find("chkPreWithdrawn").get('v.value')==true)
        {
            component.find("PreWithdrawnbtn1").set("v.disabled", false);
        }
        else{
            component.find("PreWithdrawnbtn1").set("v.disabled", true);
        }
    }, 
    btnPreWithdrawn: function(component, event, helper) {
        var action = component.get('c.PreWithdraw1'); 
        var self = this; 
        action.setParams({
            "syndId" : component.get("v.syndId"),
            "body2" : component.find("body2").get("v.value")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.PreWithdrawn", false);
            component.set("v.SecondPreWithdraw", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result); 
                component.set("v.parentId", result.offerId); 
                 $A.get('e.force:refreshView').fire();
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        
        $A.enqueueAction(action);
    }, 
    PreWithdrawncancel: function(component, event) {
        component.set("v.PreWithdrawn", false);
         $A.get('e.force:refreshView').fire();
    },    
    SecondPreWithdrawcancel: function(component, event) {
        component.set("v.SecondPreWithdraw", false);
    }, 
    AgreeBid: function(component, event) { 
        
        component.set("v.syndId", event.target.id); 
        component.set("v.AgreetoTerms", true);       
    }, 
    AgreeBidcheck: function(component, event, helper) {
        debugger;
        //var ACDate = component.find("AntDate").get("v.value");
        if(component.find("chkAgreeBid").get('v.value') == true) {// && ACDate != "" && ACDate != undefined){
            component.find("AgreeBidbtn1").set("v.disabled", false);
        } else {
            component.find("AgreeBidbtn1").set("v.disabled", true);
        }
    }, 
    btnAgreeBid: function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        /*
        var dateFormat = $A.get("$Locale.dateFormat");
        
        var ACDate = component.find("AntDate").get("v.value");
        
        var dateString = $A.localizationService.formatDateTime(ACDate, dateFormat);
        var date = new Date(dateString);
        
        dateString = $A.localizationService.formatDateTime(new Date(), dateFormat);
        var today = new Date(dateString); 
        
        var day1 = date.getDay();
        
        if(ACDate == undefined || ACDate == "")
        {
            errorFlag = 'true';
            component.set("v.acDate", 'Input field required');
        }
        else
        {
            if(helper.checkDate(ACDate))
            {
                errorFlag = 'true';
                component.set("v.acDate", 'Please enter valid date'); 
            } 
            else if(today > date)
            {
                errorFlag = 'true';
                component.set("v.acDate", 'Please enter a date in the future');
            }
                else if(day1 == 0 || day1 == 6)
                {
                    errorFlag = 'true';
                    component.set("v.acDate", 'Choose any weekdays'); 
                }
                    else
                    { 
                        component.set("v.acDate", ''); 
                    } 
        }
        */
        
        if(errorFlag == 'false')
        { 
            var action = component.get('c.AgreeBid1'); 
            var self = this; 
            action.setParams({
                "syndId" : component.get("v.syndId"),
                "body4" : component.find("body4").get("v.value"),
                "ACDate": component.find("AntDate").get("v.value")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                component.set("v.AgreetoTerms", false);
                component.set("v.SecondAgree", true);
                if(state === "SUCCESS"){
                    component.set("v.Offer", result);  
                    component.set("v.parentId", result.offerId);
                    $A.get('e.force:refreshView').fire();
                }
                else if(state === "ERROR"){
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    }, 
    AgreeBidcancel: function(component, event) {
        component.set("v.AgreetoTerms", false);
        $A.get('e.force:refreshView').fire();
    },
    SecondAgreecancel: function(component, event) {
        component.set("v.SecondAgree", false);
 $A.get('e.force:refreshView').fire();
    },
    BidReject: function(component, event) { 
        component.set("v.syndId", event.target.id); 
        component.set("v.BidReject", true);       
    },    
    BidRejectcheck: function(component, event, helper) {
        if(component.find("chkBidReject").get('v.value')==true)
        {
            component.find("BidRejectbtn1").set("v.disabled", false);
        }
        else{
            component.find("BidRejectbtn1").set("v.disabled", true);
        }
    },
    btnBidReject: function(component, event, helper) {
        var action = component.get('c.BidReject1'); 
        var self = this; 
        action.setParams({
            "syndId" : component.get("v.syndId"),
            "body3" : component.find("body3").get("v.value")/*jaya*/
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.BidReject", false);
            component.set("v.SecondBidReject", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result); 
                component.set("v.parentId", result.offerId); 
                $A.get('e.force:refreshView').fire();
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    }, 
    BidRejectcancel: function(component, event) {
        component.set("v.BidReject", false);
        $A.get('e.force:refreshView').fire();
    },
    SecondBidRejectcancel: function(component, event) {
        component.set("v.SecondBidReject", false);
    },
    CmpTrans: function(component, event) { 
        component.set("v.syndId", event.target.id);
        component.set("v.CmpTrans", true);       
    },    
    CmpTranscheck: function(component, event, helper) {
        if(component.find("chkCmpTrans").get('v.value')==true)
        {
            component.find("CmpTransbtn1").set("v.disabled", false);
        }
        else{
            component.find("CmpTransbtn1").set("v.disabled", true);
        }
    },
    btnCmpTrans: function(component, event, helper) {
        debugger;
        var action = component.get('c.createFinalized'); 
        var self = this; 
        
        action.setParams({
            "offerId" : component.get("v.offerId"),
            "syndId" : event.target.id
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.CompletePopup", true);
                /*$A.get('e.force:refreshView').fire();
                var showToast = $A.get('e.force:showToast');
                showToast.setParams(
                    {
                        'title': 'Success: ',
                        'message': 'Transaction Updated Sucessfully',
                        'type': 'Success'
                    }
                );
                showToast.fire();*/
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        
        $A.enqueueAction(action);
    }, 
    CmpTranscancel: function(component, event) {
        component.set("v.CmpTrans", false);
    },
    EditofferNotes : function(component, event, helper) { 
        component.set("v.EditofferNotes", true);       
    }, 
    EditcancelNotes : function(component, event) {
        component.set("v.EditofferNotes", false);
    },
    
    updateSellerNotes: function(component, event, helper) {
        
        var OfferObjectnote = component.get("v.Offer.OfferExtended.Offer");
        // Create the action
        var action = component.get("c.updateNotes");
        action.setParams({ "saveSellerOffersNotes": OfferObjectnote });
        
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.Offer.OfferExtended.Offer", response.getReturnValue());
                component.set("v.showpopup", false);
                $A.get('e.force:refreshView').fire();
                var showToast = $A.get('e.force:showToast');
                showToast.setParams(
                    {
                        'title': 'Success: ',
                        'message': 'Transaction notes Updated Sucessfully',
                        'type': 'Success'
                    }
                );
                showToast.fire();
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        
        // Send action off to be executed
        $A.enqueueAction(action);
    },
    Editoffer : function(component, event, helper) { 
        component.set("v.Editoffer", true);      
        var offerObject = component.get("v.Offer.OfferExtended.Offer"); 
        $A.createComponent("c:"+offerObject.Component_Name__c,
                           {
                               "aura:id": 'MyCustomComponentEdit',
                               "edit": 'true',
                               "mode": 'EDIT',
                               "objectApiName": offerObject.SObject_Name__c,
                               "recordId": offerObject.Record_Id__c
                           },
                           function(newComponent, status, errorMessage){ 
                               var targetCmp = component.find('editPlaceHolder');
                               targetCmp.set("v.body", []); 
                               var body = targetCmp.get("v.body");
                               body.push(newComponent);
                               targetCmp.set("v.body", body); 
                           });
        debugger;
        var ParticipationAmount = component.find("ParticipationAmount").get("v.value");        
        if(ParticipationAmount != undefined) { 
            
            var val = helper.numberWithoutCommas(ParticipationAmount); 
            var val1 = helper.numberWithCommas(val);
            component.set("v.Offer.OfferExtended.Offer.ParticipationAmount__c", val1);
        }
        
        var ParticipationPercentage = component.find("ParticipationPercentage").get("v.value"); 
        if(ParticipationPercentage != undefined) { 
            var val = helper.numberWithoutCommas(ParticipationPercentage); 
            var val1 = helper.round(val, 3); 
            var val2 = helper.numberWithCommas(val1);
            component.set("v.Offer.OfferExtended.Offer.ParticipationPercentage__c", val2); 
        }
        
    }, 
    Editcancel : function(component, event) {
        component.set("v.Editoffer", false);
        component.set("v.validation", false);
        component.set("v.loanError", '');
        component.set("v.templateError", '');
        component.set("v.nameError", '');
        component.set("v.participationAmountError", '');
        component.set("v.participationPercentageError", '');
        $A.get('e.force:refreshView').fire();
    },
    changeParticipationAmount: function(component, event, helper) { 
        var ParticipationAmount = component.find("ParticipationAmount").get("v.value");        
        if(ParticipationAmount != undefined)
        { 
            var val = helper.numberWithoutCommas(ParticipationAmount); 
            var val1 = helper.numberWithCommas(val);
            //component.find("ParticipationAmount").set("v.value",val1);
            component.set("v.Offer.OfferExtended.Offer.ParticipationAmount__c", val1);
        }  
    },
    changeParticipationPercentage: function(component, event, helper) {  
        var ParticipationPercentage = component.find("ParticipationPercentage").get("v.value"); 
        if(ParticipationPercentage != undefined)
        { 
            var val = helper.numberWithoutCommas(ParticipationPercentage); 
            var val1 = helper.round(val, 3); 
            var val2 = helper.numberWithCommas(val1);
            //component.find("ParticipationPercentage").set("v.value", val2);
            component.set("v.Offer.OfferExtended.Offer.ParticipationPercentage__c", val2); 
        }         
    }, 
    ParticipationAmount: function(component, event, helper) {
        var inp = component.find("ParticipationAmount").get("v.value"); 
        if(inp == 0)
            //component.find("ParticipationAmount").set("v.value", "");
            component.set("v.Offer.OfferExtended.Offer.ParticipationAmount__c", null);
    },
    ParticipationPercentage: function(component, event, helper) {
        var inp = component.find("ParticipationPercentage").get("v.value"); 
        if(inp == 0.000)
            //component.find("ParticipationPercentage").set("v.value","");
            component.set("v.Offer.OfferExtended.Offer.ParticipationPercentage__c", null);
    },
    SaveEditOffer : function(component, event, helper) { 
        var errorFlag = 'false';
        var offerName = component.find("offername").get("v.value");
        var participationAmountField = component.find("ParticipationAmount");
        
        var participationAmount;
        if(participationAmountField){
            participationAmount = participationAmountField.get("v.value");
        }
        
        var participationPercentageField = component.find("ParticipationPercentage");
        var participationPercentage;
        if(participationPercentageField){
            participationPercentage = component.find("ParticipationPercentage").get("v.value");
        }
        
        /*
         * commented by jaya since name field is not needed
         * 
        if(offerName == '' || offerName == null || offerName == undefined || offerName.trim().length*1 == 0) 
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.nameError", 'Input field required'); 
        }
        else
        { 
            if(helper.checkSpecialCharecter1(offerName))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.nameError", 'The product name has special characters. These are not allowed.'); 
            } 
            else{  
                component.set("v.nameError", ''); 
            }  
        }
        */
        
        /*
         * commented by jaya since participation amount is not needed
         * 
        if(participationAmount == undefined || participationAmount == ''){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.participationAmountError", 'Input field required'); 
        } else {   
            var numWithoutComma = helper.numberWithoutCommas(participationAmount);
            if(helper.checkSpecialCharecter(numWithoutComma) || helper.checkCharecter(numWithoutComma)) { 
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.participationAmountError", 'Please enter a numeric value.'); 
            } else {
                if(numWithoutComma <= 0)
                {
                    errorFlag = 'true';
                    component.set("v.validation", true);
                    component.set("v.participationAmountError", 'Enter a value greater than 0'); 
                } else {
                    component.set("v.participationAmountError", '');
                }
            }
        }
		*/
        
        /*
         * commented by jaya since participation percentage is not needed
         * 
        if(participationPercentage == undefined || participationPercentage == '') {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.participationPercentageError", 'Input field required'); 
        } else {
            var numWithoutComma = helper.numberWithoutCommas(participationPercentage);
            if(helper.checkSpecialCharecter(numWithoutComma) || helper.checkCharecter(numWithoutComma)) { 
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.participationPercentageError", 'Please enter a numeric value.'); 
            } else {
                if(isNaN(numWithoutComma) || numWithoutComma <= 0 || numWithoutComma > 99)
                {
                    errorFlag = 'true';
                    component.set("v.validation", true);
                    component.set("v.participationPercentageError", 'Enter a value between 1% - 99%'); 
                } else {
                    component.set("v.participationPercentageError", '');  
                }
            }
        }
        */
        
        if(errorFlag == 'false')
        { 
            var img = component.find("pdfImgloading");
            
            $A.util.removeClass(img,'slds-hide');
            $A.util.addClass(img,'slds-show');  
            
            component.find("btnSaveTrans").set("v.disabled", true); 
            debugger;            
            var customComponent = component.find("MyCustomComponentEdit");
            customComponent.find("recordCreator").saveRecord(function(saveResult) {
                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                    customComponent.set("v.recordId", saveResult.recordId);
                    component.set("v.Offer.OfferExtended.Offer.Record_Id__c", customComponent.get("v.recordId")); 
                    var OfferObject = component.get("v.Offer.OfferExtended.Offer"); 
                    var action = component.get("c.editSellerOffersItems");
                    action.setParams({ "saveSellerOffers": OfferObject });
                    action.setCallback(this, function(response) {
                        debugger;
                        var offerId = response.getReturnValue();
                        if(offerId != '') {
                            if(offerId == 'Already Used'){
                                component.find("btnSaveTrans").set("v.disabled", true); 
                                errorFlag = 'true';
                                component.set("v.validation", true);
                                component.set("v.nameError", 'This Transaction Name has already been used. Please enter a new Transaction Name.');
                                
                                $A.util.addClass(img,'slds-hide'); 
                                $A.util.removeClass(img,'slds-show');
                                
                            } else { 
                                $A.util.addClass(img,'slds-hide'); 
                                $A.util.removeClass(img,'slds-show');
                                component.set("v.showpopup", false);
                                $A.get('e.force:refreshView').fire();
                                var showToast = $A.get('e.force:showToast');
                                showToast.setParams(
                                    {
                                        'title': 'Success: ',
                                        'message': 'Transaction Updated Sucessfully',
                                        'type': 'Success'
                                    }
                                );
                                showToast.fire();
                            }
                        } else { 
                            component.find("btnSaveTrans").set("v.disabled", false); 
                            component.set("v.validation", false);
                            component.set("v.Editoffer", false);
                            
                            $A.util.addClass(img,'slds-hide'); 
                            $A.util.removeClass(img,'slds-show');
                            
                            var showToast = $A.get('e.force:showToast');
                            showToast.setParams(
                                {
                                    'title': 'Failure : ',
                                    'message': 'Unable to Update Transaction',
                                    'type': 'Success'
                                }
                            );
                            showToast.fire();
                        } 
                    });
                    $A.enqueueAction(action);
                } else if (saveResult.state === "INCOMPLETE") {
                    component.find("btnSaveTrans").set("v.disabled", false); 
                    
                    $A.util.addClass(img,'slds-hide'); 
                    $A.util.removeClass(img,'slds-show');
                    
                    console.log("User is offline, device doesn't support drafts.");
                } else if (saveResult.state === "ERROR") {
                    component.find("btnSaveTrans").set("v.disabled", false); 
                    
                    $A.util.addClass(img,'slds-hide'); 
                    $A.util.removeClass(img,'slds-show');
                    
                    console.log('Problem saving transaction, error: ' + JSON.stringify(saveResult.error));
                } else {
                    component.find("btnSaveTrans").set("v.disabled", false); 
                    
                    $A.util.addClass(img,'slds-hide'); 
                    $A.util.removeClass(img,'slds-show');
                    
                    console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
                }
            });
        }
    },
    partcipation: function(component, event, helper) {
        var selected = component.find("levels").get("v.value"); 
    }, 
    LoanType: function(component, event, helper) {
        var selected = component.find("loan").get("v.value"); 
    },
    delete1: function(component, event, helper) {
        var action = component.get('c.DeleteFile');  
        action.setParams({
            "attId" : event.target.id 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){      
                component.set("v.Offer", result);
                component.set("v.DeleteFile", true);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action); 
    },
    delAttach: function(component, event, helper) {
        var action = component.get('c.DeleteAttach');  
        action.setParams({
            "attId" : event.target.id 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){      
                component.set("v.Offer", result);
                component.set("v.DeleteFile", true);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action); 
    },
    DeleteFilecancel: function(component, event, helper) {
        component.set('v.DeleteFile', false);
    }, 
    loadFirstTab: function(component, event, helper) {
        //component.set("v.hideDiv", true);
    },
    loadSecondTab: function(component, event, helper) {
        component.set("v.hideDiv", false);
    },
    
    hoverCreditUnion: function(component, event) { 
        var action = component.get('c.CUDetails'); 
        action.setParams({
            "CUId" : event.target.id
        });       
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                component.set("v.CUUser", result); 
                component.set("v.popoverCreditUnion", true);
            } 
        });
        $A.enqueueAction(action);
    },
    hoverCreditUnionhide: function(component, event) { 
        component.set("v.popoverCreditUnion", false);
    },  
    hoverBuyerUser: function(component, event) { 
        debugger;
        var action = component.get('c.UserDetails1'); 
        action.setParams({
            "attachId" : event.target.id
        });       
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                component.set("v.user", result);
                component.set("v.popoverBidBuyerName", true);
            } 
        });
        $A.enqueueAction(action);
    },
    hoverBidBuyerUser: function(component, event) { 
        var action = component.get('c.UserDetails2'); 
        action.setParams({
            "syndId1" : event.target.id
        });       
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                component.set("v.user", result); 
                component.set("v.popoverBidBuyerName", true);
            } 
        });
        $A.enqueueAction(action);
    },
    hoverBidbuyerhide: function(component, event) { 
        component.set("v.popoverBidBuyerName", false); 
    },
    showNotes:function(component, event, helper) {
        component.set("v.docId", event.target.id);
        component.set("v.ShowNotes", true);
    },
    showNotesCancel: function(component, event) {
        component.set("v.ShowNotes", false);
        component.set("v.showSaveNote", '');
    },
    loadNotes:function(component, event) {
        debugger;
        component.set("v.docId", event.target.id); 
        component.set("v.showRequestNotes", []);
        var alltrs = document.getElementsByClassName('hidden-tr');
        for (var i = 0; i < alltrs.length; i++) {
            alltrs[i].style.display = 'none';
        }
        var action = component.get('c.showLoadNotes');
        action.setParams({
            "offerId" : component.get("v.offerId"),
            "docId" : component.get("v.docId"),
        });
        action.setCallback(this, function(actionResult) { 
            debugger;
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.showRequestNotes", result);
                var showselleernotechk = component.get("v.showsellernotes");
                if(showselleernotechk){
                    component.set("v.showsellernotes", false);
                }else
                {
                    component.set("v.showsellernotes", true);
                }
                
                var elementId ='tr_'+event.target.id;
                document.getElementById(elementId).style.display = 'table-row';
                
            }
        });
        $A.enqueueAction(action);
    },
    showNotesSave:function(component, event) {
        var savNote = component.find("body").get("v.value");
        if(savNote == '' || savNote == null || savNote == undefined || savNote.trim().length*1 ==0)
        {            
            component.set("v.showSaveNote", 'Input field required'); 
            component.find("body").set("v.value", '')
        }
        else{
            
            component.set("v.showSaveNote", ''); 
            var action = component.get('c.showSaveNotes');
            
            var isBuyer = component.get("v.Offer.isBuyer");
            var isBuyerValidator = component.get("v.Offer.isBuyerValidator");
            var isSellerOrBuyer = (isBuyer || isBuyerValidator) ? false : true;
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "docId" : component.get("v.docId"),
                "isSellerOrBuyer" : isSellerOrBuyer,
                "body" : component.find("body").get("v.value")  
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                component.set("v.ShowNotes", false);
                if(state === "SUCCESS"){
                    component.set("v.showRequestNotes", result);
                    var elementId ='tr_'+event.target.id;
                    document.getElementById(elementId).style.display = 'table-row';
                }
            });
            
            $A.enqueueAction(action);
        }
    },
    
    requestPopNotes: function(component, event) {
        component.set("v.RequestNotes", true);
    },
    RequestNotesCancel: function(component, event) {
        component.set("v.RequestNotes", false);
        component.set("v.requestSaveNote", '');
    },
    RequestNotesSave: function(component, event, helper) {
        debugger;
        var savNote = component.find("body").get("v.value");
        if(savNote == '' || savNote == null || savNote == undefined || savNote.trim().length*1 ==0)
        {            
            component.set("v.requestSaveNote", 'Input field required'); 
            component.find("body").set("v.value", '')
        }
        else{
            debugger;
            component.set("v.requestSaveNote", ''); 
            var action = component.get('c.requestSaveNotes');
            
            var isBuyer = component.get("v.Offer.isBuyer");
            var isBuyerValidator = component.get("v.Offer.isBuyerValidator");
            var isSellerOrBuyer = (isBuyer || isBuyerValidator) ? false : true;
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "docId" : component.get("v.docId"),
                "isSellerOrBuyer" : isSellerOrBuyer,
                "body" : component.find("body").get("v.value")  
            });
            
            action.setCallback(this, function(actionResult) { 
                debugger;
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                component.set("v.RequestNotes", false);
                if(state === "SUCCESS"){
                    component.set("v.Offer", result);
                                        $A.get('e.force:refreshView').fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            
            $A.enqueueAction(action);
        }
    }, 
    popNotes: function(component, event) {
        component.set("v.Notes", true);
    },
    NotesCancel: function(component, event) {
        component.set("v.Notes", false);
        component.set("v.saveNote", '');
    },
    NotesSave: function(component, event, helper) {
        var savNote = component.find("body").get("v.value");
        if(savNote == '' || savNote == null || savNote == undefined || savNote.trim().length*1 ==0)
        {            
            component.set("v.saveNote", 'Input field required'); 
            component.find("body").set("v.value", '')
        }
        else{
            component.set("v.saveNote", ''); 
            var action = component.get('c.saveNotes');
            
            var isBuyer = component.get("v.Offer.isBuyer");
            var isBuyerValidator = component.get("v.Offer.isBuyerValidator");
            var isSellerOrBuyer = (isBuyer || isBuyerValidator) ? false : true;
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : isSellerOrBuyer,
                "body" : component.find("body").get("v.value")  
            });
            
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                component.set("v.Notes", false);
                $A.log(actionResult);
                if(state === "SUCCESS"){
                    component.set("v.Offer", result);
                                        $A.get('e.force:refreshView').fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            
            $A.enqueueAction(action);
        }
    }, 
    OriginalPrincipal: function(component, event, helper) {
        var inp = component.find("oPrincipal").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.OriginalPrincipal__c","");
    },
    Wac: function(component, event, helper) {
        var inp = component.find("wac").get("v.value"); 
        if(inp == 0.000)
            component.set("v.Offer.OfferExtended.Offer.WAC__c","");
    },
    CurrentPrincipal: function(component, event, helper) {
        var inp = component.find("cPrincipal").get("v.value"); ;
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.OutstandingPrincipal__c","");
    },
    WAFICO: function(component, event, helper) {
        var inp = component.find("fico").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.FICO_WA__c","");
    },
    NumOfLoan: function(component, event, helper) {
        var inp = component.find("nof").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.NumberOfLoans__c","");
    },
    MinFICO: function(component, event, helper) {
        var inp = component.find("minfico").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.MinFICO__c","");
    },
    AvgLoanAmount: function(component, event, helper) {
        var inp = component.find("avgloan").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.AverageLoanAmount__c","");
    },
    WAOriginal: function(component, event, helper) {
        var inp = component.find("oTerm").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.Term_WA__c","");
    },
    WALTV: function(component, event, helper) {
        var inp = component.find("ltv").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.LoanToValue__c","");
    },
    WARemaining: function(component, event, helper) {
        var inp = component.find("Rmonth").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.RemainingTerm__c","");
    },
    WADTI: function(component, event, helper) {
        var inp = component.find("dti").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.WA_DTI__c","");
    },
    New: function(component, event, helper) {
        var inp = component.find("newper").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.NewPercentage__c","");
    },
    Indirect: function(component, event, helper) {
        var inp = component.find("indirect").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.IndirectPercentage__c","");
    },
    AvgVehAge: function(component, event, helper) {
        var inp = component.find("AvgVhclAge").get("v.value"); 
        if(inp == 0.0)
            component.set("v.Offer.OfferExtended.Offer.AverageVehicleAge__c","");
    },
    ChargeOff: function(component, event, helper) {
        var inp = component.find("ChrgOff").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.Charge_Off__c","");
    },
    WALoan: function(component, event, helper) {
        var inp = component.find("WaLifeLoan").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.Average_Life_of_Loan__c","");
    },
    PortPrice: function(component, event, helper) {
        var inp = component.find("PPrice").get("v.value"); 
        if(inp == 0.00)
            component.set("v.Offer.OfferExtended.Offer.PortfolioPrice__c","");
    },
    ServFee: function(component, event, helper) {
        var inp = component.find("SFee").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.ServicingFee__c","");
    },
    bidNameClick: function(component, event, helper) {
        debugger;
        var address = "/bid-detail?offerId=&syndId="+ event.target.id;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": address,
            "isredirect" :false
        });
        urlEvent.fire();
    },
    refreshModel: function (component, event, helpler) {
        
        var action = component.get('c.getContentVersions');  
        action.setParams({
            "parentId" : component.get("v.parentId"),
            "description" : ''
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                debugger;
                component.set("v.Offer.content", result);
                
                component.set("v.opendocvault", false);
                component.set("v.popdocuvault", false);
                var img = component.find("imgloading");
        $A.util.addClass(img,'slds-hide'); 
        $A.util.removeClass(img,'slds-show');
        component.set("v.FileUploadMsg", 'File(s) have been uploaded successfully');
        component.set("v.FileUploadMsgSorE", 'Success');
        component.set("v.FileUpload", true);
                //component.set("v.Offer.attachment", result); 
            }
            else if(state === "ERROR") {
                $A.log('error');
            }
        });
        $A.enqueueAction(action); 
    },
    refreshModelDue: function (component, event, helpler) {
        var img = component.find("imgloadingDue");
        $A.util.addClass(img,'slds-hide'); 
        $A.util.removeClass(img,'slds-show');
        component.set("v.FileUploadMsg", 'File(s) have been uploaded successfully');
        component.set("v.FileUploadMsgSorE", 'Success');
        component.set("v.FileUpload", true);
        var action = component.get('c.getContentVersions');  
        action.setParams({
            "parentId" : component.get("v.parentId"),
            "description" : 'due'
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){      
                component.set("v.Offer.duecontent", result);
                //component.set("v.Offer.dueAttachment", result); 
            }
            else if(state === "ERROR") {
                $A.log('error');
            }
        });
        $A.enqueueAction(action); 
    },
    ChooseFile : function(component, event) {        
        component.set("v.FileUploadMsg", 'Please choose the file(s)');
        component.set("v.FileUploadMsgSorE", 'Error');
        component.set("v.FileUpload", true); 
    },
    ChooseFailure : function(component, event) {        
        component.set("v.FileUploadMsg", 'An error has occurred. Please contact us at\nLoanParticipationBeta@cunamutual.com.');
        component.set("v.FileUploadMsgSorE", 'Error');        
        component.set("v.FileUpload", true); 
    },
    fileExtension : function(component, event) {        
        component.set("v.FileUploadMsg", 'This file format not allowed to upload');
        component.set("v.FileUploadMsgSorE", 'Error');        
        component.set("v.FileUpload", true); 
    },
    LoaderDisplay : function(component, event) {
        var img = component.find("imgloading");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');  
    },
    LoaderDisplayDue : function(component, event) {
        var img = component.find("imgloadingDue");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');  
    },
    FileUploadcancel : function(component, event) {
        var img = component.find("imgloading");
        $A.util.addClass(img,'slds-hide'); 
        $A.util.removeClass(img,'slds-show');
        var img1 = component.find("imgloadingDue");
        $A.util.addClass(img1,'slds-hide'); 
        $A.util.removeClass(img1,'slds-show');
        component.set("v.FileUpload", false); 
    },    
    BidDue : function(component, event) {
        component.set("v.syndId", event.target.id); 
        component.set("v.BidDue", true); 
    },
    BidDueCheck : function(component, event, helper) {
        if(component.find("chkBidDue").get('v.value') == true) {
            component.find("BidDuebtn1").set("v.disabled", false);
        }
        else{
            component.find("BidDuebtn1").set("v.disabled", true);
        }
    },
    btnBidDue: function(component, event, helper) {
        debugger;
        var action = component.get('c.BidDue1'); 
        action.setParams({
            "syndId" : component.get("v.syndId"),
            "body6" : component.find("body6").get("v.value")
        });
        action.setCallback(self, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.BidDue", false);
            component.set("v.SecondBidDue", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);  
                component.set("v.parentId", result.offerId);  
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },
    BidDuecancel : function(component, event) {
        component.set("v.BidDue", false);
    },
    SecondBidDuecancel : function(component, event) {
        component.set("v.SecondBidDue", false);
    },
    counterPopupCancel : function(component, event) {
        var action = component.get('c.getCounterParties');  
        action.setParams({
            "offerId" : component.get("v.Offer.OfferExtended.Offer.Id")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.Offer.OfferExtended.CounterParties", result);
                component.set("v.counterPopup", false);
            }
        });
        $A.enqueueAction(action);
        component.set("v.counterPopup", false);
    },
    showCounterParties: function(component, event) {
        debugger;
        var action = component.get('c.getApprovedCreditUnions');  
        action.setParams({
            "creditUnionId" : component.get("v.Offer.OfferExtended.CreditUnion.Id"),
            "offerId" : component.get("v.Offer.OfferExtended.Offer.Id")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                debugger;
                component.set("v.ApprovedCounterParties", result);
                component.set("v.counterPopup", true);
            }
        });
        $A.enqueueAction(action);
    },
    addCounterPartiespop : function(component, event) { 
        debugger;
        component.set("v.newcounterPopup", true);       
    },
    popcounterPopupCancel : function(component, event) { 
        debugger;
        component.set("v.newcounterPopup", false);       
    },
    addCounterParties: function(component, event) {
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        var Bankname = component.find("Banknameval").get("v.value");
        var emval = component.find("emailval").get("v.value");
        var mval = component.find("mobileval").get("v.value");
        var vcode =  component.find("code").get("v.value");
        var vcity =component.find("city").get("v.value");
        var vpcontact =component.find("pcontact").get("v.value");
        var vwebsite =component.find("website").get("v.value");
        var vstate =component.find("state").get("v.value");
        
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var regPhoneNo = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        if(mval == '' || mval == 'undefined')
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.mobileError", 'Input field required'); 
        }
        else if(!$A.util.isEmpty(mval))
        {   
            if(!mval.match(regPhoneNo))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.mobileError", 'Please enter valid phone number.'); 
            } 
            else{
                component.set("v.mobileError", ''); 
            }
        }
       if(emval == '' || emval == 'undefined')
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.emailError", 'Input field required'); 
        }
        else if(!$A.util.isEmpty(emval))
        {   
            if(!emval.match(regExpEmailformat))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.emailError", 'Please Enter a Valid Email Address'); 
            }
            else
            {
                component.set("v.emailError", ''); 
            }
        }

        if(Bankname == '' || Bankname == 'undefined' || Bankname == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.cpError", 'Input field required'); 
        } else { 
            component.set("v.cpError", ''); 
        } 
         if(vcode == '' || vcode == 'undefined' || vcode == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.codeerror", 'Input field required'); 
        } else { 
            component.set("v.codeerror", ''); 
        } 
         if(vcity == '' || vcity == 'undefined'|| vcity == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.cityerror", 'Input field required'); 
        } else { 
            component.set("v.cityerror", ''); 
        } 
         if(vpcontact == '' || vpcontact == 'undefined' || vpcontact == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Pcontacterror", 'Input field required'); 
        } else { 
            component.set("v.Pcontacterror", ''); 
        } 
        if(vwebsite == '' || vwebsite == 'undefined' || vwebsite == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.weberror", 'Input field required'); 
        } else { 
            component.set("v.weberror", ''); 
        } 
        if(vstate == '' || vstate == 'undefined' || vstate == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Stateerror", 'Input field required'); 
        } else { 
            component.set("v.Stateerror", ''); 
        } 
        if(errorFlag == 'false') {
        console.log('@@'+component.get("v.counterpartyname"));
        var action = component.get('c.addCounterPartiesname');  
        action.setParams({
            "saveCreditunion" : component.get("v.Creditunion"),
            "PName" : component.get("v.Partname"),
            "PID" : component.get("v.PartyID")
        });
        debugger;
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.newcounterPopup", false);
                component.set("v.onboard", true);
            }
        });
        debugger;
        $A.enqueueAction(action);
        }
    },
    onboard: function(component, event) {
        component.set("v.onboard", false);
    },
    selectCounterParties: function(component, event) {
        debugger;
        var checkbox = event.getSource();
        var checked = checkbox.get("v.value");
        var creditUnionId = checkbox.get("v.text");    
        var action = component.get('c.addRemoveCounterParty');  
        action.setParams({
            "creditUnionId" : creditUnionId,
            "offerId" : component.get("v.Offer.OfferExtended.Offer.Id"),
            "forAdd" : checked
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
            }
        });
        $A.enqueueAction(action);
    },
    
    approveValidation : function(component, event) { 
        component.set("v.ApproveValidation", true);       
    },
    approveValidationCancel : function(component, event) { 
        component.set("v.ApproveValidation", false);       
    },
    resubmitValidationCancel : function(component, event) { 
        component.set("v.ResubmitValidation", false);       
    },
    submitForApproval: function(component, event, helper) {
        debugger;
        var savNote = component.find("bodyValidation").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteValidation", 'Input field required'); 
            component.find("bodyValidation").set("v.value", '')
        } else {
            component.set("v.saveNoteValidation", ''); 
            var action = component.get('c.SubmitForApprove'); 
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.ApproveValidation", false); 
                    component.set("v.Offer", result);
                    component.set("v.offerId", result.offerId);  
                    component.set("v.parentId", result.offerId);  
                    
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction Updated Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    },
    
    rejectValidation : function(component, event) { 
        component.set("v.RejectValidation", true);       
    },
    rejectValidationCancel : function(component, event) { 
        component.set("v.RejectValidation", false);       
    },
    rejectForApproval: function(component, event, helper) {
        debugger;        
        var savNote = component.find("body").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNote", 'Input field required'); 
            component.find("body").set("v.value", '')
        } else {
            component.set("v.saveNote", ''); 
            var action = component.get('c.RejectValidation');
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote  
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                component.set("v.RejectValidation", false);       
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.Offer", result);
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            
            $A.enqueueAction(action);
        }        
    }, 
    Resubmitforvalidate : function(component, event) { 
        debugger;
        component.set("v.ResubmitValidation", true);
        component.set("v.setTargetids",event.target.id)
    },
    Resubmitforvalidatesave : function(component, event) { 
        debugger;
        
        var action = component.get('c.Resubmitforvalidatecall'); 
        action.setParams({
            "PCPids" : component.get("v.setTargetids")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){ 
                var savNote = component.find("bodyValidation").get("v.value");
                if(savNote == '' || 
                   savNote == null || 
                   savNote == undefined || 
                   savNote.trim().length*1 == 0) {            
                    component.set("v.saveNoteValidation", 'Input field required'); 
                    component.find("bodyValidation").set("v.value", '')
                } else {
                    component.set("v.saveNoteValidation", ''); 
                    var action = component.get('c.reSubmitForApprove'); 
                    
                    action.setParams({
                        "offerId" : component.get("v.offerId"),
                        "isSellerOrBuyer" : true,
                        "body" : savNote,
                        "counterids" :component.get("v.setTargetids")
                    });
                    action.setCallback(this, function(actionResult) { 
                        var state = actionResult.getState(); 
                        if(state === "SUCCESS"){
                            var result = actionResult.getReturnValue();
                            component.set("v.ResubmitValidation", false); 
                            component.set("v.Offer", result);
                            component.set("v.offerId", result.offerId);  
                            component.set("v.parentId", result.offerId);  
                            
                            $A.get('e.force:refreshView').fire();
                            var showToast = $A.get('e.force:showToast');
                            showToast.setParams(
                                {
                                    'title': 'Success: ',
                                    'message': 'Transaction Updated Sucessfully',
                                    'type': 'Success'
                                }
                            );
                            showToast.fire();
                        }
                        else if(state === "ERROR") {
                            var emptyTask = component.get("v.Offer");
                            emptyTask.Subject = "";
                            emptyTask.Description = "";  
                            component.set("v.Offer", emptyTask);
                        }
                    });
                    $A.enqueueAction(action);
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    //jaya
    approveCounterValidation : function(component, event) { 
        component.set("v.ApproveCounterValidation", true);       
    },
    approveCounterValidationCancel : function(component, event) { 
        component.set("v.ApproveCounterValidation", false);       
    },
    submitForCounterApproval: function(component, event, helper) {
        debugger;
        var savNote = component.find("bodyCounterValidation").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteCounterValidation", 'Input field required'); 
            component.find("bodyCounterValidation").set("v.value", '')
        } else {
            component.set("v.saveNoteCounterValidation", ''); 
            var action = component.get('c.SubmitForCounterApprove'); 
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : false,
                "body" : savNote,
                "LogedCUnionId" : component.get("v.LogeduserCUnionId")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.ApproveCounterValidation", false); 
                    component.set("v.Offer", result);
                    component.set("v.offerId", result.offerId);  
                    component.set("v.parentId", result.offerId);  
                    
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction Updated Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    },
    rejectCounterValidation : function(component, event) { 
        component.set("v.RejectCounterValidation", true);       
    },
    rejectCounterValidationCancel : function(component, event) { 
        component.set("v.RejectCounterValidation", false);       
    },
    rejectForCounterApproval: function(component, event, helper) {
        var savNote = component.find("body").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveCounterNote", 'Input field required'); 
            component.find("body").set("v.value", '')
        } else {
            component.set("v.saveCounterNote", ''); 
            var action = component.get('c.RejectCounterValidation');
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : false,
                "body" : savNote,
                "LogedCUnionId" : component.get("v.LogeduserCUnionId")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                component.set("v.RejectCounterValidation", false);       
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.Offer", result);
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            
            $A.enqueueAction(action);
        }        
    }, 
    //jaya
    
    sellerApproval1 : function(component, event) { 
        component.set("v.SellerApproval1", true);       
    },
    sellerApproval1Cancel : function(component, event) { 
        component.set("v.SellerApproval1", false);       
    },
    approve1: function(component, event, helper) {
        debugger;        
        var savNote = component.find("bodyApproval1").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteApproval1", 'Input field required'); 
            component.find("bodyApproval1").set("v.value", '')
        } else {
            component.set("v.saveNoteApproval1", ''); 
            var action = component.get('c.SellerApproval1');            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote  
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                component.set("v.SellerApproval1", false);       
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.Offer", result);
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction Updated Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });            
            $A.enqueueAction(action);
        }        
    },
    
    sellerReject1 : function(component, event) { 
        component.set("v.SellerReject1", true);       
    },
    sellerReject1Cancel : function(component, event) { 
        component.set("v.SellerReject1", false);       
    },
    reject1: function(component, event, helper) {
        debugger;        
        var savNote = component.find("bodyReject1").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteReject1", 'Input field required'); 
            component.find("bodyReject1").set("v.value", '')
        } else {
            component.set("v.saveNoteReject1", ''); 
            var action = component.get('c.SellerReject1');
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote  
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                component.set("v.SellerReject1", false);       
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.Offer", result);
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction Updated Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });            
            $A.enqueueAction(action);
        }        
    },  
    
    sellerApproval2 : function(component, event) { 
        component.set("v.SellerApproval2", true);       
    },
    sellerApproval2Cancel : function(component, event) { 
        component.set("v.SellerApproval2", false);       
    },
    approve2: function(component, event, helper) {
        debugger;        
        var savNote = component.find("bodyApproval2").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteApproval2", 'Input field required'); 
            component.find("bodyApproval2").set("v.value", '')
        } else {
            component.set("v.saveNoteApproval2", ''); 
            var action = component.get('c.SellerApproval2');
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote  
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                component.set("v.SellerApproval2", false);       
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.Offer", result);
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });            
            $A.enqueueAction(action);
        }        
    },
    
    sellerReject2 : function(component, event) { 
        component.set("v.SellerReject2", true);       
    },
    sellerReject2Cancel : function(component, event) { 
        component.set("v.SellerReject2", false);       
    },
    reject2: function(component, event, helper) {
        debugger;        
        var savNote = component.find("bodyReject2").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteReject2", 'Input field required'); 
            component.find("bodyReject2").set("v.value", '')
        } else {
            component.set("v.saveNoteReject2", ''); 
            var action = component.get('c.SellerReject2');
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                component.set("v.SellerReject2", false);       
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.Offer", result);
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });            
            $A.enqueueAction(action);
        }        
    },
    
    sellerApproval3 : function(component, event) { 
        component.set("v.SellerApproval3", true);       
    },
    sellerApproval3Cancel : function(component, event) { 
        component.set("v.SellerApproval3", false);       
    },
    approve3: function(component, event, helper) { 
        debugger;        
        var savNote = component.find("bodyApproval3").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteApproval3", 'Input field required'); 
            component.find("bodyApproval3").set("v.value", '')
        } else {
            component.set("v.saveNoteApproval3", ''); 
            var action = component.get('c.Approve'); 
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.SellerApproval3", false);
                    component.set("v.Offer", result);
                    component.set("v.offerId", result.offerId);  
                    component.set("v.parentId", result.offerId);  
                    
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction Updated Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    }, 
    selectAll: function(component, event, helper) {
        var getAllId = component.find("boxPack");
        for (var i = 0; i < getAllId.length; i++) {
            component.find("boxPack")[i].set("v.value", true);
            
        }
    },
    deselectAll: function(component, event, helper) {
        var getAllId = component.find("boxPack");
        for (var i = 0; i < getAllId.length; i++) {
            component.find("boxPack")[i].set("v.value", false);
            
        }
    },
    sellerReject3 : function(component, event) { 
        component.set("v.SellerReject3", true);       
    },
    sellerReject3Cancel : function(component, event) { 
        component.set("v.SellerReject3", false);       
    },
    reject3: function(component, event, helper) {
        debugger;
        var savNote = component.find("bodyReject3").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteReject3", 'Input field required'); 
            component.find("bodyReject3").set("v.value", '')
        } else {
            component.set("v.saveNoteReject3", ''); 
            var action = component.get('c.Reject'); 
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.SellerReject3", false);
                    component.set("v.Offer", result);
                    component.set("v.offerId", result.offerId);  
                    component.set("v.parentId", result.offerId);  
                    
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction Updated Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    }, 
})