({
	 initialize: function(component, event, helper) {
        debugger;
         document.title='User Registration';
        $A.get("e.siteforce:registerQueryEventMap").setParams({"qsToEvent" : helper.qsToEventMap}).fire();    
        component.set('v.isUsernamePasswordEnabled', helper.getIsUsernamePasswordEnabled(component, event, helper));
        component.set("v.isSelfRegistrationEnabled", helper.getIsSelfRegistrationEnabled(component, event, helper));
        component.set("v.communityForgotPasswordUrl", helper.getCommunityForgotPasswordUrl(component, event, helper));
        component.set("v.communitySelfRegisterUrl", helper.getCommunitySelfRegisterUrl(component, event, helper));
    },
    afterScriptLoaded: function(component, event, helper) {
        debugger;
        try{ 
            var getUrlParameter = function getUrlParameter(sParam) {  
                try{
                    var searchString = document.URL.split('?')[1];
                    var sPageURL = decodeURIComponent(searchString),
                        sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                        sParameterName,
                        i;
                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');                 
                        if (sParameterName[0] === sParam)  {
                            return sParameterName[1] === undefined ? true : sParameterName[1];
                        }
                    }
                } catch(ex){}
            };           
            sforce.connection.serverUrl = 'https://tradeaix99-cmfg.cs95.force.com/demo/services/Soap/u/41.0';
            var result = sforce.connection.login('ssonalkar@stratizant.com.tradeaix99', 'Password@123456');
            sforce.connection.sessionId = result.sessionId;
            
        }catch(ex){
            debugger;
        }
    },
    updateinfo : function(component, event, helper) {
        debugger;
        try{ 
            var errorFlag = 'false';
            component.set("v.validation", false); 
            var newPassword = component.find("newPassword").get("v.value");
            var confirmPassword = component.find("confirmPassword").get("v.value"); 
            var re = new RegExp("^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$");
            
            if(newPassword == undefined || newPassword == ""){
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.newpassworderror",'Input field required');
            }else{
                component.set("v.newpassworderror",'');
            }
            if(confirmPassword == undefined || confirmPassword == ""){
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.confirmpassworderror",'Input field required');
            }
            else if(newPassword != confirmPassword){  
                errorFlag = 'true';
                component.set("v.confirmpassworderror",'');
                component.set("v.changepasswordvalidation",true);
                component.set("v.validation", false);
                component.set("v.combinationvalidation",false);
            }
                else if (!re.test(confirmPassword)) {
                    errorFlag = 'true';
                    component.set("v.combinationvalidation",true);
                    component.set("v.changepasswordvalidation",false);
                    component.set("v.validation",false);
                }
            
            
            if(errorFlag == 'false') {
                var getUrlParameter = function getUrlParameter(sParam) {  
                    try{
                        var searchString = document.URL.split('?')[1];
                        var sPageURL = decodeURIComponent(searchString),
                            sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                            sParameterName,
                            i;
                        for (i = 0; i < sURLVariables.length; i++) {
                            sParameterName = sURLVariables[i].split('=');                 
                            if (sParameterName[0] === sParam)  {
                                return sParameterName[1] === undefined ? true : sParameterName[1];
                            }
                        }
                    } catch(ex){}
                }; 
                var userid = getUrlParameter('id');
                sforce.connection.serverUrl = 'https://tradeaix99-cmfg.cs95.force.com/demo/services/Soap/u/41.0';
                var result = sforce.connection.login('ssonalkar@stratizant.com.tradeaix99', 'Password@123456');
                sforce.connection.sessionId = result.sessionId;
                var updateUM = new sforce.SObject('User_Management__c');
                updateUM.Id= userid;                
                updateUM.Password__c = confirmPassword;  
                updateUM.Profiles__c =  $A.get("{!$Label.c.BMCP_ProfileID}");
                sforce.connection.update([updateUM], 
                {
                    onSuccess : function(result)
                    {   
                        component.set("v.CustomerConfirmationpop",true);
                    },onFailure : function(error)
                    {
                        debugger;
                        
                    }
                 
                });
            }
            
            
        }catch(ex){}
    },
    cancelRegister: function(component, event, helper) {
        debugger;
        window.location.replace("https://tradeaix99-cmfg.cs95.force.com/demo/secur/logout.jsp?retUrl=https%3A%2F%2Ftradeaix99-cmfg.cs95.force.com%2Fdemo%2Fs%2Fadmin");
    },
    afterregister: function(component, event, helper) {
        debugger;
        window.location.replace("https://tradeaix99-cmfg.cs95.force.com/demo/secur/logout.jsp?retUrl=https%3A%2F%2Ftradeaix99-cmfg.cs95.force.com%2Fdemo%2Fs%2Fadmin");
    },
})