({
    numberWithCommas: function(x) {  
        x =String(x).toString();
        var afterPoint = '';
        if(x.indexOf('.') > 0)
            afterPoint = x.substring(x.indexOf('.'),x.length);
        x = Math.floor(x);
        x=x.toString();
        var lastThree = x.substring(x.length-3);
        var otherNumbers = x.substring(0,x.length-3);
        if(otherNumbers != '')
            lastThree = ',' + lastThree; 
        return otherNumbers.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + lastThree + afterPoint;
    },
    numberWithoutCommas : function(data){
        var result = '';
        for (var i = 0; i < data.toString().split(',').length; i++) { 
            result += data.toString().split(',')[i];
        } 
        return result;
    }, 
    checkSpecialCharecter : function(data){ 
        var iChars = "!`@#$%^&*()+=-[]\\\';/{}|\":<>?~_";   
        for (var i = 0; i < data.length; i++)
        {      
            if (iChars.indexOf(data.charAt(i)) != -1)
            {    
                return true;
            } 
        }
        return false;
    },
    checkSpecialCharecter1 : function(data){ 
        var iChars = "!`@#$%^&*()+=-[]\\\';.,/{}|\":<>?~_";   
        for (var i = 0; i < data.length; i++)
        {      
            if (iChars.indexOf(data.charAt(i)) != -1)
            {    
                return true;
            } 
        }
        return false;
    },
    checkCharecter : function(data){ 
        for (var i = 0; i < data.length; i++)
        {   
            if (data.match(/[^0-9\.,]/g, ''))
            {    
                return true;
            } 
        }
        return false;
    }, 
    checkDate : function(data){ 
        for (var i = 0; i < data.length; i++)
        {   
            if (data.match(/[^0-9\-]/g, ''))
            {    
                return true;
            } 
        }
        return false;
    }, 
    
    round : function(num, place) { 
        var div = '1';
        for (var i = 0; i < place; i++) { 
            div = div + '0' ;
        }
        var divideby = Number(div);
        return (Math.round(num +'e+' + place)/divideby).toFixed(place); 
        //return +(Math.round(number + "e+" + place)  + "e-" + place);
    },
})