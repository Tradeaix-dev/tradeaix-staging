({    
    getAttribute: function(component) { 
        debugger;
            var sitename = localStorage.getItem("TSiteName").charAt(0).toUpperCase() + localStorage.getItem("TSiteName").slice(1)
            document.title =sitename +' : Attribute Detail';
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            }catch(ex){}
        };
         var action = component.get('c.getAttributeDetails');    
        var attrId = getUrlParameter('attributeId');
        action.setParams({
            "attributeId" : attrId
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
				//component.find("txtPrimary").set("v.disabled", true);                
                component.set("v.Result", result);
                component.set("v.AttHistory",result.Alogs);
                component.set("v.attributeId", result.result.Id);
                component.set("v.oldAttributeName", result.result.Name);    
                component.set("v.resultattributeType", result.result.Attribute_Type__c);
                if(result.result.Mandatory__c == true){
                    component.find("dropMandatoryType").set("v.value",'Yes');
                }else{
                    component.find("dropMandatoryType").set("v.value",'No');
                }
                
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Result"); 
                component.set("v.Result", emptyTask);
            }
          
        }); 
        $A.enqueueAction(action);
       
    },
    editAttribute: function(component, event, helper) {   
        component.set("v.showEdit", true);     
    },
    cancel: function(component, event, helper) {
        component.set("v.showEdit", false);
        component.set("v.validation", false);
        component.set("v.AttributeNameError", '');
        component.set("v.AttributeSizeError", '');
    }, 
    editAttributeDetail: function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        var Id = component.get("v.attributeId");
        //var txtAttributeName = component.find("txtAttributeName").get("v.value");
        //var dropAttributeType = component.find("dropAttributeType").get("v.value");
        var dropProductType = '';
        //var dropProductType = component.find("dropProductType").get("v.value");
        if(component.get("v.resultattributeType") == 'Text'){
            var txtAttributeSize = '255';
        }else{
            var txtAttributeSize = '';
        }
        
        var txtActive = component.find("txtActive").get("v.value");
        //var txtPrimary = component.find("txtPrimary").get("v.value");
        //var txtSecondary = component.find("txtSecondary").get("v.value"); 
        //var txtrbtnmanyes = component.find("rbtnmanyes").get("v.value");
        //var txtrbtnmanno = component.find("rbtnmanno").get("v.value");
        var mandatory = component.find("dropMandatoryType").get("v.value");
        //if(errorFlag == 'false') {
        component.find("btnEditAttribute").set("v.disabled", true);                    
        var action1 = component.get("c.EditAttribute");
        action1.setParams({
            "Id" : Id,
            //"Name" : txtAttributeName,
            //"AttributeType" : dropAttributeType,
            //"ProductType" : dropProductType,
            "AttributeSize" : txtAttributeSize,
            "IsActive" : txtActive,
            //"IsPrimary" : txtPrimary,
           // "IsSecondary" : false,
            "Ismandatory" :mandatory
        }); 
        action1.setCallback(this, function(response1) {
            var state1 = response1.getState();
            var attributeId = response1.getReturnValue(); 
            debugger;
            component.find("btnEditAttribute").set("v.disabled", false);
            if(attributeId != '' && attributeId != undefined) {
                var site = $A.get("{!$Label.c.Org_URL}");
                window.location = '/'+site+'/s/template-attribute-detail?attributeId='+ attributeId;
            }
            else{ 
                component.set("v.validation", false); 
                component.set("v.showEdit", false);
                //component.set("v.AttributeSizeError", '');
            }
            
        });
        $A.enqueueAction(action1);
        //}
        
        /*if(txtAttributeName == undefined || txtAttributeName == "" || txtAttributeName == null)
        {
            errorFlag = 'true';
            component.set("v.validation", true); 
            component.set("v.AttributeNameError", 'Input field required'); 
        }
        
        if(txtAttributeSize != undefined && txtAttributeSize != "" && txtAttributeSize != null)
        {
            if(helper.checkCharecter(txtAttributeSize))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.AttributeSizeError", 'Please enter a numeric value.'); 
            } 
            else
                component.set("v.AttributeSizeError", '');
        } 
        else if(dropAttributeType != "DateTime")
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.AttributeSizeError", 'Input field required');
        }
        else
            txtAttributeSize = '';
        
        if(txtAttributeName != undefined && txtAttributeName != "" && txtAttributeName != null)
        {
            var action = component.get("c.CheckTemplateAttributes");
            action.setParams({ 
                "attributeName": txtAttributeName,
                "oldAttributeName": component.get("v.oldAttributeName")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                var attribute = response.getReturnValue(); 
                if(attribute != '' && attribute != undefined) {
                    if(attribute == 'Already Used') { 
                        errorFlag = 'true';
                        component.set("v.validation", true); 
                        component.set("v.AttributeNameError", 'This Attribute Name has already been used. Please enter a new Attribute Name.');
                    } else {
                        component.set("v.AttributeNameError", ''); 
                    }
                } else { 
                    component.set("v.AttributeNameError", ''); 
                }
                if(errorFlag == 'false') {
                    component.find("btnEditAttribute").set("v.disabled", true);                    
                    var action1 = component.get("c.EditAttribute");
                    action1.setParams({
                        "Id" : Id,
                        "Name" : txtAttributeName,
                        "AttributeType" : dropAttributeType,
                        "ProductType" : dropProductType,
                        "AttributeSize" : txtAttributeSize,
                        "IsActive" : txtActive,
                        "IsPrimary" : txtPrimary,
                        "IsSecondary" : txtSecondary
                    }); 
                    action1.setCallback(this, function(response1) {
                        var state1 = response1.getState();
                        var attributeId = response1.getReturnValue(); 
                        debugger;
                        component.find("btnEditAttribute").set("v.disabled", false);
                        if(attributeId != '' && attributeId != undefined) {
                            window.location = '/demo/s/template-attribute-detail?attributeId='+ attributeId;
                        }
                        else{ 
                            component.set("v.validation", false); 
                            component.set("v.showEdit", false);
                            component.set("v.AttributeSizeError", '');
                        }
                        
                    });
                    $A.enqueueAction(action1);
                }
            });
            $A.enqueueAction(action);
        }
        */
    },
    changeAttributeType: function(component, event, helper) {        
        var dropAttributeType = component.find("dropAttributeType").get("v.value");
        if(dropAttributeType == "DateTime")
            component.find("txtAttributeSize").set("v.value", ''); 
        component.set("v.AttributeSizeError", '');
    },
    changePrimary: function(component, event, helper) {  
        debugger;
        var txtPrimary = component.find("txtPrimary").get("v.value");
        if(txtPrimary == true)  
            component.find("txtSecondary").set("v.value", false); 
        else
            component.find("txtSecondary").set("v.value", true); 
    },
    changeSecondary: function(component, event, helper) {   
        debugger;
        var txtSecondary = component.find("txtSecondary").get("v.value");
        if(txtSecondary == true)  
            component.find("txtPrimary").set("v.value", false); 
        else
            component.find("txtPrimary").set("v.value", true); 
    },
})