({
    getIssuingBank: function(component, event, helper) {   
        debugger;       
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            }catch(ex){}
        };
        var action = component.get('c.getIssuingBankDetails');    
        var Id = getUrlParameter('issuingbankId');   
        component.set("v.IssuingBankId",Id);
        action.setParams({
            "IssuingbankId" : Id
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){				
                component.set("v.Result", result);
                component.set("v.AttHistory", result.Alogs);
            }           
          
        }); 
        $A.enqueueAction(action);
    },
    ApproveIssuingbank: function(component, event, helper) {   
        debugger; 
        component.set("v.approvepopup",true);
        component.set("v.approvebtn",true);
    },
    RejectIssuingbank: function(component, event, helper) {   
        debugger; 
        component.set("v.approvepopup",true);
        component.set("v.approvebtn",false);
    },
    approvecancel: function(component, event, helper) {   
        debugger; 
        component.set("v.approvepopup",false);
    },
    Approved: function(component, event, helper) {   
        debugger;   
        var errorFlag = 'false';
        var action = component.get('c.approveIssuingbank');  
        var sessionname = localStorage.getItem("UserSession");
        var notes = component.find("bodyValidation").get("v.value");
        if(notes == '' || notes == undefined || notes == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.approverejectnotes", 'Input field required'); 
        } else { 
            component.set("v.approverejectnotes", ''); 
        }
        if(errorFlag == 'false') {    
            action.setParams({
                "IssuingbankId" : component.get("v.IssuingBankId"),
                "LoggedUserID" : sessionname,
                "Comments" : notes
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                if(state === "SUCCESS"){			
                    component.set("v.approvepopup", false);
                    var site = $A.get("{!$Label.c.Org_URL}");
                    window.location = '/'+site+'/s/issuing-banks';      
                }           
                
            }); 
            $A.enqueueAction(action);
        }
        
    },
    Rejected:function(component, event, helper) {   
        debugger;   
        var errorFlag = 'false';
        var action = component.get('c.rejectIssuingbank');  
        var sessionname = localStorage.getItem("UserSession");
        var notes = component.find("bodyValidation").get("v.value");
        if(notes == '' || notes == undefined || notes == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.approverejectnotes", 'Input field required'); 
        } else { 
            component.set("v.approverejectnotes", ''); 
        }
        if(errorFlag == 'false') {    
            action.setParams({
                "IssuingbankId" : component.get("v.IssuingBankId"),
                "LoggedUserID" : sessionname,
                "Comments" : notes
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                if(state === "SUCCESS"){			
                    component.set("v.approvepopup", false);
                    var site = $A.get("{!$Label.c.Org_URL}");
                    window.location = '/'+site+'/s/issuing-banks';      
                }           
                
            }); 
            $A.enqueueAction(action);
        }
        
    }
})