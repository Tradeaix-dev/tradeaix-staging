public without sharing class LightTransactionSummaryCtrl {
    public static String query = '';
    public static Integer size = 0; 
    @AuraEnabled  
    public static Organization__c[] loadorgs(String username) {
        return Helper.loadorgs(username);
    }
    @AuraEnabled 
    public static LightSellerOfferListV1 TransactionDetails(Integer counter, String sortbyField, String sortDirection, String selected,String LoggedUserTenantId,String Filtervalue)
    {   
        return ShowTable(counter, sortbyField, sortDirection, selected,LoggedUserTenantId, Filtervalue);
    }
    
    @AuraEnabled
    public static Transactionviewctrlv1 Chkunregisteruser(string creditUnionId,String GID) {
        system.debug('##creditUnionId##'+creditUnionId);
        Transactionviewctrlv1 Trans =New Transactionviewctrlv1();   
        Trans.CPSelection=[SELECT Tenant_User__c FROM CPSelection__c WHERE Transaction__r.GroupID__c=:GID AND Tenant_User__r.Profiles__c=:System.label.OBCP_ProfileID Limit 1];
        Trans.userslist1 = [SELECT Id, Name, First_Name__c, Last_Name__c, User_Email__c, Title__c, Phone__c,Department__c from User_Management__c WHERE Tenant__c =: creditUnionId];
        List<String> ls = new List<String>();
        SET<String> SETls = new SET<String>();
        List<Published_Tenant__c> lstPT = [SELECT Id, Name from Published_Tenant__c WHERE Transaction__r.GroupID__c =:GID];
        Set<Id> PTId = new Set<Id>();
        for(Published_Tenant__c PT : lstPT){
            PTId.add(PT.Id);
        }
        system.debug('==PTId=='+PTId);
        for(Published_Tenant_User__c PTU: [SELECT Id, Tenant_User__c from Published_Tenant_User__c WHERE Published_Tenant__c =: PTId]){
            
            ls.add(PTU.Tenant_User__c);
        }
        system.debug('==ls=='+ls);
        if(ls.size() >0){
            
                ls.addAll(SETls);
            Trans.idString = ls;
        }
        return Trans;
    }
    @AuraEnabled
    public static Transactionviewctrlv1 OrgcounterPartylist(string creditUnionId,String GID) {
        system.debug('##creditUnionId##'+creditUnionId);
        Transactionviewctrlv1 Trans =New Transactionviewctrlv1();
        set<Id> tenantId = new set<Id>();
        List<Tenant_Mapping__c> TM = [SELECT Id, Tenant__c, Tenant_Approved__c, Tenant_Approved__r.Organization__c from Tenant_Mapping__c WHERE Tenant__c =: creditUnionId AND Selectchk__c = true];
        for(Tenant_Mapping__c TId: TM){
            tenantId.add(TId.Tenant_Approved__r.Organization__c);
        }
        Trans.Org = [select Id,Name,Tenant_Counts__c,Organization_Logo_URL__c, (select Id, name,City__c, Tenant_Site_Name__c,Organization__c,Tenant_User_Count__c from Tenants__r)  from Organization__c WHERE Id=: tenantId];
        Trans.userslist = [SELECT id,CreatedFromrapid__c,Profiles__c,Mobile__c,Title__c,First_Name__c,User_Email__c,Last_Name__c,Street__c,Phone__c,Tenant__r.Tenant_Swift_Code__c,Tenant__r.Tenant_Site_Name__c,City__c,Department__c,Country_Code__c,State_Province__c,Zip_Postal_Code__c,Country__c,User_Title__c FROM User_Management__c WHERE Tenant_Name__c=:creditUnionId AND Profiles__c=:System.label.OBCP_ProfileID];
        system.debug('####'+Trans.userslist);
        Trans.CPSelection=[SELECT Tenant_User__c FROM CPSelection__c WHERE Transaction__r.GroupID__c=:GID AND Tenant_User__r.Profiles__c=:System.label.OBCP_ProfileID Limit 1];
        
        return Trans;
    }
    
    @AuraEnabled
    public static Transactionviewctrlv1 publishRFQ(String LoggedUserId,String LoggedUserTenantId,Boolean IsManager,String offerId, Boolean requestForInfo){
        Transactionviewctrlv1 offer = new Transactionviewctrlv1();
        List<Transaction__c> Trans=New List<Transaction__c>();
        for(Transaction__c offr:[Select Id, Name,CreatedBy__c,TransactionRefNumber__c,status__c from Transaction__c where GroupID__c = :LoggedUserTenantId])
        {
            Transaction__c T=New Transaction__c();
            T.Id = offr.Id;
            T.status__c ='Requested For Quote';
            T.Published__c = true;
            T.Published_By__c = offr.CreatedBy__c;
            T.Published_Date__c = System.Now();
            T.RFQChk__c=true;
            Trans.add(T);
            
        }
        system.debug('##'+Trans.Size());
        If(Trans.Size()>0)
        {
            update Trans;
            for(Transaction__c offr:Trans)
            {
                
                String CreatedBy =[SELECT CreatedBy__c FROM Transaction__c WHERE Id=:offr.Id Limit 1].CreatedBy__c;
                Helper.ActivityLogInsertCallForRecord(CreatedBy,offr.Id,'Requested For Quote','',false);
            }
        }
        return offer;
    }
    @AuraEnabled
    public static Transactionviewctrlv1 viewTenantUsersList(String tenantId){
        Transactionviewctrlv1 offer = new Transactionviewctrlv1(); 
        offer.userslist = [SELECT Id, Name, First_Name__c, Last_Name__c, User_Email__c, Title__c, Phone__c from User_Management__c WHERE Tenant__c =: tenantId];
        return offer;
    }
    @AuraEnabled
    public static Transactionviewctrlv1 viewRequestUsers(String GId){
        Transactionviewctrlv1 offer = new Transactionviewctrlv1(); 
        system.debug('##GId##'+GId);
        Set<Id> LPT =New Set<Id>();
        Transaction__c[] T =[SELECT id FROM Transaction__c WHERE GroupID__c=:GId Limit 1];
        for(Published_Tenant__c PT:[SELECT id FROM Published_Tenant__c WHERE Transaction__c=:T[0].Id])
        {
            
            LPT.add(PT.id);
        }
        offer.CounterParties = [SELECT id,Tenant__r.Tenant_Logo_Url__c,Tenant__r.Tenant_Site_Name__c,(SELECT Id, Name,Tenant_User__r.Id,Tenant__r.City__c,Tenant_User__r.First_Name__c, Tenant_User__r.Last_Name__c,Tenant_User__r.User_Email__c,Tenant_User__r.Phone__c,Tenant_User__r.Title__c,Tenant_User__r.Department__c from Published_Tenant_Users__r) FROM Published_Tenant__c WHERE ID =: LPT];
        offer.CPSelection=[SELECT Tenant_User__r.id,Tenant_User__r.CreatedFromrapid__c,Tenant_User__r.Profiles__c,Tenant_User__r.Mobile__c,Tenant_User__r.Title__c,Tenant_User__r.First_Name__c,Tenant_User__r.User_Email__c,Tenant_User__r.Last_Name__c,Tenant_User__r.Street__c,Tenant_User__r.Phone__c,Tenant_User__r.Tenant__r.Tenant_Swift_Code__c,Tenant_User__r.Tenant__r.Tenant_Site_Name__c,Tenant_User__r.City__c,Tenant_User__r.Department__c,Tenant_User__r.Country_Code__c,Tenant_User__r.State_Province__c,Tenant_User__r.Zip_Postal_Code__c,Tenant_User__r.Country__c,Tenant_User__r.User_Title__c FROM CPSelection__c WHERE Transaction__c=:T[0].Id AND Tenant_User__r.Profiles__c=:System.label.OBCP_ProfileID];
        return offer;
    }
    private static LightSellerOfferListV1 ShowTable(Integer counter, String sortbyField, String sortDirection, String selected,String LoggedUserTenantId,String Filtervalue)
    {
        
        LightSellerOfferListV1 retValue = new LightSellerOfferListV1();
        string Ids='';
        size = [SELECT count() from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'SellerOfferLists' AND Grid_Name__c =: 'SellerOfferLists']; 
        
        if(size > 0){
            retValue.list_size = Integer.valueOf([SELECT List_Size__c from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'SellerOfferLists' AND Grid_Name__c =: 'SellerOfferLists'][0].List_Size__c);
        }
        else {
            retValue.list_size = 25;
        }
<<<<<<< HEAD
        System.debug('####selected###'+selected);
        String newStr = '' ;
        String getCommaSepratedString =  LoggedUserTenantId.removeEnd(',');
        system.debug('===getCommaSepratedString=='+getCommaSepratedString);
        String[] myList = new List<String>();
        myList = getCommaSepratedString.split(',');
        for(String str : myList)
        {
            system.debug('====str==='+str);
            newStr += '\'' + str.trim() + '\',';
            //newStr = newStr.lastIndexOf(',') > 0 ? newStr.substring(0,newStr.lastIndexOf(',')) + ',' : newStr ;
            newStr = newStr.lastIndexOf(',') > 0 ? newStr.substring(0,newStr.lastIndexOf(',')) + ',' : newStr ;
        }
        String alpha = LoggedUserTenantId;
        List<String> lstAlpha = alpha.split(',');
        string empty = '\'\'';
        System.debug('+++++'+newStr+empty);
        //query = 'SELECT Id,GroupID__c,Notes__c,Issuing_Banks__r.Tenant_Site_Name__c,Issuing_Banks__r.City__c,Tenant__r.Organization__r.Name,Tenant__r.Tenant_Site_Name__c,Product_Template__r.TemplateName__c,Status__c,Loan_Type__c,CreatedDate,TransactionRefNumber__c,Currency__c,(SELECT id,Product_Template__c,Attribute_Name__c,Attribute_Value__c,Display_on_gride__c,Sort_Order__c FROM Transaction_Attributes__r ORDER BY Attribute_Name__c asc) FROM Transaction__c WHERE Id IN('+newStr+''+empty+')';
=======
        
>>>>>>> branch 'master' of https://Tradeaix-dev@bitbucket.org/Tradeaix-dev/tradeaix-staging.git
        query = 'SELECT Id,GroupID__c,Notes__c,Issuing_Banks__r.Tenant_Site_Name__c,Issuing_Banks__r.City__c,Tenant__r.Organization__r.Name,Tenant__r.Tenant_Site_Name__c,Product_Template__r.TemplateName__c,Status__c,Loan_Type__c,CreatedDate,TransactionRefNumber__c,Currency__c,(SELECT id,Product_Template__c,Attribute_Name__c,Attribute_Value__c,Display_on_gride__c,Sort_Order__c FROM Transaction_Attributes__r ORDER BY Attribute_Name__c asc) FROM Transaction__c WHERE GroupID__c=\''+LoggedUserTenantId+'\'';
        query=query+' order by CreatedDate desc '+ 'LIMIT '+retValue.list_size+' offset '+counter;
        system.debug('===String Query==='+query);
        retValue.results = Database.query(query);
        retValue.total_size = retValue.results.Size();
        retValue.counter = counter; 
        retValue.sortbyField = sortbyField; 
        retValue.sortDirection = sortDirection;
        retValue.total_page = getTotalPages(retValue.list_size, retValue.total_size);
        retValue.selectedItem = 'Active';
        
        return retValue;   
    }
    private static Integer getTotalPages(Integer list_size, Integer total_size) { 
        if (math.mod(total_size, list_size) > 0) {
            return total_size/list_size + 1;
        }
        else {
            return (total_size/list_size);
        }
    }  
    @AuraEnabled
    public static LightSellerOfferListV1 Filter(Integer counter, String sortbyField, String sortDirection, String selected,String LoggedUserTenantId,String Filtervalue) { 
        
        return ShowTable( counter, sortbyField, sortDirection,  selected, LoggedUserTenantId, Filtervalue);
    }
    @AuraEnabled
    public static LightSellerOfferListV1 saveFilter(Integer counter, String sortbyField, String sortDirection,string filter,String LoggeduserTenantID,String LoggedUserId,String Filtervalue) 
    { 
        LightSellerOfferListV1 retValue = new LightSellerOfferListV1();
        try { 
            
            retValue = ShowTable(counter, sortbyField, sortDirection, filter, LoggeduserTenantID,Filtervalue); 
        } catch(Exception ex){
            System.debug('LightSellerOfferList - saveFilter() : ' + ex.getMessage());
            //Helper.ActivityLogInsertCall('','Error - LightSellerOfferList - saveFilter()',ex.getMessage()); 
        }  
        return retValue; 
    }
    @AuraEnabled
    public static LightSellerOfferListV1 alyFilter(Integer counter, String sortbyField, String sortDirection, String selected, UI_Filter__c filter,String LoggeduserTenantID,String LoggedUserId,String Filtervalue) 
    { 
        LightSellerOfferListV1 retValue = new LightSellerOfferListV1(); 
        try { 
            List<UI_Filter__c> filter1 = [SELECT Id,Name FROM UI_Filter__c WHERE Name =:'SellerOfferLists' AND CreatedById =: UserInfo.getUserId()]; 
            if(filter1.size() > 0){
                filter.Id = filter1[0].Id;
                system.debug('@F@'+filter);
                filter1[0] = filter;
                update filter1; 
            }
            else {
                filter.Name = 'SellerOfferLists';
                upsert filter; 
            }   
            Helper.setSessionValue('Filter', 'SellerOfferLists');
            retValue = ShowTable(counter, sortbyField, sortDirection, selected,LoggeduserTenantID,Filtervalue);
        } catch(Exception ex){
            System.debug('LightSellerOfferList - saveFilter() : ' + ex.getMessage());
            //Helper.ActivityLogInsertCall('','Error - LightSellerOfferList - saveFilter()',ex.getMessage()); 
        }  
        return retValue; 
    }
    @AuraEnabled
    public static UI_Filter__c getFilter() 
    { 
        UI_Filter__c filter = new UI_Filter__c(); 
        try { 
            List<UI_Filter__c> filter1 = [SELECT Id,CreateddateTo__c,CreateddateFrom__c,IsPrimary__c,Loan_Type__c,Transaction_Status__c,TemplateId_c__C, Name FROM UI_Filter__c WHERE Name =: 'QuoteList' AND CreatedById =: UserInfo.getUserId()]; 
            if(filter1.size() > 0){
                filter = filter1[0]; 
                return filter;
            } 
        } catch(Exception ex){
            System.debug('LightSellerOfferList - getFilter() : ' + ex.getMessage());
            //Helper.ActivityLogInsertCall('','Error - LightSellerOfferList - getFilter()',ex.getMessage()); 
        }
        return filter; 
    }
    @AuraEnabled
    public static LightSellerOfferListV1 getBeginning(Integer counter, String sortbyField, String sortDirection, String selected, String LoggedUserTenantId,String Filtervalue) { 
        return ShowTable( counter, sortbyField, sortDirection,NULL, LoggedUserTenantId, Filtervalue);
    }
    
    @AuraEnabled
    public static LightSellerOfferListV1 Views(Integer counter, String sortbyField, String sortDirection, String selected, String LoggedUserTenantId,String Filtervalue) { 
        Helper.setSessionValue('sessionselectedPT', selected);
        return ShowTable( counter, sortbyField, sortDirection,  selected, LoggedUserTenantId, Filtervalue);
    }
    
    @AuraEnabled
    public static LightSellerOfferListV1 getPrevious(Integer counter, String sortbyField, String sortDirection, String selected, String LoggedUserTenantId,String Filtervalue) {
        return ShowTable( counter, sortbyField, sortDirection,  selected, LoggedUserTenantId, Filtervalue);
    }
    
    @AuraEnabled
    public static LightSellerOfferListV1 getNext(Integer counter, String sortbyField, String sortDirection, String selected, String LoggedUserTenantId,String Filtervalue) {  
        return ShowTable( counter, sortbyField, sortDirection,  selected, LoggedUserTenantId, Filtervalue);
    }
    
    @AuraEnabled
    public static LightSellerOfferListV1 getEnd(Integer counter, String sortbyField, String sortDirection, String selected, String LoggedUserTenantId,String Filtervalue) { 
        return ShowTable( counter, sortbyField, sortDirection,  selected, LoggedUserTenantId, Filtervalue);
    } 
    @AuraEnabled
    public static String addremoveunregisteredCP(String UserID, string GId,Boolean Flag){
        string str='';
        try{
            
            if(Flag){
                List<CPSelection__c> InsListCP =New List<CPSelection__c>();
                List<Limited_Bidding__c> LBList =New List<Limited_Bidding__c>();
                for(Transaction__c Trans:[SELECT id,CreatedBy__c,Name FROM Transaction__c WHERE GroupID__c=:Gid])
                {
                    String Email =[SELECT User_Email__c FROM User_Management__c WHERE id=:UserID].User_Email__c;
                    CPSelection__c CP = New CPSelection__c(); 
                    CP.IsSave__c = true;
                    CP.Tenant_User__c = UserID;
                    CP.Transaction__c = Trans.id;
                    InsListCP.add(CP);
                    
                    Helper.ActivityLogInsertCallForRecord(Trans.CreatedBy__c,Trans.id,'CP - '+Email+'- Selected','',false);
                    
                    Limited_Bidding__c LB =New Limited_Bidding__c();
                    LB.Tenant_User__c =UserID; 
                    LB.agree__c=true;
                    LB.Transaction__c =Trans.id;
                    LB.CreatedBy__c = Trans.CreatedBy__c;
                    LB.Don_t_Sent_mail__c=true;
                    LB.LastModifiedBy__c = Trans.CreatedBy__c;
                    LBList.add(LB);
                    
                    
                }
                if(InsListCP.Size()>0)
                {
                    Insert InsListCP;
                }
                if(LBList.Size()>0){
                    Insert LBList;
                }
            }else{
                List<CPSelection__c> DelListCP = [SELECT id,Tenant_User__c,Transaction__c,Transaction__r.CreatedBy__c FROM CPSelection__c WHERE Tenant_User__c=:UserID AND Transaction__r.GroupID__c=:Gid];
                if(DelListCP.Size()>0)
                {
                     for(CPSelection__c Trans:DelListCP){
                        String Email =[SELECT User_Email__c FROM User_Management__c WHERE id=:UserID].User_Email__c;
                        Helper.ActivityLogInsertCallForRecord(Trans.Transaction__r.CreatedBy__c,Trans.Transaction__c,'CP - '+Email+'- UnSelected','',false);
                    }
                    Delete DelListCP;
                }
                List<Limited_Bidding__c> ListDEL = [SELECT id FROM Limited_Bidding__c WHERE Tenant_User__c=:UserID AND Transaction__r.GroupID__c=:Gid];
                if(ListDEL.Size()>0)
                {
                    Delete ListDEL;
                }
            }   
            
        } catch(Exception ex){
        }
        return str;
    }
    @AuraEnabled
    public static LightSellerOfferListV1 changelist_size(Integer counter, String sortbyField, String sortDirection, Integer list_size, String selected, String LoggedUserTenantId,String Filtervalue) { 
        LightSellerOfferListV1 retValue = new LightSellerOfferListV1(); 
        try{
            size = [SELECT count() from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'TemplateLists' AND Grid_Name__c =: 'SellerOfferLists'];
            if(size > 0){  
                UI_List_Size__c listSize = new UI_List_Size__c(
                    Id = [SELECT Id from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'SellerOfferLists' AND Grid_Name__c =: 'SellerOfferLists'][0].Id,
                    List_Size__c = list_size  
                );
                update listSize;
            }
            else {
                UI_List_Size__c listSize = new UI_List_Size__c(
                    List_Size__c = list_size,
                    Name = 'SellerOfferLists',
                    Grid_Name__c = 'SellerOfferLists',
                    User__c = UserInfo.getUserId()
                );
                insert listSize;
            }  
            
            return ShowTable( counter, sortbyField, sortDirection,  selected, LoggedUserTenantId, Filtervalue);
            
        } catch(Exception ex){
            //Logger.LogException(
            //   'Error - LightProductTemplate - changelist_size()', 
            //   ex.getMessage()
            //);
        }
        return retValue;
    }
    
    @AuraEnabled
    public static LightSellerOfferListV1 SortTable(Integer counter, String sortbyField, String sortDirection, String selected, String LoggedUserTenantId,String Filtervalue) { 
        LightSellerOfferListV1 retValue = new LightSellerOfferListV1(); 
        try{
            if(sortbyField == '') {
                sortbyField = 'LastModifiedDate';
                sortDirection = 'DESC';
            } else {
                if(sortDirection.equals('ASC')){
                    sortDirection = 'DESC';
                } else {
                    sortDirection = 'ASC';
                }
            }
            Helper.setSessionValue('sortbyFieldSOL', sortbyField);
            Helper.setSessionValue('sortDirectionSOL', sortDirection); 
            return ShowTable( counter, sortbyField, sortDirection,  selected, LoggedUserTenantId, Filtervalue);
        } catch (Exception ex){ 
            // Logger.LogException(
            //     'Error - LightProductTemplate - SortTable()', 
            //     ex.getMessage()
            // );
        }
        return retValue;
    } 
    
    @AuraEnabled
    public static LightSellerOfferListV1 changePage(Integer counter, String sortbyField, String sortDirection, String selected, String LoggedUserTenantId,String Filtervalue) {
        return ShowTable( counter, sortbyField, sortDirection,  selected, LoggedUserTenantId, Filtervalue);
    }
}