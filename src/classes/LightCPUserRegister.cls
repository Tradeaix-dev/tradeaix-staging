public class LightCPUserRegister {
     public  LightCPUserRegister() {
        
    }
	@AuraEnabled
    public static Userloginv1 getTenant(String TenantId) {
        Userloginv1 ULogin =NEW Userloginv1();
        try{
            
            String RetVal ='';
            ApexPages.PageReference lgn;
            lgn = Site.login('tradeaixcom_user@gmail.com.tradeaix99', 'trade@123', null);
            system.debug('=====login user===='+userinfo.getName()); 
            ULogin.userlists = [SELECT Id,Tenant_Site_Name__c,Tenant_Short_Name__c,Tenant_Logo_Url__c,Tenant_Banner_Url__c,Tenant_vision__c,Name from Tenant__c WHERE Tenant_Short_Name__c =:TenantId LIMIT 1];
            system.debug('####'+ULogin);
        }
        catch (Exception ex) {
            //return ex.getMessage();            
        }
        return ULogin;
    }
    
    
    @AuraEnabled
    public static String login(String Username,String ID,String LBIds, string browsername, String ipAddress,String LoginURL,String RetURL, String TId) {
        try{
            
            String RetVal ='';
            ApexPages.PageReference lgn;
                 lgn = Site.login('tradeaixcom_user@gmail.com.tradeaix99', 'trade@123', '/demo/s/transactionviewdetails?transactionId='+TId);
            system.debug('===userinfo=='+userinfo.getUserId());
            RetVal = Helper.getLoginUserDetailsForOTP(ID,browsername,ipAddress,LoginURL);
            if(RetVal =='Failed'){
               return RetVal;
            }else{                    
                aura.redirect(lgn);
           }
            
            Helper.ActivityLogInsertCall(RetVal,'Login',Username+' is successfully logged.',false);
            return RetVal;
            
        }
        catch (Exception ex) {
            Helper.ActivityLogInsertCall('','Login Error',ex.getMessage(),true);
            return ex.getMessage();            
        }
    }
    @AuraEnabled
    public static Boolean getIsUsernamePasswordEnabled() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getUsernamePasswordEnabled();
    }
    
    @AuraEnabled
    public static Boolean getIsSelfRegistrationEnabled() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getSelfRegistrationEnabled();
    }
    
    @AuraEnabled
    public static String getSelfRegistrationUrl() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        if (authConfig.getSelfRegistrationEnabled()) {
            return authConfig.getSelfRegistrationUrl();
        }
        return null;
    }
    
    @AuraEnabled
    public static String getForgotPasswordUrl() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getForgotPasswordUrl();
    }
    
    @TestVisible
    private static Auth.AuthConfiguration getAuthConfig(){
        Id networkId = Network.getNetworkId();
        Auth.AuthConfiguration authConfig = new Auth.AuthConfiguration(networkId,'');
        return authConfig;
    }
}