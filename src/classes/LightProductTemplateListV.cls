public class LightProductTemplateListV { 
    @AuraEnabled
    public List <ProductTemplate__c> results = new List <ProductTemplate__c>();
    @AuraEnabled
    public Integer counter = 0; 
    @AuraEnabled
    public Integer Tcount = 0; 
    @AuraEnabled
    public Integer ACount = 0; 
    @AuraEnabled
    public String selectedItem = ''; 
    @AuraEnabled
    public String showpage = '';
    @AuraEnabled
    public String sortbyField = '';
    @AuraEnabled
    public String sortDirection = '';
    @AuraEnabled
    public Integer total_page = 0; 
    @AuraEnabled
    public Integer list_size = 0;
    @AuraEnabled
    public Integer total_size = 0;
     
}