public without sharing class LightSellerOfferList1 {
    public static String query = '';
    public static Integer size = 0; 
    @AuraEnabled 
    public static Organization__c[] loadorgs(String username) {
        return Helper.loadorgs(username);
    }
    @AuraEnabled 
    public static LightSellerOfferListV1 TransactionDetails(Integer counter, String sortbyField, String sortDirection, String selected,String LoggedUserTenantId,String Filtervalue)
    {   
        return ShowTable(counter, sortbyField, sortDirection, selected,LoggedUserTenantId, Filtervalue);
    }
    private static LightSellerOfferListV1 ShowTable(Integer counter, String sortbyField, String sortDirection, String selected,String LoggedUserTenantId,String Filtervalue)
    {
        
        LightSellerOfferListV1 retValue = new LightSellerOfferListV1();
        string Ids='';
        size = [SELECT count() from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'SellerOfferLists' AND Grid_Name__c =: 'SellerOfferLists']; 
        
        if(size > 0){
            retValue.list_size = Integer.valueOf([SELECT List_Size__c from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'SellerOfferLists' AND Grid_Name__c =: 'SellerOfferLists'][0].List_Size__c);
        }
        else {
            retValue.list_size = 25;
        }
        SET<id> setTids =New SET<id>();
        if(Filtervalue!='All Realms' && Filtervalue!=null)
        {  
            if(Filtervalue.startsWith('a0M')){
                
                for(Tenant__c T:[SELECT id FROM Tenant__c WHERE Organization__c=:Filtervalue])
                {
                    setTids.add(T.id);    
                    Ids = '\''+T.id+'\',';                    
                }
                
            }else{
                setTids.add(Filtervalue);  
                Ids = '\''+Filtervalue+'\',';
            }
        }
        if(setTids.size() > 0){
            query = 'SELECT Id,Notes__c,Tenant__r.Organization__r.Name,Tenant__r.Tenant_Site_Name__c,Product_Template__r.TemplateName__c,Status__c,Loan_Type__c,CreatedDate,TransactionRefNumber__c,Currency__c,(SELECT id,Product_Template__c,Attribute_Name__c,Attribute_Value__c,Display_on_gride__c,Sort_Order__c FROM Transaction_Attributes__r) FROM Transaction__c WHERE Tenant__c IN('+Ids.removeEnd(',')+')';
        }else{
            if(LoggedUserTenantId !=system.label.TradeAix_Tenant_Id){
                system.debug('########'+LoggedUserTenantId);
                List<Tenant__c> ListT =[SELECT id,CreatedFromOTP__c,Tenant_Bank_Type__c FROM Tenant__c WHERE id=:LoggedUserTenantId LIMIT 1];
                if(ListT[0].Tenant_Bank_Type__c =='Counterparty Bank')
                { 
                    
                    
                    system.debug('########'+ListT[0].Tenant_Bank_Type__c);
                    SET<id> setPTids =New SET<id>();
                    string PTids='';
                    for(Published_Tenant__c PT:[SELECT Tenant__c,Transaction__c FROM Published_Tenant__c WHERE Tenant__c=:LoggedUserTenantId])
                    {
                        if(PT.Transaction__c!=null){
                            setPTids.add(PT.Transaction__c); 
                            PTids =PTids+ '\''+PT.Transaction__c+'\',';   
                        }
                        
                    }
                    SET<id> setTBids =New SET<id>();
                    for(User_Management__c UM:[SELECT id,Profiles__c FROM User_Management__c WHERE Tenant__c=:LoggedUserTenantId])
                    {
                        if(UM.Profiles__c==system.label.OBCP_ProfileID || ListT[0].CreatedFromOTP__c==true)
                        {
                            setTBids.add(UM.id);
                        }
                    }
                    system.debug('########'+setTBids);
                    if(setTBids.size()>0)
                    {
                        for(Limited_Bidding__c LBList:[SELECT Transaction__c FROM Limited_Bidding__c WHERE Tenant_User__c IN :setTBids])
                        {
                            if(LBList.Transaction__c!=null){
                                setPTids.add(LBList.Transaction__c); 
                                PTids =PTids+ '\''+LBList.Transaction__c+'\',';
                            }
                        }
                    }
                    system.debug('########'+PTids);
                    if(setPTids.size() > 0){
                         system.debug('####84####'+setPTids);
                        query = 'SELECT Id,Notes__c,Issuing_Banks__r.Tenant_Site_Name__c,Issuing_Banks__r.City__c,Tenant__r.Organization__r.Name,Tenant__r.Tenant_Site_Name__c,Product_Template__r.TemplateName__c,Status__c,Loan_Type__c,CreatedDate,TransactionRefNumber__c,Currency__c,(SELECT id,Product_Template__c,Attribute_Name__c,Attribute_Value__c,Display_on_gride__c,Sort_Order__c FROM Transaction_Attributes__r) FROM Transaction__c WHERE Id IN('+PTids.removeEnd(',')+')  AND Status__c!=\'Transaction Approved\'';
                   system.debug('===String Query==='+query);
                    }else{
                        system.debug('####87####');
                        query = 'SELECT Id,Issuing_Banks__r.Tenant_Site_Name__c,Issuing_Banks__r.City__c,Notes__c,Tenant__r.Organization__r.Name,Tenant__r.Tenant_Site_Name__c,Product_Template__r.TemplateName__c,Status__c,Loan_Type__c,CreatedDate,TransactionRefNumber__c,Currency__c,(SELECT id,Product_Template__c,Attribute_Name__c,Attribute_Value__c,Display_on_gride__c,Sort_Order__c FROM Transaction_Attributes__r) FROM Transaction__c WHERE Tenant__c=\''+LoggedUserTenantId+'\'  AND Status__c!=\'Transaction Approved\'';
                        
                    }
                }else{
                    system.debug('####92####');
                    query = 'SELECT Id,Issuing_Banks__r.Tenant_Site_Name__c,Issuing_Banks__r.City__c,Notes__c,Tenant__r.Organization__r.Name,Tenant__r.Tenant_Site_Name__c,Product_Template__r.TemplateName__c,Status__c,Loan_Type__c,CreatedDate,TransactionRefNumber__c,Currency__c,(SELECT id,Product_Template__c,Attribute_Name__c,Attribute_Value__c,Display_on_gride__c,Sort_Order__c FROM Transaction_Attributes__r) FROM Transaction__c WHERE Tenant__c=\''+LoggedUserTenantId+'\'';
                }
            }
            else{
                system.debug('####97####');
                query = 'SELECT Id,Issuing_Banks__r.Tenant_Site_Name__c,Issuing_Banks__r.City__c,Notes__c,Tenant__r.Organization__r.Name,Tenant__r.Tenant_Site_Name__c,Product_Template__r.TemplateName__c,Status__c,Loan_Type__c,CreatedDate,TransactionRefNumber__c,Currency__c,(SELECT id,Product_Template__c,Attribute_Name__c,Attribute_Value__c,Display_on_gride__c,Sort_Order__c FROM Transaction_Attributes__r) FROM Transaction__c';
            }
            
        }
        system.debug('===String Query==='+query);
        List<UI_Filter__c> filter = [SELECT Id,Markettype__c,IsDeleted,IsPrimary__c,LastModifiedById,LastModifiedDate,Loan_Type__c,Name,OwnerId,SystemModstamp,CreateddateTo__c,CreateddateFrom__c,Transaction_Status__c,TemplateId_c__c,TransactionRefNumber__c FROM UI_Filter__c WHERE Name =: 'SellerOfferLists' AND CreatedById =: UserInfo.getUserId()]; 
        if(filter .size() > 0 && (String)Cache.Session.get('Filter') != null && (String)Cache.Session.get('Filter') != '')
        {
            retValue.filter = filter[0];
            if(retValue.filter.CreateddateFrom__c!=NULL){
                string Fromdate = retValue.filter.CreateddateFrom__c.format('yyyy-MM-dd hh:mm:ss').replace(' ','T');
                string Todate = retValue.filter.CreateddateTo__c.format('yyyy-MM-dd hh:mm:ss').replace(' ','T');
                system.debug('@Fromdate@'+retValue.filter.CreateddateFrom__c);
                if(LoggedUserTenantId !=system.label.TradeAix_Tenant_Id){
                    query=query+' AND (( CreatedDate >' + Fromdate+'.000Z'+' AND CreatedDate <'+ Todate+'.000Z ) OR ( CreatedDate >' + Todate+'.000Z'+' AND CreatedDate <'+ Fromdate+'.000Z ))';  
                }else{
                    query=query+' WHERE (( CreatedDate >' + Fromdate+'.000Z'+' AND CreatedDate <'+ Todate+'.000Z ) OR ( CreatedDate >' + Todate+'.000Z'+' AND CreatedDate <'+ Fromdate+'.000Z ))';  
                    
                }
                retValue.isFilterA = true;
            }
        }
        if(selected!=NULL){
            system.debug('#Search#');
            retValue.isFilterA = true;
            string getOfferIds='';
            //Transaction_Attributes__c
            List<List<Transaction_Attributes__c >> Syndications  = new List<List<Transaction_Attributes__c >>();
            Syndications= [FIND :'*'+selected+'*' IN ALL FIELDS  RETURNING Transaction_Attributes__c (Transaction__c)];
            for(List<Transaction_Attributes__c> syffers : Syndications) {
                for (Transaction_Attributes__c synd : syffers) {
                    getOfferIds = getOfferIds + '\'' + synd.Transaction__c + '\',';
                }
            }
            //Transactions
            List<List<Transaction__c >> Trans  = new List<List<Transaction__c >>();
            Trans= [FIND :'*'+selected+'*' IN ALL FIELDS  RETURNING Transaction__c (Id)];
            for(List<Transaction__c> syffers : Trans) {
                for (Transaction__c t : syffers) {
                    getOfferIds = getOfferIds + '\'' + t.Id + '\',';
                }
            }
            
            
            
            if(getOfferIds!=''){
                system.debug('#getOfferIds#');
                getOfferIds = getOfferIds.substring(0, getOfferIds.length() - 1);
                if(LoggedUserTenantId !=system.label.TradeAix_Tenant_Id){
                    query = query + ' AND id IN ('+ getOfferIds +')';
                }
                else{
                    query = query + ' WHERE id IN ('+ getOfferIds +')';
                }
            }
            /*if(getOfferIds1!=''){
                system.debug('#getOfferIds1#');
                getOfferIds1 = getOfferIds1.substring(0, getOfferIds1.length() - 1);
                if(LoggedUserTenantId !=system.label.TradeAix_Tenant_Id){
                    query = query + ' AND id IN ('+ getOfferIds1 +')';
                }
                else{
                    query = query + ' WHERE id IN ('+ getOfferIds1 +')';
                }
            }*/
        }
        
        system.debug('===String Query==='+query);
        
        
        query=query+' order by CreatedDate desc '+ 'LIMIT '+retValue.list_size+' offset '+counter;
        system.debug('===String Query==='+query);
        //retValue.results = Database.query(query);
        List<Transaction__c> ListTransaction = new List<Transaction__c>();
        ListTransaction = Database.query(query);
     
        List<Transaction__c> ListTra =New List<Transaction__c>();
        MAP<Id,String> QuoMAP =New MAP<Id,String>();
        for(Quotes__c Q:[SELECT Transaction__c,Bid_Status__c FROM Quotes__c WHERE Bid_Status__c !=:'Quote Inactivated' AND CounteParty__c =:LoggedUserTenantId Order by LastmodifiedDate asc])
        {
            QuoMAP.put(Q.Transaction__c,Q.Bid_Status__c);
        }
        
        for(Transaction__c ListTrans : ListTransaction) {
            
            if(QuoMAP.containsKey(ListTrans.Id))
            {
                iF(QuoMAP.get(ListTrans.Id) =='Quote Closed')
                {
                    ListTrans.Status__c ='Completed Transaction';
                }else{
                    ListTrans.Status__c = QuoMAP.get(ListTrans.Id);
                }
            }
            else{
                 List<Tenant__c> ListT =[SELECT id,CreatedFromOTP__c,Tenant_Bank_Type__c FROM Tenant__c WHERE id=:LoggedUserTenantId LIMIT 1];
                if(ListT[0].Tenant_Bank_Type__c =='Counterparty Bank')
                { 
                if(ListTrans.Status__c !='Requested For Info'){
                    List<Quotes__c> Q = [SELECT Transaction__c,Bid_Status__c FROM Quotes__c WHERE Transaction__c =:ListTrans.Id AND CounteParty__c =:LoggedUserTenantId LIMIT 1];
                    if(Q.size() ==0){
                        system.debug('##T##'+ListTrans.Id);
                        system.debug('##LoggedUserTenantId##'+LoggedUserTenantId);
                        List<Request_Information_Quote__c>  RIQuote =[SELECT Status__c FROM Request_Information_Quote__c WHERE Transaction__c =: ListTrans.Id AND Tenant__c=:LoggedUserTenantId ORDER BY CreatedDate DESC Limit 1];
                        if(RIQuote.size()>0)
                        {if(RIQuote[0].Status__c =='Requested For Info')
                            ListTrans.Status__c ='Requested For Info';
                        }else{
                            ListTrans.Status__c ='Requested For Quote';
                        }
                    }
                }
                }
            }
            ListTra.add(ListTrans);
        }
        retValue.results = ListTra;
        retValue.total_size = retValue.results.Size();
        retValue.counter = counter; 
        retValue.sortbyField = sortbyField; 
        retValue.sortDirection = sortDirection;
        retValue.total_page = getTotalPages(retValue.list_size, retValue.total_size);
        retValue.selectedItem = 'Active';
        
        return retValue;   
    }
    private static Integer getTotalPages(Integer list_size, Integer total_size) { 
        if (math.mod(total_size, list_size) > 0) {
            return total_size/list_size + 1;
        }
        else {
            return (total_size/list_size);
        }
    }  
    @AuraEnabled
    public static LightSellerOfferListV1 Filter(Integer counter, String sortbyField, String sortDirection, String selected,String LoggedUserTenantId,String Filtervalue) { 
        
        return ShowTable( counter, sortbyField, sortDirection,  selected, LoggedUserTenantId, Filtervalue);
    }
    @AuraEnabled
    public static LightSellerOfferListV1 saveFilter(Integer counter, String sortbyField, String sortDirection,string filter,String LoggeduserTenantID,String LoggedUserId,String Filtervalue) 
    { 
        LightSellerOfferListV1 retValue = new LightSellerOfferListV1();
        try { 
            
            retValue = ShowTable(counter, sortbyField, sortDirection, filter, LoggeduserTenantID,Filtervalue); 
        } catch(Exception ex){
            System.debug('LightSellerOfferList - saveFilter() : ' + ex.getMessage());
            //Helper.ActivityLogInsertCall('','Error - LightSellerOfferList - saveFilter()',ex.getMessage()); 
        }  
        return retValue; 
    }
    @AuraEnabled
    public static LightSellerOfferListV1 alyFilter(Integer counter, String sortbyField, String sortDirection, String selected, UI_Filter__c filter,String LoggeduserTenantID,String LoggedUserId,String Filtervalue) 
    { 
        LightSellerOfferListV1 retValue = new LightSellerOfferListV1(); 
        try { 
            List<UI_Filter__c> filter1 = [SELECT Id,Name FROM UI_Filter__c WHERE Name =:'SellerOfferLists' AND CreatedById =: UserInfo.getUserId()]; 
            if(filter1.size() > 0){
                filter.Id = filter1[0].Id;
                system.debug('@F@'+filter);
                filter1[0] = filter;
                update filter1; 
            }
            else {
                filter.Name = 'SellerOfferLists';
                upsert filter; 
            }   
            Helper.setSessionValue('Filter', 'SellerOfferLists');
            retValue = ShowTable(counter, sortbyField, sortDirection, selected,LoggeduserTenantID,Filtervalue);
        } catch(Exception ex){
            System.debug('LightSellerOfferList - saveFilter() : ' + ex.getMessage());
            //Helper.ActivityLogInsertCall('','Error - LightSellerOfferList - saveFilter()',ex.getMessage()); 
        }  
        return retValue; 
    }
    @AuraEnabled
    public static UI_Filter__c getFilter() 
    { 
        UI_Filter__c filter = new UI_Filter__c(); 
        try { 
            List<UI_Filter__c> filter1 = [SELECT Id,CreateddateTo__c,CreateddateFrom__c,IsPrimary__c,Loan_Type__c,Transaction_Status__c,TemplateId_c__C, Name FROM UI_Filter__c WHERE Name =: 'QuoteList' AND CreatedById =: UserInfo.getUserId()]; 
            if(filter1.size() > 0){
                filter = filter1[0]; 
                return filter;
            } 
        } catch(Exception ex){
            System.debug('LightSellerOfferList - getFilter() : ' + ex.getMessage());
            //Helper.ActivityLogInsertCall('','Error - LightSellerOfferList - getFilter()',ex.getMessage()); 
        }
        return filter; 
    }
    @AuraEnabled
    public static LightSellerOfferListV1 getBeginning(Integer counter, String sortbyField, String sortDirection, String selected, String LoggedUserTenantId,String Filtervalue) { 
        return ShowTable( counter, sortbyField, sortDirection,NULL, LoggedUserTenantId, Filtervalue);
    }
    
    @AuraEnabled
    public static LightSellerOfferListV1 Views(Integer counter, String sortbyField, String sortDirection, String selected, String LoggedUserTenantId,String Filtervalue) { 
        Helper.setSessionValue('sessionselectedPT', selected);
        return ShowTable( counter, sortbyField, sortDirection,  selected, LoggedUserTenantId, Filtervalue);
    }
    
    @AuraEnabled
    public static LightSellerOfferListV1 getPrevious(Integer counter, String sortbyField, String sortDirection, String selected, String LoggedUserTenantId,String Filtervalue) {
        return ShowTable( counter, sortbyField, sortDirection,  selected, LoggedUserTenantId, Filtervalue);
    }
    
    @AuraEnabled
    public static LightSellerOfferListV1 getNext(Integer counter, String sortbyField, String sortDirection, String selected, String LoggedUserTenantId,String Filtervalue) {  
        return ShowTable( counter, sortbyField, sortDirection,  selected, LoggedUserTenantId, Filtervalue);
    }
    
    @AuraEnabled
    public static LightSellerOfferListV1 getEnd(Integer counter, String sortbyField, String sortDirection, String selected, String LoggedUserTenantId,String Filtervalue) { 
        return ShowTable( counter, sortbyField, sortDirection,  selected, LoggedUserTenantId, Filtervalue);
    } 
    
    @AuraEnabled
    public static LightSellerOfferListV1 changelist_size(Integer counter, String sortbyField, String sortDirection, Integer list_size, String selected, String LoggedUserTenantId,String Filtervalue) { 
        LightSellerOfferListV1 retValue = new LightSellerOfferListV1(); 
        try{
            size = [SELECT count() from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'TemplateLists' AND Grid_Name__c =: 'SellerOfferLists'];
            if(size > 0){  
                UI_List_Size__c listSize = new UI_List_Size__c(
                    Id = [SELECT Id from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'SellerOfferLists' AND Grid_Name__c =: 'SellerOfferLists'][0].Id,
                    List_Size__c = list_size  
                );
                update listSize;
            }
            else {
                UI_List_Size__c listSize = new UI_List_Size__c(
                    List_Size__c = list_size,
                    Name = 'SellerOfferLists',
                    Grid_Name__c = 'SellerOfferLists',
                    User__c = UserInfo.getUserId()
                );
                insert listSize;
            }  
            
            return ShowTable( counter, sortbyField, sortDirection,  selected, LoggedUserTenantId, Filtervalue);
            
        } catch(Exception ex){
            //Logger.LogException(
            //   'Error - LightProductTemplate - changelist_size()', 
            //   ex.getMessage()
            //);
        }
        return retValue;
    }
    
    @AuraEnabled
    public static LightSellerOfferListV1 SortTable(Integer counter, String sortbyField, String sortDirection, String selected, String LoggedUserTenantId,String Filtervalue) { 
        LightSellerOfferListV1 retValue = new LightSellerOfferListV1(); 
        try{
            if(sortbyField == '') {
                sortbyField = 'LastModifiedDate';
                sortDirection = 'DESC';
            } else {
                if(sortDirection.equals('ASC')){
                    sortDirection = 'DESC';
                } else {
                    sortDirection = 'ASC';
                }
            }
            Helper.setSessionValue('sortbyFieldSOL', sortbyField);
            Helper.setSessionValue('sortDirectionSOL', sortDirection); 
            return ShowTable( counter, sortbyField, sortDirection,  selected, LoggedUserTenantId, Filtervalue);
        } catch (Exception ex){ 
            // Logger.LogException(
            //     'Error - LightProductTemplate - SortTable()', 
            //     ex.getMessage()
            // );
        }
        return retValue;
    } 
    
    @AuraEnabled
    public static LightSellerOfferListV1 changePage(Integer counter, String sortbyField, String sortDirection, String selected, String LoggedUserTenantId,String Filtervalue) {
        return ShowTable( counter, sortbyField, sortDirection,  selected, LoggedUserTenantId, Filtervalue);
    }
}