public class LightDashboardCtrlV1 {
    @AuraEnabled
    public DateTime ExpDate {get;set;}
    @AuraEnabled
    public DateTime ValDate {get;set;}
}