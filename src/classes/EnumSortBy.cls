public enum EnumSortBy {
    CUName,
    OfferCUName,
    OfferStatus,
    OfferName,
    BuyerName,
    PublishedDate,
    NDASignDate,
    TemplateName,
    Title,
    TagCsv,
    CreatedDate
}