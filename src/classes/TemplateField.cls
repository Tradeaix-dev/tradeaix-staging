public class TemplateField {
    @AuraEnabled 
    public String label {get; set;}
    @AuraEnabled 
    public String value {get; set;}
    @AuraEnabled 
    public String fieldType {get; set;}
    @AuraEnabled 
    public String fieldSize {get; set;}
    @AuraEnabled 
    public Boolean fieldmandatory {get; set;}
    @AuraEnabled 
    public String LastModifiedByName {get; set;}
     @AuraEnabled 
    public String CreatedByName {get; set;}
    @AuraEnabled 
    public String AttID {get; set;}
    
    public TemplateField(String value, String label,String AttID, string fieldType, string fieldSize,Boolean fieldmandatory,String LastModifiedByName,String CreatedByName) {
        this(value, label);
        this.AttID = AttID;
        this.fieldType = fieldType;
        this.fieldSize = fieldSize;
        this.fieldmandatory = fieldmandatory;
        this.LastModifiedByName =LastModifiedByName;
        this.CreatedByName=CreatedByName; 
    }
    
    public TemplateField(String value, String label) {
        this.value = value;
        this.label = label;
        this.fieldType = 'Text';
        this.fieldSize = '255';
    }
}