public without sharing class  LightTransactions {
    @AuraEnabled 
    public static Organization__c[] loadorgs(String username) {
        return Helper.loadorgs(username);
    }
    @AuraEnabled 
    public static Transaction__c[] TransactionDetails(String Filtervalue)
    {   
        return ShowTable(Filtervalue);
    }
    private static Transaction__c[] ShowTable(String Filtervalue)
    {
        List<String> TransIds = Filtervalue.split(',');
        
        if(TransIds.size() > 0){
            Return [SELECT Id,Notes__c,Tenant__r.Organization__r.Name,Tenant__r.Tenant_Site_Name__c,Product_Template__r.TemplateName__c,Status__c,Loan_Type__c,CreatedDate,TransactionRefNumber__c,Currency__c,(SELECT id,Product_Template__c,Attribute_Name__c,Attribute_Value__c,Display_on_gride__c,Sort_Order__c FROM Transaction_Attributes__r) FROM Transaction__c WHERE Id IN:TransIds order by CreatedDate desc];
        }else{
            Return [SELECT Id,Notes__c,Tenant__r.Organization__r.Name,Tenant__r.Tenant_Site_Name__c,Product_Template__r.TemplateName__c,Status__c,Loan_Type__c,CreatedDate,TransactionRefNumber__c,Currency__c,(SELECT id,Product_Template__c,Attribute_Name__c,Attribute_Value__c,Display_on_gride__c,Sort_Order__c FROM Transaction_Attributes__r) FROM Transaction__c order by CreatedDate desc];
        }
    }
    @AuraEnabled
    public static Transaction__c[] Filter(String Filtervalue) { 
        
        return ShowTable(Filtervalue);
    }
}