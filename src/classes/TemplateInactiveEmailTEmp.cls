public class TemplateInactiveEmailTEmp {
	public Id tempId {get;set;} 
    
    public class TemplateDetails { 
        //public String tempId {get;set;}
        public String templateName {get;set;}
        public String templateType {get;set;}   
        public String tenantName {get;set;}  
        public String marketType {get;set;}  
        public Boolean templateStatus {get;set;}  
        public String logo{get;set;}
        public String CreatedBy {get;set;}
        public String CreatedDate {get;set;}
        public String Title {get;set;}
        public String Email {get;set;}
        public String Phone {get;set;}
        public String City {get;set;}
        public String State {get;set;}
        public String Thanksmsg{get;set;}
        public List<String> Body{get;set;}        
    }
    
    public TemplateDetails email {get;set;}  
    public TemplateDetails getTemplate(){
        email = New TemplateDetails();
        try{           
           
            ProductTemplate__c PT = [SELECT Id, Name,CreatedDate, TemplateName__c, ProductType__c, Active__c,IsPrimary__c, Tenant__r.Tenant_Site_Name__c,Tenant__r.Tenant_Logo_Url__c,Tenant__r.Tenant_Footer_Message__c,Created_ByName__c from ProductTemplate__c WHERE ID=: tempId];
            email.templateName = PT.TemplateName__c;
            email.templateType = PT.ProductType__c;
            email.templateStatus = PT.Active__c;
            email.tenantName = PT.Tenant__r.Tenant_Site_Name__c;
            email.logo = PT.Tenant__r.Tenant_Logo_Url__c;
            email.Thanksmsg = PT.Tenant__r.Tenant_Footer_Message__c;
            if(PT.IsPrimary__c == true){
                email.marketType = 'Primary';
            }else{
                email.marketType = 'Secondary';
            }
            Datetime myDT = PT.CreatedDate; 
            TimeZone tz = UserInfo.getTimeZone();
            String myDate = myDT.format('dd/MM/YYYY HH:mm:ss', tz.getID());             
            email.CreatedDate = myDate;            
            List<String> emailBody = new List<String>();           
            for(Product_Template_Object__c PTO:[SELECT Id, Name,Attributes__r.Name from Product_Template_Object__c WHERE Product_Template__c =: PT.Id]){
                emailBody.add(PTO.Attributes__r.Name);       
            }
            //Party Information
            User_Management__c partyUser = [SELECT Id, Tenant__c,Tenant__r.Tenant_Site_Name__c ,Name,First_Name__c,Last_Name__c,User_Name__c,Profiles__r.Name,User_Email__c,Street__c, City__c,State_Province__c,Title__c,Phone__c,Tenant__r.Tenant_Footer_Message__c, Tenant__r.Tenant_Logo_Url__c from User_Management__c WHERE Id =: PT.Created_ByName__c];
            email.CreatedBy = partyUser.First_Name__c + ' '+partyUser.Last_Name__c ;
            email.Title = partyUser.Title__c;
            email.Email = partyUser.User_Email__c;
            email.Phone = partyUser.Phone__c;
            email.City = partyUser.city__c;
            email.State = partyUser.State_Province__c;    
            
            
            system.debug('===emailBody==='+emailBody);
            email.Body = emailBody;
        }catch(exception e){
            system.debug('//****// '+e.getMessage());
            email.tenantName = e.getMessage();
        } 
        return email;
    }
}