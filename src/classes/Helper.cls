public without sharing class Helper {
    
    public static Organization__c[] loadorgs(String username) {
        List<User_Management__c> lstName= [Select Id,Tenant__c,Tenant__r.Tenant_Short_Name__c,Tenant__r.Tenant_Logo_Url__c,First_Name__c,Last_Name__c,Profiles__c,Tenant__r.Organization__c,Tenant__r.Organization__r.isPrimary__c from User_Management__c where Id =: username LIMIT 1];
        If(lstName[0].Tenant__r.Organization__r.isPrimary__c)
        {return [ select Id, Name ,(select Id,Tenant_Site_Name__c from Tenants__r) from Organization__c WHERE CreatedFromOTP__c=false AND Id!=:system.label.TradeAix_Org_Id AND isPrimary__c=true order by Name asc];
        }else{ 
            return [ select Id, Name ,(select Id,Tenant_Site_Name__c from Tenants__r) from Organization__c WHERE Id=:lstName[0].Tenant__r.Organization__c AND CreatedFromOTP__c=false  AND isPrimary__c=true AND Id!=:system.label.TradeAix_Org_Id order by Name asc];
        }
    }
    
    public static String getLoginUserDetailsForOTP(String ID, string browsername,String ipAddress, String LoginURL)
    {
        
        List<User_Management__c> UM=[Select Name,Id,Time_Zone__c,Password__c,User_Name__c From User_Management__c WHERE Id=:ID LIMIT 1];
        system.debug('##'+UM);
        if(UM.Size() >0){
            Login_History__c LH = new Login_History__c();
            LH.BrowserName__c = browsername;
            LH.Login_Time__c = system.now();
            LH.SessionID__c = userinfo.getSessionId();
            LH.User_Management__c = UM[0].Id;
            LH.IP_Address__c = ipAddress;
            LH.Login_URL__c = LoginURL;
            LH.Location__c = UM[0].Time_Zone__c;
            insert LH;
            system.debug('##'+LH.id);
            return UM[0].Id;
        }else{
            return 'Failed';
        }
    }
    
    public static String getLoginUserDetails(String username, String password, String TenantId, string browsername,String ipAddress, String LoginURL)
    {
        system.debug('##'+username);
        system.debug('##'+password);
        system.debug('##'+TenantId);
        system.debug('##'+browsername);
        system.debug('##'+ipAddress);
        system.debug('##'+LoginURL);
        system.debug('##'+userinfo.getSessionId());
        
        List<User_Management__c> UM=[Select Name,Id,Time_Zone__c,Password__c,User_Name__c From User_Management__c WHERE User_Name__c=:username AND Password__c =:password AND Tenant__r.Tenant_Short_Name__c=:TenantId LIMIT 1];
        system.debug('##'+UM);
        if(UM.Size() >0){
            Login_History__c LH = new Login_History__c();
            LH.BrowserName__c = browsername;
            LH.Login_Time__c = system.now();
            LH.SessionID__c = userinfo.getSessionId();
            LH.User_Management__c = UM[0].Id;
            LH.IP_Address__c = ipAddress;
            LH.Login_URL__c = LoginURL;
            LH.Location__c = UM[0].Time_Zone__c;
            insert LH;
            system.debug('##'+LH.id);
            return UM[0].Id;
        }else{
            return 'Failed';
        }
    }
    
    public static String ActivityLogInsertCall(String ActionBy, String Operation, String OperationLog,Boolean ErrorCall)
    {
        system.debug('##'+ActionBy);
        system.debug('##'+Operation);
        system.debug('##'+OperationLog);
        system.debug('##'+ErrorCall);
        Activity_Log__c AL =New Activity_Log__c();
        AL.Action_By__c = ActionBy;
        AL.Operation__c = Operation;
        AL.Operation_Log__c = OperationLog;
        AL.ErrorCall__c = ErrorCall;
        Insert AL;
        return '';
    }
    public static String ActivityLogInsertCallForRecord(String ActionBy,String RecordID, String Operation, String OperationLog,Boolean ErrorCall)
    {
        Activity_Log__c AL =New Activity_Log__c();
        AL.Action_By__c = ActionBy;
        AL.Operation__c = Operation;
        AL.RecordID__c =RecordID;
        AL.Operation_Log__c = OperationLog;
        AL.ErrorCall__c = ErrorCall;
        Insert AL;
        return '';
    }
    
    public static String NotificationInsertCall(String ActionBy,String ActionTo, String Status, String Comments,String ReferenceId,String RecordLink)
    {
        Notification__c Noti =New Notification__c();
        Noti.Action_By__c = ActionBy;
        Noti.Action_To__c = ActionTo;
        Noti.Action__c = Status;
        Noti.Message__c = Comments;
        Noti.Reference_Id__c = ReferenceId;
        Noti.Record_Link__c = RecordLink;
        Noti.ReadFlag__c=true;
        Insert Noti;
        return '';
    }
    
    public static String getSessionValue(String key)
    {     
        return (String)Cache.Session.get(key);
    }
    
    public static void setSessionValue(String key, String value)
    {     
        Cache.Session.put(key, value); 
    }
    public static User getUser(List<User> usrList, String UserId)
    {
        User usr = new User();
        for(Integer j = 0; j < usrList.size(); j++){
            if(usrList.get(j).Id == UserId){
                usr =  (User)usrList.get(j); 
                break;
            }
        }
        return usr;
    }
    
    public static String getDocuSignSettingValue(String key)
    {
        return [SELECT Value__c from App_Settings__c WHERE Name =: key LIMIT 1].Value__c;         
    }
}