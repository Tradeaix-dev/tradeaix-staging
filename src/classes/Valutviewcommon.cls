public class Valutviewcommon {
     	@AuraEnabled
        public List <LightOfferExtended> results = new List <LightOfferExtended>();
    	@AuraEnabled
        public Integer counter = 0; 
        @AuraEnabled
        public String selectedItem = ''; 
    	@AuraEnabled
        public String showpage = '';
        @AuraEnabled
        public String sortbyField = '';
        @AuraEnabled
        public String sortDirection = '';
        @AuraEnabled
        public Integer total_page = 0; 
        @AuraEnabled
        public Integer list_size = 0;
        @AuraEnabled
        public Integer total_size = 0;
    	@AuraEnabled
    	public List <ContentVersion> content {get;set;}
     	@AuraEnabled
        public String FileUploadUrl='';
}