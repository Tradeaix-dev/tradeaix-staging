public class IssuingBankCtrl {
	@AuraEnabled
    public static string SaveIssuingBank(String LoggedUserID,Tenant__c TenantDetails)
    {
        try{
            TenantDetails.CreatedBy__c = LoggedUserID;
            TenantDetails.LastModifiedBy__c = LoggedUserID;
            TenantDetails.Organization__c = System.Label.TradeAix_Org_Id;
            TenantDetails.Tenant_Type__c = 'Bank';
			TenantDetails.Tenant_Bank_Type__c = 'Issuing Bank';
            insert TenantDetails;            
            Helper.ActivityLogInsertCallForRecord(LoggedUserID, TenantDetails.Id, 'Issuing Bank Created','', false);
        }catch(exception ex){
            system.debug('==SavesetupOrg='+ex.getMessage());
        }
        return null;
    }
    
    @AuraEnabled
    public static IssuingBankCtrlV getIssuingBankDetails(String IssuingbankId)
    {
        IssuingBankCtrlV bankresult = new IssuingBankCtrlV();
        try{
            bankresult.result = [SELECT Id, Name, Tenant_Site_Name__c, Tenant_Type__c, Tenant_Bank_Type__c,Tenant_Swift_Code__c,City__c,Country__c,State_Province__c,Street__c,Zip_Postal_Code__c,Country_Code__c,Tenant_Website__c,Primary_Contact_Name__c,Primary_Contact_Email__c,Primary_Contact_Mobile__c,CreatedDate,CreatedBy__r.First_Name__c,CreatedBy__r.Last_Name__c,LastModifiedBy__r.First_Name__c,LastModifiedBy__r.Last_name__c,LastModifiedDate,Approve_Reject_Reason__c from Tenant__c WHERE Id =: IssuingbankId];
            bankresult.Alogs =[SELECT id,CreatedDate,Action_By__r.First_Name__c,Action_By__r.Last_Name__c,Operation_Log__c FROM Activity_Log__c WHERE RecordID__c=:IssuingbankId];
        }catch(exception ex){
            
        }
        return bankresult;
    }
    @AuraEnabled
    public static String approveIssuingbank(String IssuingbankId, String LoggedUserID, string Comments)
    {
        try{
            system.debug('==approveIssuingbank try==');
            Tenant__c T = new Tenant__c();
            T.Id = IssuingbankId;
            T.Approve_Reject_Reason__c = Comments;
            T.Issuing_Bank_Approved__c = true;
            T.Issuing_Bank_Status__c = 'Approved';
            T.IsActive__c = true;
            T.LastModifiedBy__c = LoggedUserID;
            update T;
            Helper.ActivityLogInsertCallForRecord(LoggedUserID, IssuingbankId, 'Issuing Bank Approved',Comments, false);
            //Helper.NotificationInsertCall(LoggedUserID,lstStr.Id,'Issuing Bank Approved','',t.TransactionRefNumber__c,t.id);
            return 'Success';
        }catch(exception ex){
            system.debug('==approveIssuingbank Exception=='+ex.getMessage());
            return ex.getMessage();
        }
        
    }
    @AuraEnabled
    public static String rejectIssuingbank(String IssuingbankId, String LoggedUserID, string Comments)
    {
         try{
            Tenant__c T = new Tenant__c();
            T.Id = IssuingbankId;
            T.Approve_Reject_Reason__c = Comments;
            T.Issuing_Bank_Approved__c = false;
            T.Issuing_Bank_Status__c = 'Rejected';
            T.LastModifiedBy__c = LoggedUserID;
            update T;
            Helper.ActivityLogInsertCallForRecord(LoggedUserID, IssuingbankId, 'Issuing Bank Rejected',Comments, false);
       		return 'Success';
         }catch(exception ex){
             system.debug('==approveIssuingbank Exception=='+ex.getMessage());
             return ex.getMessage();
        }
        
    }
}