public class AttributeCreateEmailTemp {
    public Id attId {get;set;} 
     public class AttributeDetails { 
        public String logo{get;set;}
        public String CreatedBy {get;set;}
        public String CreatedDate {get;set;}
        public String attributeName {get;set;}
        public String attributesize {get;set;}
        public String producttype {get;set;}
        public String attributetype {get;set;}
        public String primary {get;set;}
        public String secondary {get;set;}
        public String active {get;set;}    
        public String Tenantname {get;set;}
         public Boolean Mandatory{get;set;}
        public String Thanksmsg{get;set;}
     }
    public AttributeDetails email {get;set;}  
    public AttributeDetails getAttribute(){
        email = New AttributeDetails();
         try{  
             Attributes__c A = [SELECT Id, Name,Attribute_Size__c,CreatedDate,Attribute_Type__c,CreatedByName__c,Edit_Attribute__c,IsActive__c,IsPrimary__c,IsSecondary__c,LastModifiedByName__c,Mandatory__c,ProductType__c,Tenant__c,CreatedByName__r.First_Name__c,CreatedByName__r.Last_Name__c,Tenant__r.Tenant_Logo_Url__c,Tenant__r.Tenant_Site_Name__c,Tenant__r.Tenant_Footer_Message__c from Attributes__c WHERE Id =: attId];
             email.attributeName = A.Name;
             email.attributesize = String.ValueOf(A.Attribute_Size__c);
             if(A.ProductType__c == 'Both'){
                 email.producttype = 'Funded, UnFunded';
             }else{
                 email.producttype = A.ProductType__c;
             }
             email.attributetype = A.Attribute_Type__c;
             if(A.IsPrimary__c == true){
                 email.primary = 'True';
              }else{
                 email.primary = 'False';         
              }
             if(A.IsSecondary__c == true){
                 email.secondary = 'True';
              }else{
                 email.secondary = 'False';         
              }
             if(A.IsActive__c == true){
                 email.active = 'True';
              }else{
                 email.active = 'False';         
              }
              email.Mandatory =A.Mandatory__c;
             Datetime myDT = A.CreatedDate; 
             TimeZone tz = UserInfo.getTimeZone();
             String myDate = myDT.format('dd/MM/YYYY HH:mm:ss', tz.getID());             
             email.CreatedDate = myDate;  
             email.CreatedBy = A.CreatedByName__r.First_Name__c+' '+A.CreatedByName__r.Last_Name__c;
             email.logo = A.Tenant__r.Tenant_Logo_Url__c;
             email.Tenantname = A.Tenant__r.Tenant_Site_Name__c;
             email.Thanksmsg = A.Tenant__r.Tenant_Footer_Message__c;
             
         }catch(exception e){
            system.debug('//****// '+e.getMessage());           
        } 
        return email;
    }
}