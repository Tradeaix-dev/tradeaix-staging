public class UserMultilookupCtrl {
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName, List<sObject> ExcludeitemsList,string LoggeduserTenantID, string userId) {
        String searchKey = '%' + searchKeyWord + '%';
        List < sObject > returnList = new List < sObject > ();
        List<string> lstExcludeitems = new List<string>();
        List<string> Excludeitems = new List<string>();
        SET<Id> TSET =NEW SET<Id>();
        for(Tenant_Mapping__c TM:[SELECT Id,Tenant_Approved__c FROM  Tenant_Mapping__c WHERE Tenant__c=:LoggeduserTenantID])
        {
           TSET.add(TM.Tenant_Approved__c); 
        }
        List<User_Management__c> lstUM = [SELECT Id from User_Management__c WHERE Tenant__c =: LoggeduserTenantID OR Tenant__c IN:TSET];
        for(User_Management__c UM:lstUM){
            lstExcludeitems.add(UM.Id);
        }        
        //lstExcludeitems.add('a0S6C00000091Gc');
        for(sObject item : ExcludeitemsList ){
            Excludeitems.add(item.id);
        }
        system.debug('==lstExcludeitems=='+lstExcludeitems);
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5 and exclude already selected records  
        String sQuery =  'select id, Name, User_Email__c,User_Name__c from ' +ObjectName + ' where ((User_Email__c LIKE:searchKey AND Id IN : lstExcludeitems) AND Id NOT IN:Excludeitems) AND Id !=:userId order by createdDate DESC limit 5';
        //String sQuery =  'select id, Name, User_Email__c,User_Name__c from ' +ObjectName + ' where User_Email__c LIKE: searchKey AND Id NOT IN : lstExcludeitems order by createdDate DESC limit 5';
        system.debug('====String Query===='+sQuery);
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        system.debug('====returnList===='+returnList);
        return returnList;
    }
    
    @AuraEnabled
    public static String foldersharing(String fileId, List<User_Management__c> userlist, String LoggedUserId)
    {
        try{
            Map<String,File_Sharing__c> FSMap = new Map<String,File_Sharing__c>();
            for(File_Sharing__c FSharing: [SELECT Id, Name, Document_Vault_Folder__r.Folder_Name__c,Tenant_User__r.User_Email__c,Content_Id__c 
                                           from File_Sharing__c WHERE Content_Id__c =: fileId]){
                    FSMap.put(FSharing.Tenant_User__r.User_Email__c,FSharing); 
                }
            set<Id> UIds = new set<Id>();
            system.debug('==foldersharing==');
            List<File_Sharing__c> lstFile = new List<File_Sharing__c>();
            for(User_Management__c s: userlist){
                if(!FSMap.containsKey(s.User_Email__c)){
                system.debug('==selectedUsers=='+s);
                File_Sharing__c FS = new File_Sharing__c();
                if(fileId.contains('068')){
                    FS.Content_Id__c = fileId; 
                    ContentVersion[] lstcontent = [SELECT CreatedById, CreatedDate, Id , Title, ParentId__c, TagCsv,Tenant_User__r.First_Name__c,Tenant_User__r.Last_Name__c,ContentSize,FileExtension,RefType__c,Reference__c,ParentName__c  FROM ContentVersion where Id =:fileId];
                	FS.File_Folder_Name__c = lstcontent[0].Title+'.'+lstcontent[0].FileExtension;
                }else{
                    String foldername = [SELECT Folder_Name__c from Document_Vault_Folder__c WHERE Id =: fileId].Folder_Name__c;
                    FS.Content_Id__c = fileId;
                    FS.Document_Vault_Folder__c = fileId;
                    FS.File_Folder_Name__c = foldername +'- Invitation to collaborate';
                }                
                FS.File_Permission__c = 'Edit';
                FS.Tenant_User__c = s.Id;
                FS.CreatedBy__c = LoggedUserId;
                FS.LastModifiedBy__c = LoggedUserId;
                lstFile.add(FS);    
                UIds.add(s.Id);
            	}
            }
            system.debug('==lstFile Size=='+lstFile.size());
            if(lstFile.size()>0){
                
                insert lstFile;
            }
            
            for(File_Sharing__c FSharing: [SELECT Id, Name, Document_Vault_Folder__r.Folder_Name__c,Tenant_User__c,Content_Id__c from File_Sharing__c WHERE Content_Id__c =: fileId AND Tenant_User__c =: UIds]){
                if(FSharing.Content_Id__c.contains('068')){
                	Helper.NotificationInsertCall(LoggedUserID,FSharing.Tenant_User__c,'File Shared','',FSharing.Document_Vault_Folder__r.Folder_Name__c,FSharing.Id); 
                }else{
                    Helper.NotificationInsertCall(LoggedUserID,FSharing.Tenant_User__c,'Folder Shared','',FSharing.Document_Vault_Folder__r.Folder_Name__c,FSharing.Id); 
                }
            }
            if(fileId.contains('068')){
                ContentVersion CV = new ContentVersion();
                CV.Id = fileId;
                CV.File_Share__c = 'Share';
                update CV;
                UpdatetoPrivateCVersion(fileId);
            }else{
                Document_Vault_Folder__c DVF = new Document_Vault_Folder__c();
                DVF.Id = fileId;
                DVF.File_Share__c = 'Share';
                update DVF;
                UpdatetoPrivate(fileId);
            }
            
            return 'Success';            
        }catch(exception ex){
            system.debug('==lstFile exception=='+ex.getMessage()+':::'+string.valueof(ex.getLineNumber()));
            return ex.getMessage();
        }
    }
    
     Private static string UpdatetoPrivateCVersion(string selectedId)
    {
        
        SET<id> DF = New SET<id>();
               for(ContentVersion DDF:[SELECT id FROM ContentVersion WHERE ParentId__c=:selectedId])
               {
                   DF.add(DDF.id);
               }
               if(DF.size()>0)
               {
                  for(ContentVersion DDF:[SELECT id FROM ContentVersion WHERE ParentId__c IN:DF])
               {
                   DF.add(DDF.id);
               }  
               }
               
               if(DF.size()>0)
               {
                   List<ContentVersion> DVList = New List<ContentVersion>();
                   for(ContentVersion DDF:[SELECT id FROM ContentVersion WHERE ID IN:DF])
                   {
                       ContentVersion DV1 = new ContentVersion();
                       DV1.Id = DDF.id;
                       DV1.File_Share__c = 'Share';                      
                       DVList.add(DV1);
                   }
                   system.debug('<##>'+DVList.size());
                   if(DVList.size()>0)
                   {
                       update DVList;
                   }
               }
        return '';
    }
    Private static string UpdatetoPrivate(string selectedId)
    {
        
        SET<id> DF = New SET<id>();
               for(Document_Vault_Folder__c DDF:[SELECT id FROM Document_Vault_Folder__c WHERE Parent_Folder__c=:selectedId])
               {
                   DF.add(DDF.id);
               }
               if(DF.size()>0)
               {
                  for(Document_Vault_Folder__c DDF:[SELECT id FROM Document_Vault_Folder__c WHERE Parent_Folder__c IN:DF])
               {
                   DF.add(DDF.id);
               }  
               }
               
               if(DF.size()>0)
               {
                   List<Document_Vault_Folder__c> DVList = New List<Document_Vault_Folder__c>();
                   for(Document_Vault_Folder__c DDF:[SELECT id FROM Document_Vault_Folder__c WHERE ID IN:DF])
                   {
                       Document_Vault_Folder__c DV1 = new Document_Vault_Folder__c();
                       DV1.Id = DDF.id;
                       DV1.File_Share__c = 'Share';
                       DVList.add(DV1);
                   }
                   system.debug('<##>'+DVList.size());
                   if(DVList.size()>0)
                   {
                       update DVList;
                   }
               }
        return '';
    }
}