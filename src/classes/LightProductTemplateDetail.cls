public class LightProductTemplateDetail {
    
    @AuraEnabled
    public static String getloggeduserBrowserTitle() { 
        String Btitle = '';
        
        return '';
    }
    @AuraEnabled
    public static string SaveRFI(string RFI,string Transid){
        String retValue = '';
        system.debug('@RFI@'+RFI);
        try{
            ProductTemplate__c updateoffLIST =new ProductTemplate__c();
            updateoffLIST.Transactiondetailfields__c=RFI;
            updateoffLIST.Id=Transid;
            update updateoffLIST;
        }
        catch(Exception ex){
            System.debug('addRemoveCounterParty: ' + ex.getMessage());
            retValue = 'ex.getMessage()';
        }
        return retValue;
    }
    @AuraEnabled
    public static LightProductTemplateDetailV getProductTemplate(string templateId){
        LightProductTemplateDetailV ProdTemlate = new LightProductTemplateDetailV();
        String selectedObject  = '';
        ProdTemlate.ProdTemp = [SELECT Id, Name, ProductType__c, TemplateName__c, Active__c, IsPrimary__c,CreatedById,LastModifiedById,Created_ByName__r.Last_Name__c,Last_ModifiedByName__r.Last_Name__c,Created_ByName__r.First_Name__c,Last_ModifiedByName__r.First_Name__c, CreatedDate,LastModifiedDate FROM ProductTemplate__c WHERE Id =: templateId];
        ProdTemlate.Alogs =[SELECT id,Operation__c,CreatedDate,Action_By__r.First_Name__c,Action_By__r.Last_Name__c,Operation_Log__c FROM Activity_Log__c WHERE RecordID__c=:templateId];
           system.debug('###'+ProdTemlate.Alogs);
        /*
        system.debug('// test1 '+ ProdTemlate.ProdTemp); 
        selectedObject = ProdTemlate.ProdTemp.SObject_Name__c;
        String type = selectedObject;  // Say,this is my object
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(type);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        for (String fieldName: fieldMap.keySet()) {  
            String mylabel = fieldMap.get(fieldName).getDescribe().getLabel();
            if(mylabel != 'Record ID' && !mylabel.contains(ProdTemlate.ProdTemp.TemplateName__c) && mylabel != 'Currency ISO Code' && mylabel != 'Owner ID' && mylabel != 'Deleted' && mylabel != 'Created Date' && mylabel != 'Created By ID' && mylabel != 'System Modstamp' && mylabel != 'Last Modified By ID' && mylabel != 'Last Modified Date' ){
                lst.add(mylabel);
            }
        }*/
        ProdTemlate.strList = [SELECT Attributes__r.Name FROM Product_Template_Object__c WHERE Product_Template__c=:templateId];
        system.debug('###'+ProdTemlate);
        return ProdTemlate;
    } 
    
    @AuraEnabled
    public static LightProductTemplateDetailV Inactive(String templateId, String body5 )
    { 
        LightProductTemplateDetailV ProdTemlate = new LightProductTemplateDetailV();
        
        ProductTemplate__c ProdTemp1 = [SELECT Id,TemplateName__c,Name,Created_ByName__c,Active__c FROM ProductTemplate__c WHERE Id =: templateId];
        ProdTemp1.Active__c = false;
        
        update ProdTemp1; 
        Helper.ActivityLogInsertCallForRecord(ProdTemp1.Created_ByName__c,ProdTemp1.Id,'Product Template Inactivated',ProdTemp1.TemplateName__c+' Inactivated.',false);

        ProdTemlate = getProductTemplate(templateId); 
        return ProdTemlate;
    }
}