public class EmailManager {  
    @future
    public static void sendMailWithTemplate1(String address, String[] CcAddresses, String templateId, String targetObjectId, String whatId){  
        try{
            system.debug('==sendMailWithTemplate=='+address+'::'+templateId+'::'+targetObjectId+'::'+whatId);
            Messaging.Singleemailmessage email = new Messaging.Singleemailmessage();        
            String[] toAddresses = new String[] {address}; 
            email.setToAddresses(toAddresses); 
            
            List<String> GmailList = New List<String>();
            String[] Groupmail = System.label.GroupEmail.split(',');
            for (String str : Groupmail) {
            GmailList.add(str);
            }
            email.setCcAddresses(GmailList);
            
            email.setBccAddresses(CcAddresses);
            email.setTemplateId(templateId);
            email.setTargetObjectId(targetObjectId);
            email.setWhatId(whatId);
            email.setReplyTo('barclays_email@oxgyzjiud6h170gvvnxddzx6trvvpoo3q798q7h21f6n8c8zm.4f-13amuaq.cs93.apex.sandbox.salesforce.com');
            email.setSaveAsActivity(false);            
            Messaging.SendEmailResult[] results = Messaging.sendEmail(
            new Messaging.SingleEmailMessage[] { email });            
        } catch(exception ex){
            system.debug('//****// '+ex.getMessage());
        } 
    }
    public void sendMailWithTemplate(String address, String[] CcAddresses, String templateId, String targetObjectId, String whatId){  
        try{
            system.debug('==sendMailWithTemplate=='+address+'::'+templateId+'::'+targetObjectId+'::'+whatId);
            Messaging.Singleemailmessage email = new Messaging.Singleemailmessage();        
            String[] toAddresses = new String[] {address}; 
            email.setToAddresses(toAddresses); 
            //email.setCcAddresses(CcAddresses);
            email.setBccAddresses(CcAddresses);
            email.setTemplateId(templateId);
            email.setTargetObjectId(targetObjectId);
            email.setWhatId(whatId);
             email.setReplyTo('barclays_email@oxgyzjiud6h170gvvnxddzx6trvvpoo3q798q7h21f6n8c8zm.4f-13amuaq.cs93.apex.sandbox.salesforce.com');
           
            email.setSaveAsActivity(false);            
            Messaging.SendEmailResult[] results = Messaging.sendEmail(
            new Messaging.SingleEmailMessage[] { email });            
        } catch(exception ex){
            system.debug('//****// '+ex.getMessage());
        } 
    }
}