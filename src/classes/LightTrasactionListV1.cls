public class LightTrasactionListV1 {
    @AuraEnabled
    public List <LightOfferExtended> results = new List <LightOfferExtended>();
    @AuraEnabled 
    public Integer counter = 0; 
    @AuraEnabled
    public String selectedItem = ''; 
    @AuraEnabled
    public String showpage = '';
    @AuraEnabled
    public String sortbyField = '';
    @AuraEnabled
    public String sortDirection = '';
    @AuraEnabled
    public Integer total_page = 0; 
    @AuraEnabled
    public Integer list_size = 0;
    @AuraEnabled
    public Integer total_size = 0;
    @AuraEnabled
    public Boolean isSeller = false;  
    @AuraEnabled
    public Boolean isSuperUser = false;
    @AuraEnabled
    public Boolean isSellerBuyer = false;
    @AuraEnabled
    public Boolean isSellerAssociate = false;
    @AuraEnabled
    public Boolean isSellerValidator = false;
    @AuraEnabled
    public Boolean isSellerApproval1 = false;
    @AuraEnabled
    public Boolean isSellerApproval2 = false;
    @AuraEnabled
    public Boolean isInputter = false;
    @AuraEnabled
    public Boolean isValidator = false;
    @AuraEnabled
    public Boolean isApprover = false;
    @AuraEnabled
    public Boolean isManager = false;
    @AuraEnabled
    public Boolean isSellerRequester = false;
    @AuraEnabled
    public Boolean isSellerRequesterApprover = false;
    
    @AuraEnabled
    public Boolean isPartial = false;
    @AuraEnabled
    public Boolean isTandC = false;
    @AuraEnabled
    public List <LightOfferExtended> TranspastList = new List <LightOfferExtended>(); 
    @AuraEnabled
    public List <LightOfferExtended> TranstodayList = new List <LightOfferExtended>();
    
    @AuraEnabled
    public Boolean isFilterA = false;
}