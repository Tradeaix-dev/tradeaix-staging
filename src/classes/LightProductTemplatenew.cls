public with sharing class LightProductTemplatenew {
    public static String query = ''; 
    public static Integer size = 0; 
    @AuraEnabled
    public static String getloggeduserBrowserTitle() { 
        
        return '';
    }
    private static LightProductTemplateListV ShowTable(Integer counter, String sortbyField, String sortDirection, String selected, String LoggeduserTenantID){ 
        LightProductTemplateListV retValue = new LightProductTemplateListV();
        try{
            
            String whereCondition = ''; 
            if(selected == 'Active' || selected == null){
                whereCondition = ' where Active__c = True';
            }
            else if(selected == 'All'){
                whereCondition = ' where (Active__c = True OR Active__c = false)';
            }
            else if(selected == 'Inactive'){
                whereCondition = ' where Active__c = false';
            }
            size = [SELECT count() from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'TemplateLists' AND Grid_Name__c =: 'TemplateLists']; 
            
            if(size > 0){
                retValue.list_size = Integer.valueOf([SELECT List_Size__c from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'TemplateLists' AND Grid_Name__c =: 'TemplateLists'][0].List_Size__c);
            }
            else {
                retValue.list_size = 25;
            }
            
            String query = 'SELECT Id, Name, ProductType__c, TemplateName__c, Active__c,Approve_Level__c,IsPrimary__c,CreatedDate,LastModifiedDate,CreatedBy.name,CreatedById,UserName__c FROM ProductTemplate__c';
            //query = query + ' WHERE CreatedById = ' + UserInfo.getUserId();
            query = query + whereCondition +' AND CreatedById=\'005m0000003cfA6\'' + ' ORDER BY ' + sortbyField + ' ' + sortDirection + ' LIMIT ' + retValue.list_size + ' offset ' + counter;
            retValue.results = Database.query(query);
            
            List <ProductTemplate__c> proTemplateList = new List <ProductTemplate__c>();
            List <User> userList = [SELECT Id, Name FROM User];
            for(ProductTemplate__c proTemplate : retValue.results) {
                //proTemplate.UserName__c = (Helper.getUser(userList, proTemplate.CreatedById)).Name;
                proTemplateList.add(proTemplate);
            }
            retValue.results = proTemplateList;
            
            retValue.total_size = Database.countquery('SELECT count() FROM ProductTemplate__c'+ whereCondition);
            retValue.counter = counter; 
            retValue.sortbyField = sortbyField; 
            retValue.sortDirection = sortDirection; 
            retValue.total_page = getTotalPages(retValue.list_size, retValue.total_size);
            retValue.selectedItem = (selected == null) ? 'Active': selected;
        } catch(Exception ex){
            
        }
        
        return retValue;    
    }
    
    @AuraEnabled
    public static LightProductTemplateListV getProductTemplateList(Integer counter, String sortbyField, String sortDirection, String selected, String LoggeduserTenantID){           
        LightProductTemplateListV retValue = new LightProductTemplateListV(); 
        try{  
            if((String)Cache.Session.get('sortbyFieldSOL') != null)  
                sortbyField = Helper.getSessionValue('sortbyFieldSOL');  
            
            if((String)Cache.Session.get('sortDirectionSOL') != null)  
                sortDirection = Helper.getSessionValue('sortDirectionSOL'); 
            retValue = ShowTable(counter, sortbyField, sortDirection, selected,LoggeduserTenantID);  
        } catch(Exception ex){
            
        }
        return retValue;
    } 
    
    @AuraEnabled
    public static LightProductTemplateListV getBeginning(Integer counter, String sortbyField, String sortDirection, String selected) { 
        return ShowTable(counter, sortbyField, sortDirection, selected,null); 
    }
    
    @AuraEnabled
    public static LightProductTemplateListV Views(Integer counter, String sortbyField, String sortDirection, String selected) { 
        return ShowTable(counter, sortbyField, sortDirection, selected,null); 
    }
    
    @AuraEnabled
    public static LightProductTemplateListV getPrevious(Integer counter, String sortbyField, String sortDirection, String selected) {
        return ShowTable(counter, sortbyField, sortDirection, selected,null); 
    }
    
    @AuraEnabled
    public static LightProductTemplateListV getNext(Integer counter, String sortbyField, String sortDirection, String selected) {  
        return ShowTable(counter, sortbyField, sortDirection, selected,null); 
    }
    
    @AuraEnabled
    public static LightProductTemplateListV getEnd(Integer counter, String sortbyField, String sortDirection, String selected) { 
        return ShowTable(counter, sortbyField, sortDirection, selected,null); 
    } 
    
    private static Integer getTotalPages(Integer list_size, Integer total_size) { 
        if (math.mod(total_size, list_size) > 0) {
            return total_size/list_size + 1;
        }
        else {
            return (total_size/list_size);
        }
    }  
    
    @AuraEnabled
    public static LightProductTemplateListV changelist_size(Integer counter, String sortbyField, String sortDirection, Integer list_size, String selected) { 
        LightProductTemplateListV retValue = new LightProductTemplateListV(); 
        try{
            size = [SELECT count() from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'TemplateLists' AND Grid_Name__c =: 'TemplateLists'];
            if(size > 0){  
                UI_List_Size__c listSize = new UI_List_Size__c(
                    Id = [SELECT Id from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'TemplateLists' AND Grid_Name__c =: 'TemplateLists'][0].Id,
                    List_Size__c = list_size  
                );
                update listSize;
            }
            else {
                UI_List_Size__c listSize = new UI_List_Size__c(
                    List_Size__c = list_size,
                    Name = 'TemplateLists',
                    Grid_Name__c = 'TemplateLists',
                    User__c = UserInfo.getUserId()
                );
                insert listSize;
            }  
            
            retValue = ShowTable(counter, sortbyField, sortDirection, selected,null);
            
        } catch(Exception ex){
            
        }
        return retValue;
    }
    
    @AuraEnabled
    public static LightProductTemplateListV SortTable(Integer counter, String sortbyField, String sortDirection, String selected) { 
        LightProductTemplateListV retValue = new LightProductTemplateListV(); 
        try{
            if(sortbyField == '') {
                sortbyField = 'LastModifiedDate';
                sortDirection = 'DESC';
            } else {
                if(sortDirection.equals('ASC')){
                    sortDirection = 'DESC';
                } else {
                    sortDirection = 'ASC';
                }
            }
            Helper.setSessionValue('sortbyFieldSOL', sortbyField);
            Helper.setSessionValue('sortDirectionSOL', sortDirection); 
            retValue = ShowTable(counter, sortbyField, sortDirection, selected,null); 
        } catch (Exception ex){ 
            
        }
        return retValue;
    } 
    
    @AuraEnabled
    public static LightProductTemplateListV changePage(Integer counter, String sortbyField, String sortDirection, String selected) {
        return ShowTable(counter, sortbyField, sortDirection, selected,null); 
    }
    
    @AuraEnabled
    public static List<TemplateField> getSourceFieldsList(string productType, string marketType, string LoggeduserTenantID){
        List<TemplateField> retValue = new List<TemplateField>();
        try{
            
            system.debug('@@productType1@@'+productType);
            system.debug('@@marketType1@@'+marketType);
            
            String whereCondition = ''; 
            if(marketType == 'Secondary'){
                whereCondition = ' WHERE ProductType__c IN( \'' + productType + '\', \'Both\') AND IsSecondary__c = true  AND ( Tenant__c=\''+LoggeduserTenantID+'\' OR Tenant__c=\'\') ORDER BY Name';
            }
            else if(marketType == 'Primary'){
                whereCondition = ' WHERE ProductType__c IN( \'' + productType + '\', \'Both\') AND IsPrimary__c = true AND  ( Tenant__c=\''+LoggeduserTenantID+'\' OR Tenant__c=\'\') ORDER BY Name';
            }
            
            String query = 'SELECT Id, Name,Mandatory__c, CreatedByName__c,LastModifiedByName__c, Attribute_Size__c, ProductType__c, Attribute_Type__c, IsActive__c, IsPrimary__c, IsSecondary__c, CreatedDate FROM Attributes__c';
            query = query + whereCondition;
            
            system.debug('@@query@@'+query);
            List<Attributes__c> attributesList = Database.query(query);            
            
            for(Attributes__c attr : attributesList)
            {
                String value = attr.Name.replace('~' ,' ');
                value = value.replace('`' ,' ');
                value = value.replace('@' ,' ');
                value = value.replace('#' ,' ');
                value = value.replace('$' ,' ');
                value = value.replace('%' ,' ');
                value = value.replace('^' ,' ');
                value = value.replace('&' ,' ');
                value = value.replace('*' ,' ');
                value = value.replace('(' ,' ');
                value = value.replace(')' ,' ');
                value = value.replace('-' ,' ');
                value = value.replace('_' ,' ');
                value = value.replace('=' ,' ');
                value = value.replace('+' ,' ');
                value = value.replace('{' ,' ');
                value = value.replace('}' ,' ');
                value = value.replace('[' ,' ');
                value = value.replace(']' ,' ');
                value = value.replace('|' ,' ');
                value = value.replace('/' ,' ');
                value = value.replace('\\' ,' ');
                value = value.replace('?' ,' ');
                value = value.replace('>' ,' ');
                value = value.replace('<' ,' ');
                value = value.replace('.' ,' ');
                value = value.replace('    ' ,' ');
                value = value.replace('   ' ,' ');
                value = value.replace('  ' ,' ');
                
                retValue.add(new TemplateField(
                    value.trim(), 
                    attr.Name.trim(),
                    attr.Id,
                    attr.Attribute_Type__c, 
                    String.valueOf(attr.Attribute_Size__c),
                    attr.Mandatory__c,
                    attr.CreatedByName__c,
                    attr.LastModifiedByName__c
                ));
            }
            
        } catch(Exception ex){
            
        }
        return retValue;
    }
    @AuraEnabled
    public static ProductTemplatevNEW getCloneProductTemplate(string templateId,String LoggeduserTenantID){
        List<String>  lst = new List<String>();
        String idString;
        String selectedObject  = '';
        List<TemplateField> lstTemp = new List<TemplateField>();
        List<TemplateField> srclstTempField = new List<TemplateField>();
        system.debug('/// templateId'+templateId);
        ProductTemplatevNEW retValue = new ProductTemplatevNEW();
        try{
            
            retValue.ProductTemplateaa = [SELECT Id, Name, ProductType__c, TemplateName__c, Active__c, IsPrimary__c,CreatedById,LastModifiedById, CreatedDate,LastModifiedDate FROM ProductTemplate__c WHERE Id =: templateId];
            
            if(retValue.ProductTemplateaa.TemplateName__c.contains('CloneV3'))
            {
                retValue.ProductTemplateVExtended.TemplateName = retValue.ProductTemplateaa.TemplateName__c.replace('CloneV3', 'CloneV4');
            }
            else if(retValue.ProductTemplateaa.TemplateName__c.contains('CloneV2'))
            {
                retValue.ProductTemplateVExtended.TemplateName = retValue.ProductTemplateaa.TemplateName__c.replace('CloneV2', 'CloneV3');
            }
            else if(retValue.ProductTemplateaa.TemplateName__c.contains('CloneV1'))
            {
                retValue.ProductTemplateVExtended.TemplateName = retValue.ProductTemplateaa.TemplateName__c.replace('CloneV1', 'CloneV2');
            }
            else if(retValue.ProductTemplateaa.TemplateName__c.contains('Clone'))
            {
                retValue.ProductTemplateVExtended.TemplateName = retValue.ProductTemplateaa.TemplateName__c.replace('Clone', 'CloneV1');
            }
            else{
                retValue.ProductTemplateVExtended.TemplateName = retValue.ProductTemplateaa.TemplateName__c+' Clone';
            }
            
            retValue.ProductTemplateVExtended.ProductType = retValue.ProductTemplateaa.ProductType__c;
            retValue.ProductTemplateVExtended.IsPrimary = retValue.ProductTemplateaa.IsPrimary__c;
            
            List<Product_Template_Object__c> PTList = [SELECT Attributes__r.Name FROM Product_Template_Object__c WHERE Product_Template__c=:templateId];
              for (Product_Template_Object__c PTObj:PTList){
                lst.add(PTObj.Attributes__r.Name.trim());
            }
            system.debug('####'+lst);
            String Mtype='';
            if(retValue.ProductTemplateVExtended.IsPrimary)
            {
                Mtype ='Primary';
            }else{
                Mtype ='Secondary';
            }
            retValue.ProductTemplateVExtended.SourceFields = getSourceFieldsList(retValue.ProductTemplateVExtended.ProductType,Mtype,LoggeduserTenantID);
            retValue.ProductTemplateVExtended.SelectedFields = lst;
            
            
        } catch(Exception ex){
            
        }
        return retValue;
    }
    @AuraEnabled
    public static ProductTemplatevNEW getProductTemplate(string templateId, string productType, string marketType, string templateName,String LoggeduserTenantID){
        ProductTemplatevNEW retValue = new ProductTemplatevNEW();
        try{
            if(string.isBlank(templateId) || string.isEmpty(templateId)){
                retValue.ProductType = productType; 
                system.debug('@@productType@@'+productType);
                system.debug('@@marketType@@'+marketType);
                if(marketType=='Primary')
                {
                    retValue.IsPrimary=TRUE;
                }else{
                    
                    retValue.IsPrimary=FALSE;
                }
                retValue.TemplateName = templateName;
                retValue.SourceFields = getSourceFieldsList(productType,marketType,LoggeduserTenantID);
                retValue.SelectedFields = new List<String>();
            }
        } catch(Exception ex){
            
        }
        return retValue;
    }
    
    private static TemplateField getTemplateField(List<TemplateField> sourceFields, String value) {
        TemplateField retValue = new TemplateField('', '');
        try{
            for(Integer j = 0; j < sourceFields.size(); j++){
                if(sourceFields.get(j).value == value){
                    retValue =  (TemplateField)sourceFields.get(j); 
                    break;
                }
            }
        } catch(Exception ex){
            System.debug('getTemplateField() - ' + ex.getMessage());
        }
        return retValue;
    }
    @AuraEnabled
    public static String saveProductTemplate(string template,string TenantId,String Type){
        system.debug('@@template@@'+template);
        String response = '';
        try {  
            
            ProductTemplatevNEW templateObject = (ProductTemplatevNEW)System.JSON.deserializeStrict(
                template, 
                ProductTemplatevNEW.Class
            );
            system.debug('#####'+templateObject.SelectedFields.Size());
            Integer count = [SELECT count() from ProductTemplate__c WHERE TemplateName__c = :templateObject.TemplateName AND Tenant__c=:TenantId AND ProductType__c=:Type]; 
            if(count > 0){
                response = 'Already Used';
            }
            else { 
                if(templateObject.SelectedFields.Size()==0)
                {
                     response = 'Attributes Zero';
                }else{
                system.debug('#####'+templateObject.SelectedFields);
                ProductTemplate__c result = insertProductTemplate(template,TenantId);
                response = result.Id;
                system.debug('#####'+templateObject.SelectedFields);
                List<Product_Template_Object__c> ListPTObj = New List<Product_Template_Object__c>();
                for(String value : templateObject.SelectedFields) {
                    TemplateField templateField = getTemplateField(templateObject.SourceFields, value);
                    Product_Template_Object__c PTObj = New Product_Template_Object__c();
                    PTObj.Attributes__c =templateField.AttID;
                    PTObj.Product_Template__c =result.Id;
                    PTObj.Tenant__c = TenantId;
                    ListPTObj.add(PTObj);
                    
                }
                system.debug('#####'+ListPTObj);
                if(ListPTObj.size()>0){
                    Insert ListPTObj;
                }
                
                }
            }  
        } catch(Exception ex){
            System.debug('LightProductTemplate - saveProductTemplate() : ' + ex.getMessage());
            
            response = '';
        }
        return response; 
    }
    
    private static ProductTemplate__c insertProductTemplate(String template,String TenantId){
        ProductTemplate__c retValue = new ProductTemplate__c();
        try {
            ProductTemplatevNEW templateObject = (ProductTemplatevNEW)System.JSON.deserializeStrict(
                template, 
                ProductTemplatevNEW.Class
            );
            system.debug('<<SourceFields<<'+templateObject.SourceFields);
            String jsonObj='';
            for(String tempFlds : templateObject.SelectedFields) {
                TemplateField templateField = getTemplateField(templateObject.SourceFields, tempFlds);
                string fieldName = templateField.value;
                //jsonObj=jsonObj+'{';
                //If(templateField.fieldmandatory)
                //{
                   // jsonObj=jsonObj+'"ID":"'+fieldName+'","Flag":"FALSE"';
                    
                //}else{
                    jsonObj=jsonObj+fieldName+',';
                //}
               // jsonObj=jsonObj+'},';
            }
            //jsonObj=jsonObj+']';
            String FJsonOBJ =jsonObj.replace(', ', ','); 
            retValue = new ProductTemplate__c(
                ProductType__c = templateObject.ProductType,
                TemplateName__c = templateObject.TemplateName,
                
                Active__c = true,
                Isclonechk__c=templateObject.icclonechk,
                IsPrimary__c = templateObject.IsPrimary,
                Transactiondetailfields__c=FJsonOBJ,
                Tenant__c =TenantId,
                Created_ByName__c =templateObject.CreatedByName,
                Last_ModifiedByName__c=templateObject.LastModifiedByName
                //Transactiondetailfields__c='[{"ID":"Amount__c","Flag":"FALSE"},{"ID":"Applicable_Rules__c","Flag":"TRUE"},{"ID":"Applicant__c","Flag":"FALSE"},{"ID":"Beneficiary__c","Flag":"FALSE"},{"ID":"Bid_for__c","Flag":"TRUE"},{"ID":"Confirming_Bank_Discounting_Bank__c","Flag":"FALSE"},{"ID":"Date_of_Expiry__c","Flag":"TRUE"},{"ID":"Goods__c","Flag":"FALSE"},{"ID":"l_c_opening_bank__c","Flag":"FALSE"},{"ID":"Latest_Date_of_Shipment__c","Flag":"TRUE"},{"ID":"Tenor__c","Flag":"TRUE"},{"ID":"Variation__c","Flag":"TRUE"}]'
            );
            insert retValue;
            Helper.ActivityLogInsertCallForRecord(templateObject.CreatedByName,retValue.Id,'Product Template Insert',templateObject.TemplateName+' Created.',false);
        } catch(Exception ex){
            System.debug('insertProductTemplate() - ' + ex.getMessage());
            Helper.ActivityLogInsertCallForRecord('','','Product Template Insert-Error',ex.getMessage(),true);
        }
        return retValue;
    }
    
    @AuraEnabled
    public static String CheckProductTemplate(String txtTemplateName,String TenantId,String Type) { 
        String response = '';
        try {  
            Integer count = [SELECT count() from ProductTemplate__c WHERE TemplateName__c = :txtTemplateName AND Tenant__c=:TenantId AND ProductType__c=:Type]; 
            if(count > 0){
                response = 'Already Used';
            }
            else { 
                response = '';
            }  
        } catch(Exception ex){
            System.debug('LightProductTemplate - CheckProductTemplate() : ' + ex.getMessage());
            
            response = '';
        }
        return response; 
    } 
    
    
    
}