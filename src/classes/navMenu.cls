public with sharing class navMenu {
@AuraEnabled
    public static boolean secondarymarketingflag(){
        system.debug('@Helper@');
        return false;
    } 
    
      @AuraEnabled
    public static String getloggeduser() { 
        
        String response = '';
        try {  
            
            List<user> usr=[select id from user WHERE id=:userinfo.getUserId()];
            if(usr[0].id=='005m0000003L2VaAAK'){
                response='admin';
            }else
            {response='notadmin';
            }
        }
        catch (Exception e) { 
            system.debug('ERROR ===== '+e.getMessage());
        }
        return response; 
    }
    
    @AuraEnabled
    public static Integer getBankDetails(){
        Integer total_size = 0;
        try{ 
            total_size = Database.countquery('SELECT count() FROM Notification__c WHERE  closecall__c=false AND ReadFlag__c=true AND User__c=\''+UserInfo.getUserId()+'\'');
                      } catch(Exception ex){
            System.debug('LightBankDetail - getBankDetails() : ' + ex.getMessage());
            
        }
        return total_size;
    }  
}