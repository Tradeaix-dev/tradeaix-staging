public class OrganizationCreateEmailTemp {
    public Id orgId {get;set;} 
    public class OrgDetails { 
        public String orgId {get;set;}
        public String orgName {get;set;}
        public String website {get;set;}
        public String phone {get;set;}
        public String Industry {get;set;}
        public String Employees {get;set;}
        public String Revenue {get;set;}
        public String contactname {get;set;}
        public String contactphone {get;set;}
        public String contactemail {get;set;}
        public String Thanksmsg{get;set;}
        public String logo{get;set;}
        public String CreatedBy {get;set;}
        public String CreatedDate {get;set;}
    }
    public OrgDetails email {get;set;}  
    public OrgDetails getOrg()
    {
        email = New OrgDetails();
        try{
            Organization__c ORG = [SELECT Id, Name, Organization_Logo_URL__c, Website__c, Phone__c, Industry__c, Employees__c, Revenue__c, Primary_Contact_Name__c, Primary_Contact_Phone__c, Primary_Contact_Email__c,CreatedBy__c,CreatedDate,CreatedBy__r.First_Name__c,CreatedBy__r.Last_Name__c from Organization__c WHERE Id=:orgId];
            email.website = ORG.Website__c;
            email.Industry = ORG.Industry__c;
            email.Employees = string.valueOf(ORG.Employees__c);
            email.Revenue = string.valueOf(ORG.Revenue__c);
            email.contactname = ORG.Primary_Contact_Name__c;
            email.contactphone = ORG.Primary_Contact_Phone__c;
            email.contactemail = ORG.Primary_Contact_Email__c;
            email.logo = ORG.Organization_Logo_URL__c;
            if(ORG.Organization_Logo_URL__c == null){            
                 email.logo = system.Label.TradeAix_Logo;
             }	
            email.Thanksmsg = ORG.Name +' Team';
            email.orgName = ORG.Name;
            email.phone = ORG.Phone__c;
            Datetime myDT = ORG.CreatedDate; 
            TimeZone tz = UserInfo.getTimeZone();
            String myDate = myDT.format('dd/MM/YYYY HH:mm:ss', tz.getID()); 
            email.CreatedDate = myDate;
            email.CreatedBy = ORG.CreatedBy__r.First_Name__c+' '+ORG.CreatedBy__r.Last_Name__c;  
        }catch(exception e){
            system.debug('//****// '+e.getMessage());
        }
        return email;
    }
}