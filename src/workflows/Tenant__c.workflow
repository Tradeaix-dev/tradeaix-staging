<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Tenant_Activate_Email_Alert</fullName>
        <ccEmails>mahesh@aixchange.co.in</ccEmails>
        <description>Tenant Activate Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TenantActiveNotificationEmailTemp</template>
    </alerts>
    <alerts>
        <fullName>Tenant_Creation_Email_Alert</fullName>
        <ccEmails>mahesh@aixchange.co.in</ccEmails>
        <description>Tenant Creation Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TenantCreateEmailTemp</template>
    </alerts>
    <alerts>
        <fullName>Tenant_Inactivate_Email_Alert</fullName>
        <ccEmails>mahesh@aixchange.co.in</ccEmails>
        <description>Tenant Inactivate Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TenantInactiveNotificationEmailTemp</template>
    </alerts>
    <rules>
        <fullName>Tenant Activate</fullName>
        <actions>
            <name>Tenant_Activate_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Tenant__c.IsActive__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Tenant Creation</fullName>
        <actions>
            <name>Tenant_Creation_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Tenant InActivate</fullName>
        <actions>
            <name>Tenant_Inactivate_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.IsActive</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
