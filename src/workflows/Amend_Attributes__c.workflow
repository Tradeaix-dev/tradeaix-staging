<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approved_Email_Alert</fullName>
        <ccEmails>jsloperation@gmail.com</ccEmails>
        <ccEmails>jindalsteela@gmail.com,</ccEmails>
        <ccEmails>shubhada.reuben@gmail.com</ccEmails>
        <description>Approved Email Alert</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Approved_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Rejected_Email_Alert</fullName>
        <ccEmails>jsloperation@gmail.com</ccEmails>
        <ccEmails>jindalsteela@gmail.com,</ccEmails>
        <ccEmails>shubhada.reuben@gmail.com</ccEmails>
        <description>Rejected Email Alert</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Rejected_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>SubmittedApprovalEmailAlert</fullName>
        <ccEmails>jsloperation@gmail.com</ccEmails>
        <ccEmails>jindalsteela@gmail.com,</ccEmails>
        <ccEmails>shubhada.reuben@gmail.com</ccEmails>
        <description>SubmittedApprovalEmailAlert</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Submitforapproval_Email_Template</template>
    </alerts>
    <rules>
        <fullName>Amendment Approved Email Rule</fullName>
        <actions>
            <name>Approved_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Amend_Attributes__c.Amend_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Amendment Rejected Email Rule</fullName>
        <actions>
            <name>Rejected_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Amend_Attributes__c.Amend_Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SubmittedApproval Rule</fullName>
        <actions>
            <name>SubmittedApprovalEmailAlert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Amend_Attributes__c.Amend_Status__c</field>
            <operation>equals</operation>
            <value>Submitted for Approval</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
