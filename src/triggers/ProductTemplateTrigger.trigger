trigger ProductTemplateTrigger on ProductTemplate__c (after insert,after update) {
     EmailManager em = new EmailManager();  
     for(ProductTemplate__c PT : Trigger.New){    
          try{
              String toEmail = [SELECT User_Email__c from User_Management__c WHERE Tenant__c=: PT.Tenant__c limit 1].User_Email__c;
              List<String> CcAddresses = new List<String>();
              if (Trigger.isUpdate) {
                  String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'TemplateInactivateEmailTemp' LIMIT 1].Id;
                  em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(PT.Id));
              }
              if (Trigger.isInsert) {
                  if(PT.Isclonechk__c == true){
                      String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'TemplateCloneEmailTemplate' LIMIT 1].Id;
                      em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(PT.Id));
                  }else{
                      String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'TemplateCreationEmailTemp' LIMIT 1].Id;
                      em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(PT.Id));
                  }
                  
              }
          }catch(exception ex){
            system.debug('==ProductTemplate__c Trigger Exception=='+ex.getMessage());
        }
     }
}