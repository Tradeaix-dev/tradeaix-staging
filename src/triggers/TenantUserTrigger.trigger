trigger TenantUserTrigger on User_Management__c (after insert, after update) {
    EmailManager em = new EmailManager();  
    system.debug('====TenantUserTrigger Inside====');
    if(Trigger.isAfter){
        try{
            for (User_Management__c tenantuser: Trigger.new) {
                if (Trigger.isInsert) {
                system.debug('======isInsert====='+tenantuser.CreatedFromrapid__c);
                system.debug('======isInsert====='+tenantuser.CreatedBy__c);
                system.debug('======isInsert====='+tenantuser.Id);                
                    system.debug('======isInsert=====');
                    Integer OrgCount = [select count() from User_Management__c WHERE Tenant__c =: tenantuser.Tenant__c ];
                    Tenant__c T = new Tenant__c();
                    T.Id = tenantuser.Tenant__c;
                    T.Tenant_User_Count__c = OrgCount;
                    update T;
               }
                if (Trigger.isUpdate) {
                    system.debug('======isUpdate=====');
                    User_Management__c oldUsers = Trigger.oldMap.get(tenantuser.ID);
                    //email class pass parameters here   
                    system.debug('======isUpdate====='+tenantuser.Profiles__c+':::'+oldUsers.Profiles__c);
                    if(tenantuser.Profiles__c == system.label.BMCP_ProfileID && oldUsers.Profiles__c == system.label.OBCP_ProfileID){
                        List<String> CcAddresses = new List<String>();
                        CcAddresses.add(tenantuser.User_Email__c);
                        String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'UserActiveNotificationEmailTemp' LIMIT 1].Id; 
                        em.sendMailWithTemplate(tenantuser.User_Email__c , CcAddresses , temaplateId , userinfo.getuserid(), tenantuser.Id); 
                    }                    
                }
            }
        }catch(exception ex){
            system.debug('=======Exception======='+ex.getMessage());
        }
    }

}