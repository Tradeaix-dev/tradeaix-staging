trigger OTPHistoryTrigger  on OTP_History__c (before insert,after insert) {
    EmailManager em = new EmailManager();  
    system.debug('====OTP_History__c Inside====');
    
    try{
   
        for (OTP_History__c tenantuser: Trigger.new) {
        if(Trigger.isbefore){
        tenantuser.OTP_Generated_On__c = System.Now();
        tenantuser.OTP_Expired_On__c = System.Now().AddMinutes(Integer.valueOf(System.Label.OTPTimeinterval));
        }
        if(Trigger.isafter){
            Limited_Bidding__c UM = [SELECT Id,agree__c,Transaction__r.RFQChk__c,Transaction__r.RFIChk__c,CreatedBy__c,Tenant_User__c from Limited_Bidding__c WHERE Id=:tenantuser.Limited_Bidding__c];

       system.debug('##'+tenantuser);
        system.debug('##'+UM .agree__c);
        if(UM.agree__c) {
        //&& (UM.Transaction__r.RFQChk__c || UM.Transaction__r.RFIChk__c)){
        List<String> CcAddresses = new List<String>(); 
        String toEmail = [SELECT User_Email__c from User_Management__c WHERE id=:UM.Tenant_User__c].User_Email__c ;
        system.debug('======isInsert=====');                   
        List<User_Management__c> listUserEmail= [SELECT User_Email__c from User_Management__c WHERE Id =:UM.CreatedBy__c];                 
        for( User_Management__c lstStr: listUserEmail){
        CcAddresses.add(lstStr.User_Email__c) ;
        }
        String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'OTPEmailTemp' LIMIT 1].Id;
        em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(tenantuser.Id));
        }
        }
        }
    }
    catch(exception ex){
        system.debug('=======Exception======='+ex.getMessage());
    }
    }